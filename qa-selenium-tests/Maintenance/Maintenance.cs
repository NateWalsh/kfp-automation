﻿using NUnit.Framework;
using qa_selenium_framework;
using qa_selenium_framework.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qa_selenium_tests.Maintenance
{
    [TestFixture]
    [Category("Maintenance")]


    class Maintenance : BaseTestClass
    {
        #region Delete Data
        [Test]
        public void DeleteCompanies()
        {
            loginPage().Login();
            companyPage().DeleteAllCompanies();
        }

        [Test]
        public void DeleteCompanyByName()
        {
            loginPage().Login();
            companyPage().DeleteCompanyByName("obals test");
        }
        #endregion

        #region Rollbacks

        [Test]
        public void RollbackByName()
        {
            loginPage().Login();
            companyPage().SelectCompanyByName("123TestAccount");
            payrollPage().RollRack();
        }

        #endregion
    }
}
