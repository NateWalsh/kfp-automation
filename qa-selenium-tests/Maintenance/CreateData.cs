﻿using NUnit.Framework;
using qa_selenium_framework;
using qa_selenium_framework.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qa_selenium_tests.Maintenance
{
    [TestFixture]
    [Category("Create Data")]

    class CreateData : BaseTestClass
    {
        #region Create Company Data
        [Test]
        public void CreateCompanyFromExcel()
        {
            loginPage().Login();
            companyPage().CreateExcelCompany();
        }

        [Test]
        public void CreateNewTestCompany()
        {
            loginPage().Login();
            companyPage().DeleteTestCompany().OpenTestCompany();
            pensionPage().CreateDefaultPension();
            employeePage().CreateEssEmployee();
            payrollPage().PayMonthly(4000m);
        }

        [Test]
        public void CreateCompanyWithName()
        {
            loginPage().Login();
            companyPage().CreateCompanyByName("NI Cat Test");
            employeePage().CreateDefaultEmployee();
        }

        [Test]
        public void CreateCompanyWithNest()
        {
            loginPage().Login();
            companyPage().CreateDefaultCompany();
            pensionPage().CreateNestPension();
            employeePage().CreateEmployees(1);
        }

        [Test]
        public void CreateDefaultCompany()
        {
            loginPage().Login();
            companyPage().CreateDefaultCompany();
            pensionPage().CreateDefaultPension();
            employeePage().CreateEmployees(1);
        }

        [Test]
        public void CreateCustomCompany()
        {
            loginPage().Login();
            //var customCompany = Company.GetDefault();
            //customCompany.CompanyName = "big";
            //customCompany.HasMonthly = true;
            //customCompany.MonthlyPayDate = new DateTime(2016, 04, 30);

            var customCompany = Company.GetDefault();

            customCompany.MonthlyPayDate = new DateTime(2016, 05, 02);
            companyPage().CreateCompany(customCompany);
            pensionPage().CreateCustomStaging(new DateTime(2016, 05, 01));

            employeePage().CreateEmployees(1);
        }
        #endregion

        #region Create Employee Data
        [Test]
        public void CreateEmployeesFromExcel()
        {
            loginPage().Login();
            employeePage().CreateEmployeesFromExcel(true);
        }

        [Test]
        public void CreateEmpFromExcel()
        {
            loginPage().Login();
            Employee employee = Employee.getFromExcelSingle();
            employeePage().CreateEmployee(employee);
        }

        [Test]
        public void CreateEmpsOnTestFromExcel()
        {
            loginPage().Login();
            companyPage().OpenTestCompany();
            employeePage().CreateEmployeesFromExcel(true);
        }

        #endregion

        #region Create Tenant Data

        #endregion
    }
}
