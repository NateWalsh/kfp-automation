﻿using NUnit.Framework;
using qa_selenium_framework;
using qa_selenium_framework.Elements;
using qa_selenium_framework.PageObjects;
using System;
using System.Configuration;

namespace qa_selenium_tests
{
    [TestFixture]
    public class BaseTestClass
    {
        public BaseTestClass()
        {
            string usr = ConfigurationManager.AppSettings["user"];
            string env = ConfigurationManager.AppSettings["environment"];
            string bwr = ConfigurationManager.AppSettings["browser"];

            try
            {
                user = (TestUser)Enum.Parse(typeof(TestUser), usr);
                environment = (TestEnvironment)Enum.Parse(typeof(TestEnvironment), env);
                browser = (TestBrowser)Enum.Parse(typeof(TestBrowser), bwr);
            }
            catch
            {
                //If there error, set default values
                user = TestUser.Nate;
                environment = TestEnvironment.Stage;
            }
        }

        protected static TestUser user;
        protected static TestEnvironment environment;
        protected static TestBrowser browser;
        protected static string nameOfCurrentTest = string.Empty;
        protected static bool useAdminAccount = true;
        private static string logMessage = string.Empty;

        #region Setup
        [OneTimeSetUp]
        public static void RunBeforeAllTests()
        {
            //Driver.initialise(user, environment, browser);
        }
        [OneTimeTearDown]
        public static void RunAfterAllTests()
        {
            //Driver.close();
        }
        [SetUp]
        public static void RunBeforeEachTest()
        {
            Driver.initialise(user, environment, browser);
        }
        [TearDown]
        public static void RunAfterEachTest()
        {
            //Driver.close();
        }

        #endregion

        #region Page References

        public LoginPage loginPage()
        {
            var loginPage = new LoginPage();
            return loginPage;
        }

        public PensionPage pensionPage()
        {
            var pensionPage = new PensionPage();
            return pensionPage;
        }

        public CommsPage commsPage()
        {
            var commsPage = new CommsPage();
            return commsPage;
        }

        public CompanyPage companyPage()
        {
            var companyPage = new CompanyPage();
            return companyPage;
        }

        public CompanyPageElements _companyPage
        {
            get
            {
                return new CompanyPageElements();
            }
        }

        public EmployeePage employeePage()
        {
            var employeePage = new EmployeePage();
            return employeePage;
        }

        public EmployeePageElements _employeePage
        {
            get
            {
                return new EmployeePageElements();
            }
        }

        public GmailPage gmailPage()
        {
            var gmailPage = new GmailPage();
            return gmailPage;
        }

        public GmailPageElements _gmailPage
        {
            get
            {
                return new GmailPageElements();
            }
        }

        public PayrollPage payrollPage()
        {
            var payrollPage = new PayrollPage();
            return payrollPage;
        }

        public PayrollPageElements _payrollPage
        {
            get
            {
                return new PayrollPageElements();
            }
        }

        public SalesAgentPage salesAgentPage()
        {
            var salesAgentPage = new SalesAgentPage();
            return salesAgentPage;
        }

        public SalesAgentPageElements _salesAgentPage
        {
            get
            {
                return new SalesAgentPageElements();
            }
        }

        public SettingsPageElements _settingsPage
        {
            get
            {
                return new SettingsPageElements();
            }
        }

        public SettingsPage settingsPage()
        {
            var settingsPage = new SettingsPage();
            return settingsPage;
        }


        #endregion


    }

}
