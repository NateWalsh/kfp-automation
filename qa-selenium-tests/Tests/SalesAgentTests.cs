﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using qa_selenium_framework;
using NUnit.Framework;
using qa_selenium_framework.Data;

namespace qa_selenium_tests
{

    [TestFixture]

    public class SalesAgentTests : BaseTestClass
    {

        #region Confirmation Screen Tests
        [Test]
        public void SAConfirmationScreenSME()
        {
            salesAgentPage().AssertSAConfirmationScreen(Tenant.GetSME());
        }

        [Test]
        public void SAConfirmationScreenSMEWithAE()
        {
            salesAgentPage().AssertSAConfirmationScreen(Tenant.GetSMEWithAE());
        }

        [Test]
        public void SAConfirmationScreenAccountant()
        {
            salesAgentPage().AssertSAConfirmationScreen(Tenant.GetAccountant());
        }

        [Test]
        public void SAConfirmationScreenAccountantWithSME()
        {
            salesAgentPage().AssertSAConfirmationScreen(Tenant.GetAccountantWithAE());
        }
        #endregion


        #region Price Bands
        [Test]
        public void AccountantBandsWithAE()
        {
            var tenant = Tenant.GetAccountantWithAE();

            companyPage().CreateCompany(Company.getMonthly());

            loginPage().SalesLogin();

            salesAgentPage().CompleteClientDetails(tenant).CheckAccountantBands(tenant);
        }

        [Test]
        public void AccountantBands()
        {
            var tenant = Tenant.GetAccountant();

            loginPage().SalesLogin();

            salesAgentPage().CompleteClientDetails(tenant).CheckAccountantBands(tenant);
        }

        [Test]
        public void AllBands()
        {
            loginPage().SalesLogin();

            var tenant = Tenant.GetAccountant();
            salesAgentPage().CompleteClientDetails(tenant).CheckAccountantBands(tenant);
            salesAgentPage().GoToSales();

            tenant = Tenant.GetAccountantWithAE();
            salesAgentPage().CompleteClientDetails(tenant).CheckAccountantBands(tenant);
            salesAgentPage().GoToSales();

            tenant = Tenant.GetSME();
            salesAgentPage().CompleteClientDetails(tenant).CheckAccountantBands(tenant);
            salesAgentPage().GoToSales();

            tenant = Tenant.GetSMEWithAE();
            salesAgentPage().CompleteClientDetails(tenant).CheckAccountantBands(tenant);
            salesAgentPage().GoToSales();
        }



        [Test]
        public void SMEBands()
        {
            var tenant = Tenant.GetSME();

            loginPage().SalesLogin();

            salesAgentPage().CompleteClientDetails(tenant).CheckAccountantBands(tenant);
        }

        [Test]
        public void SMEBandsWithAE()
        {
            var tenant = Tenant.GetSMEWithAE();

            loginPage().SalesLogin();

            salesAgentPage().CompleteClientDetails(tenant).CheckAccountantBands(tenant);
        }

        #endregion
        #region Self Sign Up

        [Test]
        public void SelfSignUp()
        {
            loginPage().GoToSelfSignUp();
            salesAgentPage().SelfSignUp(Tenant.GetSMEWithAE());
            companyPage().CreateDefaultCompany();
        }

        [Test]
        public void CreateSME()
        {
            Tenant t = Tenant.GetSME();
            salesAgentPage().CreateTenant(t);
        }

        [Test]
        public void CreateAccountant()
        {
            Tenant t = Tenant.GetAccountant();
            salesAgentPage().CreateTenant(t);
        }

        [Test]
        public void CreateSMEWithMonthlyWithAEAnd3EmployeesAndAnAEPensionWithPostponementOnStarters()
        {
            //Tenant t = Tenant.GetSMEWithAE();
            //salesAgentPage().CreateTenant(t);

            //companyPage().CreateDefaultCompany();

            //employeePage().CreateEmployees(3);

            //pensionPage().CreateStarterPostponementPension();

            loginPage().Login();
            var company = Company.getTestAccount();
            companyPage().SelectCompanyByName("Rams Company");
            payrollPage().CreateAllPayslips(new decimal[] { 750m, 1000m });
        }

        #endregion
    }
}
