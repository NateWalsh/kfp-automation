﻿using NUnit.Framework;
using qa_selenium_framework;
using qa_selenium_framework.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qa_selenium_tests.Tests
{
    [TestFixture]

    class AeTests: BaseTestClass
    {
        [Test]
        public void TestAllEnrolLetters()        {
            AEThreshold pay = AEThreshold.Get2016();
            Employee e = Employee.GetDefault();

            loginPage().Login();

            companyPage().DeleteTestCompany().OpenTestCompany();
            pensionPage().CreateStagingDateInFuture();
            employeePage().CreateDefaultEmployee();
            payrollPage().PayMonthly(pay.AboveTrigger);
            commsPage().AssertSingleComms(e, pay.Trigger, EnrolLetters.L0);
            payrollPage().RollRack().DeleteAllPayslips();
            payrollPage().PayMonthly(pay.AboveTrigger);
            commsPage().AssertSingleComms(e, pay.Trigger, EnrolLetters.L0);
            payrollPage().PayMonthly(pay.Trigger);
            commsPage().AssertSingleComms(e, pay.Trigger, EnrolLetters.L2and3);
            payrollPage().PayMonthly(pay.AboveTrigger);
            commsPage().AssertSingleComms(e, pay.AboveTrigger, EnrolLetters.L1);
            payrollPage().RollRack().DeleteAllPayslips();
            pensionPage().UpdatePostponement(Pension.GetStarterPostponement());
            payrollPage().PayMonthly(pay.AboveTrigger);
            commsPage().AssertSingleComms(e, pay.AboveTrigger, EnrolLetters.L6);
            payrollPage().PayMonthly(pay.Trigger);
            commsPage().AssertSingleComms(e, pay.Trigger, EnrolLetters.L2and3);
            payrollPage().RollRack().DeleteAllPayslips();
            pensionPage().UpdatePostponement(Pension.GetBothPostponemnet());
            payrollPage().PayMonthly(pay.AboveTrigger);
            commsPage().AssertSingleComms(e, pay.AboveTrigger, EnrolLetters.L6);
            payrollPage().payrollPage().PayMonthly(pay.Trigger);
            commsPage().AssertSingleComms(e, pay.Trigger, EnrolLetters.L2and3);
            payrollPage().payrollPage().PayMonthly(pay.AboveTrigger);
            commsPage().AssertSingleComms(e, pay.AboveTrigger, EnrolLetters.L1P);
            payrollPage().payrollPage().PayMonthly(pay.Trigger);
            commsPage().AssertSingleComms(e, pay.AboveTrigger, EnrolLetters.L1P);
            payrollPage().payrollPage().PayMonthly(pay.AboveTrigger);
            commsPage().AssertSingleComms(e, pay.AboveTrigger, EnrolLetters.L1P);
            payrollPage().payrollPage().PayMonthly(pay.AboveTrigger);
            commsPage().AssertSingleComms(e, pay.AboveTrigger, EnrolLetters.L1);
            payrollPage().RollRack().DeleteAllPayslips();
            payrollPage().payrollPage().PayMonthly(pay.AboveTrigger);
            commsPage().AssertSingleComms(e, pay.AboveTrigger, EnrolLetters.L6);
            payrollPage().payrollPage().PayMonthly(pay.AboveTrigger);
            commsPage().AssertSingleComms(e, pay.AboveTrigger, EnrolLetters.L1);
        }

        [Test] //Broken at the moment. Pagination kills it.
        public void CustomLetterTest()
        {
            AEThreshold pay = AEThreshold.Get2016();
            loginPage().Login();
            companyPage().DeleteTestCompany().OpenTestCompany();
            var StagingDate = new DateTime(2016, 05, 30);
            pensionPage().CreateEJPostponementPensionWithDate(StagingDate);

            employeePage().CreateEmployees(2);

            //Month 1
            decimal[] month1 = new decimal[] { pay.LEL, pay.AboveTrigger };
            payrollPage().CreateAllPayslips(month1);

            commsPage().AssertCommByNumber(1, month1[0], EnrolLetters.L2and3);
            commsPage().AssertCommByNumber(2, month1[1], EnrolLetters.L1P);


            //payrollPage().CreateAllPayslips(month1);
        }


        [Test] //Broken at the moment. Pagination kills it.
        public void FullEnrolLetterTest()
        {
            AEThreshold pay = AEThreshold.Get2016();
            loginPage().Login();
            companyPage().DeleteTestCompany().OpenTestCompany();
            pensionPage().CreateNonAE().CreateBothPostponementInFuture();

            employeePage().CreateEmployees(7);

            employeePage().OptIn(1, new DateTime(2016, 04, 01), Pension.GetNonAE());
            employeePage().AddTransitionalPeriod(3);

            //Month 1
            decimal[] month1 = new decimal[] { pay.AboveLEL, pay.AboveTrigger, pay.AboveTrigger, pay.Trigger, pay.LEL, pay.AboveUEL };
            payrollPage().CreateAllPayslips(month1);

            commsPage().AssertCommByNumber(1, month1[0], EnrolLetters.L4);
            commsPage().AssertCommByNumber(2, month1[1], EnrolLetters.L0);
            commsPage().AssertCommByNumber(3, month1[2], EnrolLetters.L0);
            commsPage().AssertCommByNumber(4, month1[3], EnrolLetters.L0);
            commsPage().AssertCommByNumber(5, month1[4], EnrolLetters.L0);
            commsPage().AssertCommByNumber(6, month1[5], EnrolLetters.L0);
            commsPage().AssertCommByNumber(7, 0, EnrolLetters.L0);

            //Month 2
            decimal[] month2 = new decimal[] { pay.AboveLEL, pay.AboveTrigger, pay.AboveTrigger, pay.Trigger, pay.LEL, pay.AboveUEL };

            payrollPage().CreateAllPayslips(month2);

            commsPage().AssertCommByNumber(1, month2[0], EnrolLetters.L4);
            commsPage().AssertCommByNumber(2, month2[1], EnrolLetters.L6);
            commsPage().AssertCommByNumber(3, month2[2], EnrolLetters.L1T);
            commsPage().AssertCommByNumber(4, month2[3], EnrolLetters.L6);
            commsPage().AssertCommByNumber(5, month2[4], EnrolLetters.L6);
            commsPage().AssertCommByNumber(6, month2[5], EnrolLetters.L6);
            commsPage().AssertCommByNumber(7, 0, EnrolLetters.L6);

            //Month 3 THIS DOESNT WORK BECAUSE OF PAGINATION. IT CAN BE FIXED BY PUTTING TRY CATCHES WITH PAGINATION IN IT
            decimal[] month3 = new decimal[] { pay.AboveLEL, pay.AboveTrigger, pay.AboveTrigger, pay.Trigger, pay.LEL, pay.AboveUEL };

            payrollPage().CreateAllPayslips(month3);

            commsPage().AssertCommByNumber(1, month3[0], EnrolLetters.L4);
            commsPage().AssertCommByNumber(2, month3[1], EnrolLetters.L1);
            commsPage().AssertCommByNumber(3, month3[2], EnrolLetters.L1T);
            commsPage().AssertCommByNumber(4, month3[3], EnrolLetters.L2and3);
            commsPage().AssertCommByNumber(5, month3[4], EnrolLetters.L2and3);
            commsPage().AssertCommByNumber(6, month3[5], EnrolLetters.L1);
            commsPage().AssertCommByNumber(7, 0, EnrolLetters.L2and3);
        }

        [Test]
        public void EnrolMutipleEmps()
        {
            AEThreshold pay = AEThreshold.Get2016();

            loginPage().Login();
            companyPage().DeleteTestCompany().OpenTestCompany();
            pensionPage().CreateDefaultPension();
            employeePage().CreateEmployees(2);
            payrollPage().CreateAllPayslips(new decimal[]{ pay.AboveLEL, pay.AboveTrigger});
            commsPage().AssertCommByNumber(1,pay.AboveLEL,EnrolLetters.L2and3);
            commsPage().AssertCommByNumber(2, pay.AboveTrigger, EnrolLetters.L1);
            payrollPage().CreateAllPayslips(new decimal[] { pay.AboveTrigger, pay.Trigger });
            commsPage().AssertCommByNumber(1, pay.AboveTrigger, EnrolLetters.L1);
            commsPage().AssertCommByNumber(2, pay.AboveTrigger, EnrolLetters.L1);
            payrollPage().RollRack().DeleteAllPayslips();
            pensionPage().UpdatePostponement(Pension.GetBothPostponemnet());
            payrollPage().CreateAllPayslips(new decimal[] { pay.Trigger, pay.AboveTrigger });
            commsPage().AssertCommByNumber(1, pay.Trigger, EnrolLetters.L6);
            commsPage().AssertCommByNumber(2, pay.AboveTrigger, EnrolLetters.L6);
            payrollPage().CreateAllPayslips(new decimal[] { pay.Trigger, pay.AboveTrigger });
            commsPage().AssertCommByNumber(1, pay.Trigger, EnrolLetters.L2and3);
            commsPage().AssertCommByNumber(2, pay.AboveTrigger, EnrolLetters.L1);
            payrollPage().CreateAllPayslips(new decimal[] { pay.AboveTrigger, pay.Trigger });
            commsPage().AssertCommByNumber(1, pay.AboveTrigger, EnrolLetters.L1P);
            commsPage().AssertCommByNumber(2, pay.AboveTrigger, EnrolLetters.L1);
            employeePage().AddTransitionalPeriod(Employee.getEmpByNumber(1));
            payrollPage().CreateAllPayslips(new decimal[] { pay.AboveTrigger, pay.LEL });
            commsPage().AssertCommByNumber(1, pay.AboveTrigger, EnrolLetters.L1T);
            commsPage().AssertCommByNumber(2, pay.AboveTrigger, EnrolLetters.L1);
        }

        [Test]
        public void PrintL2AndL3()
        {
            loginPage().Login();

            commsPage().TestL2AndL3(Employee.GetDefault());
        }

        [Test]
        public void PublishL2AndL3()
        {
            loginPage().Login();

            commsPage().TestL2AndL3(Employee.getESSEmployee());
        }

        [Test]
        public void PrintL1Opt()
        {
            loginPage().Login();

            commsPage().TestL1Opt(Employee.GetDefault());
        }

        [Test]
        public void PublishL1Opt()
        {
            loginPage().Login();

            commsPage().TestL1Opt(Employee.getESSEmployee());
        }

        [Test]
        public void PublishL1T()
        {
            loginPage().Login();
            commsPage().TestL1T(Employee.getESSEmployee());
        }

        [Test]
        public void PrintL1T()
        {
            loginPage().Login();
            commsPage().TestL1T(Employee.GetDefault());
        }

        [Test]
        public void DeletePensions()
        {
            loginPage().Login();
            companyPage().OpenTestCompany();
            payrollPage().RollRack().DeleteAllPayslips();
            pensionPage().DeleteAllPensions().CreateDefaultPension();
        }

        [Test]
        public void OptOutRefund()
        {
            AEThreshold pay = AEThreshold.Get2016();

            loginPage().Login();
            companyPage().DeleteTestCompany().OpenTestCompany();
            pensionPage().CreateDefaultPension();
            employeePage().CreateDefaultEmployee();
            payrollPage().PayMonthly(pay.AboveTrigger);
            commsPage().AssertSingleComms(Employee.GetDefault(), pay.AboveTrigger, EnrolLetters.L1);
            employeePage().OptOut(Employee.GetDefault(), new DateTime(2016,05,01));
            payrollPage().PayMonthly(pay.AboveTrigger);
        }

        [Test]
        public void EJAfterEnrol()
        {
            AEThreshold pay = AEThreshold.Get2016();

            loginPage().Login();
            companyPage().DeleteTestCompany().OpenTestCompany();
            pensionPage().CreateDefaultPension();
            employeePage().CreateDefaultEmployee();
            payrollPage().PayMonthly(pay.AboveTrigger);
            payrollPage().RollRack();
            payrollPage().DeleteAllPayslips();
            payrollPage().PayMonthly(pay.AboveTrigger);

            //payrollPage().PayMonthly(pay.AboveTrigger);
            //commsPage().AssertSingleComms(Employee.GetDefault(), pay.Trigger, EnrolLetters.L2and3);
            //pensionPage().UpdatePostponement(Pension.GetEJPostponement());
            //payrollPage().PayMonthly(pay.AboveTrigger);
        }

        [Test]
        public void EJPostponement1stOfMonth()
        {
            AEThreshold pay = AEThreshold.Get2016();

            loginPage().Login();
            companyPage().DeleteTestCompany().OpenTestCompany();
            pensionPage().CreateEJPostponementPensionWithDate(new DateTime(2016,05,01));
            employeePage().CreateDefaultEmployee();
            payrollPage().PayMonthly(pay.AboveTrigger);
            payrollPage().PayMonthly(pay.AboveTrigger);
        }

        [Test]
        public void StarterPostponement1stOfMonth()
        {
            AEThreshold pay = AEThreshold.Get2016();

            loginPage().Login();
            companyPage().DeleteTestCompany().OpenTestCompany();
            pensionPage().CreateStarterPostponementPensionWithDate(new DateTime(2016, 05, 01));
            employeePage().CreateDefaultEmployee();
            payrollPage().PayMonthly(pay.AboveTrigger);
            payrollPage().PayMonthly(pay.AboveTrigger);
        }

        [Test]
        public void NestPension()
        {
            AEThreshold pay = AEThreshold.Get2016();

            loginPage().Login();
            companyPage().DeleteTestCompany().OpenTestCompany();
            pensionPage().CreateNestPension();
            employeePage().CreateDefaultEmployee();
            payrollPage().PayMonthly(pay.AboveTrigger);
        }

        [Test]
        public void PrintL0()
        {
            loginPage().Login();

            commsPage().TestL0L2L1(Employee.GetDefault());
        }

        [Test]
        public void PublishL0()
        {
            loginPage().Login();

            commsPage().TestL0L2L1(Employee.getESSEmployee());
        }

        [Test]
        public void PrintL6()
        {
            loginPage().Login();

            commsPage().TestL0L6(Employee.GetDefault());
        }

        [Test]
        public void PublishL6()
        {
            loginPage().Login();

            commsPage().TestL0L6(Employee.getESSEmployee());
        }

        [Test]
        public void PrintL4()
        {
            AEThreshold pay = AEThreshold.Get2016();

            loginPage().Login();

            commsPage().TestL4(Employee.GetDefault());
        }

        [Test]
        public void PublishL4()
        {
            AEThreshold pay = AEThreshold.Get2016();

            loginPage().Login();

            commsPage().TestL4(Employee.getESSEmployee());
        }

        [Test]
        public void WorkerStatusTest()
        {
            AEThreshold pay = AEThreshold.Get2016();
            commsPage().TesstWorkerStatus(pay);
        }

        [Test]
        public void PrintedTabL1()
        {
            AEThreshold pay = AEThreshold.Get2016();

            loginPage().Login();
            companyPage().DeleteTestCompany().OpenTestCompany();
            pensionPage().CreateDefaultPension();
            employeePage().CreateDefaultEmployee();
            payrollPage().PayMonthly(pay.AboveTrigger);
            commsPage().AssertSingleComms(Employee.GetDefault(), pay.AboveTrigger, EnrolLetters.L1);
            commsPage().ClickLastCommsRow().AssertSingleCommsPrinted(Employee.GetDefault(), pay.AboveTrigger, EnrolLetters.L1);
        }
    }
}
