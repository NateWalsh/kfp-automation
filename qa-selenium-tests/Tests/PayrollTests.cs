﻿using NUnit.Framework;
using qa_selenium_framework;
using qa_selenium_framework.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qa_selenium_tests.Tests
{
    [TestFixture]
    class PayrollTests : BaseTestClass
    {
        [Test]
        public void PayRunFromExcel()
        {
            loginPage().Login();
            companyPage().CreateDefaultCompany();
            employeePage().CreateDefaultEmployee();

            payrollPage().PayAndAssertFromPayrollData(Employee.GetDefault());
        }

        [Test]
        public void WeeklyPayRunFromExcel()
        {
            loginPage().Login();
            companyPage().CreateWeeklyCompany();
            employeePage().CreateWeeklyEmployee();

            payrollPage().PayAndAssertFromPayrollData(Employee.getWeekly());
        }

        [Test]
        public void PayrollRunFromTestCompany()
        {
            loginPage().Login();
            companyPage().DeleteTestCompany().OpenTestCompany();
            employeePage().CreateDefaultEmployee();

            payrollPage().PayAndAssertFromPayrollData(Employee.GetDefault())
                .RollBackPayTestCompany();
        }

        [Test]
        public void PayCompany()
        {
            loginPage().Login();
            companyPage().SelectCompanyByName("big company");
            payrollPage().CreateAllPayslips(1000m);
        }
    }
}
