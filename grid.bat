REM Grid Runner Batch File Below
set HERE=C:\Users\NathanWalsh\Documents\openpayslipsopenenrol\qa-selenium-framework
set SELENIUM_VERSION=2.53.1
set HUB_URL=http://localhost:4444/grid/register
set CHROME_DRIVER_LOC=%HERE%/chromedriver.exe
start java -jar selenium-server-standalone-2.53.1.jar -role hub
start java -jar selenium-server-standalone-2.53.1.jar -role node -Dwebdriver.chrome.driver=%CHROME_DRIVER_LOC% -hub %HUB_URL% -port 5558 -maxSession 4 -browser, browserName=chrome,maxInstances=4
start java -jar selenium-server-standalone-2.53.1.jar -role node -Dwebdriver.chrome.driver=%CHROME_DRIVER_LOC% -hub %HUB_URL% -port 5559 -maxSession 4 -browser, browserName=chrome,maxInstances=4