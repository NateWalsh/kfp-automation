﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using qa_selenium_tests;
using qa_selenium_framework.Data;
using qa_selenium_framework;

namespace Payslips_Tests
{
    [TestFixture]
    [Category("EnrolLetterTemplates")]

    class OELetters : BaseTestClass
    {
        [Test]
        public void LoginAsEmployeeWithEnrolLetter()
        {
            loginPageOP().Login();
            enrolHarnessPage().LogInAsEmp(EnrolTemplates.GetL1Opt());
        }

        [Test]
        public void SendAllLetters()
        {
            loginPageOP().Login();
            enrolHarnessPage().SendAllLetters();
        }

        [Test]
        public void SendL0()
        {
            loginPageOP().Login();
            enrolHarnessPage().SendL0();
        }

        [Test]
        public void SendL1()
        {
            loginPageOP().Login();
            enrolHarnessPage().SendL1();
        }

        [Test]
        public void SendL1P()
        {
            loginPageOP().Login();
            enrolHarnessPage().SendL1P();
        }

        [Test]
        public void SendL1T()
        {
            loginPageOP().Login();
            enrolHarnessPage().SendL1T();
        }

        [Test]
        public void SendL1Opt()
        {
            loginPageOP().Login();
            enrolHarnessPage().SendL1Opt();
        }

        [Test]
        public void Send1()
        {
            loginPageOP().Login();
            enrolHarnessPage().SendL1();
        }
    }
}
