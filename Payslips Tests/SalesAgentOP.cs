﻿using NUnit.Framework;
using qa_selenium_tests;
using qa_selenium_framework.Data;
using System.Windows;

namespace Payslips_Tests
{
    [TestFixture]

    class SalesAgentOP : BaseTestClass
    {
        [Test]
        public void CreateAccountAllModules()
        {
            var tenant = TenantOP.GetDefault();
            loginPageOP().SalesLogin();
            salesAgentPageOP().CreateDefaultAccount();
        }

        [Test]
        public void RPEOnlyInvoice()
        {
            var t = TenantOP.RPEOnly();
            loginPageOP().SalesLogin();
            salesAgentPageOP().CreateAccount(t);
            invoiceHarnessPage().RunIBSLInvoiceForTenant(t,"04/11/2016");
        }

        [Test]
        public void AllModulesInvoiceIBSL()
        {
            var t = TenantOP.GetDefault();
            loginPageOP().SalesLogin();
            salesAgentPageOP().CreateAccount(t);
            invoiceHarnessPage().RunIBSLInvoiceForTenant(t, "04/11/2016");
        }

        [Test]
        public void AllModulesInvoiceIntex()
        {
            var t = TenantOP.GetDefault();
            loginPageOP().SalesLogin();
            salesAgentPageOP().CreateAccount(t);
            invoiceHarnessPage().RunIntexInvoiceForTenant(t, "04/11/2016");
        }

        [Test]
        public void Test()
        {
            var tenant = TenantOP.GetDefault();
            tenant.AdminEmail = "Nathan";
            loginPageOP().AdminLogin();
            adminPageOP().setIBSL(tenant);
        }
    }


}
