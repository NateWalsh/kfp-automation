﻿using NUnit.Framework;
using qa_selenium_framework;
using qa_selenium_framework.Elements;
using qa_selenium_framework.PageObjects;
using System;
using System.Configuration;

namespace qa_selenium_tests
{
    [TestFixture]
    public class BaseTestClass
    {
        public BaseTestClass()
        {
            string usr = ConfigurationManager.AppSettings["user"];
            string env = ConfigurationManager.AppSettings["environment"];
            string bwr = ConfigurationManager.AppSettings["browser"];

            try
            {
                user = (TestUser)Enum.Parse(typeof(TestUser), usr);
                environment = (TestEnvironment)Enum.Parse(typeof(TestEnvironment), env);
                browser = (TestBrowser)Enum.Parse(typeof(TestBrowser), bwr);
            }
            catch
            {
                //If there error, set default values
                user = TestUser.Nate;
                environment = TestEnvironment.Stage;
            }
        }

        protected static TestUser user;
        protected static TestEnvironment environment;
        protected static TestBrowser browser;
        protected static string nameOfCurrentTest = string.Empty;
        protected static bool useAdminAccount = true;
        private static string logMessage = string.Empty;

        #region Setup
        [OneTimeSetUp]
        public static void RunBeforeAllTests()
        {
            //Driver.initialise(user, environment, browser);
        }
        [OneTimeTearDown]
        public static void RunAfterAllTests()
        {
            //Driver.close();
        }
        [SetUp]
        public static void RunBeforeEachTest()
        {
            Driver.initialise(user, environment, browser);
        }
        [TearDown]
        public static void RunAfterEachTest()
        {
            //Driver.close();
        }

        #endregion

        #region Page References

        public AdminPageOP adminPageOP()
        {
            var adminPageOP = new AdminPageOP();
            return adminPageOP;
        }

        public AdminElementsOP _adminPageOP
        {
            get
            {
                return new AdminElementsOP();
            }
        }

        public InvoiceHarnessPage invoiceHarnessPage()
        {
            var invoiceHarnessPage = new InvoiceHarnessPage();
                return invoiceHarnessPage;
        }

        public LoginPageOP loginPageOP()
        {
            var loginPageOP = new LoginPageOP();
            return loginPageOP;
        }

        public LoginElementsOP _loginPageOP
        {
            get
            {
                return new LoginElementsOP();
            }
        }

        public SalesAgentPageOP salesAgentPageOP()
        {
            var salesAgentPageOP = new SalesAgentPageOP();
            return salesAgentPageOP;
        }

        public SalesAgentElementsOP _salesAgentPageOP
        {
            get
            {
                return new SalesAgentElementsOP();
            }
        }

        public EnrolHarnessPage enrolHarnessPage()
        {
            var enorolHarnessPage = new EnrolHarnessPage();
            return enorolHarnessPage;
        }

        public GmailPageElements _gmailPage
        {
            get
            {
                return new GmailPageElements();
            }
        }

        public GmailPage gmailPage()
        {
            var gmailPage = new GmailPage();
            return gmailPage;
        }

        #endregion


    }

}
