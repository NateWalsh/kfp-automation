﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using qa_selenium_tests;
using qa_selenium_framework.Data;
using qa_selenium_framework;

namespace Payslips_Tests 
{
    [TestFixture]
    [Category("OpenPayslips")]

    class OPTests : BaseTestClass
    {
        [Test]
        public void LoginAsEmployeeWithEnrolLetter()
        {
            loginPageOP().Login();
            enrolHarnessPage().LogInAsEmp(EnrolTemplates.GetL1Opt());
        }
    }
}
