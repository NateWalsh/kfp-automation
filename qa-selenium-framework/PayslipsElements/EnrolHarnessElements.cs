﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qa_selenium_framework
{
    public class EnrolHarnessElements
    {
       
        public IWebElement UserName
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("UserName"));
            }
        }

        public IWebElement Password
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("Password"));
            }
        }

        public IWebElement EmployeeFirstName
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("EmployeeFirstName"));
            }
        }

        public IWebElement EmployeeLastName
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("EmployeeLastName"));
            }
        }

        public IWebElement EmployeeEmail
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("EmployeeEmail"));
            }
        }

        public IWebElement CompanyName
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("CompanyName"));
            }
        }

        public IWebElement Nino
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("Nino"));
            }
        }

        public IWebElement EmployeeCode
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("EmployeeCode"));
            }
        }

        public IWebElement TemplateCode
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("TemplateCode"));
            }
        }

        public IWebElement XmlData
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("XmlData"));
            }
        }

        public IWebElement iterations
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[name='iterations'"));
            }
        }

        public IWebElement SubmitButton
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector(".btn"));
            }
        }

        public IWebElement Complete
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("body"));
            }
        }

    }
}
