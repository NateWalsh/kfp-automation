﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qa_selenium_framework
{
    public class InvoiceHarnessElements
    {
       
        public IWebElement singleInvoiceLink
        {
            get
            {
                return Driver.Instance.FindElement(By.PartialLinkText("Generate And Email A Single Account's Invoice For A Month"));
            }
        }

        public IWebElement accountId
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[name='accountId']"));
            }
        }

        public IWebElement billingMonth
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[name='billingMonth']"));
            }
        }

        public IWebElement generateButton
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[type='submit']"));
            }
        }


    }
}
