﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qa_selenium_framework
{
    public class SalesAgentElementsOP
    {
       
        public IWebElement SalesAgentName
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("SalesAgentName"));
            }
        }

        public IWebElement SalesAgentEmail
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("SalesAgentEmail"));
            }
        }

        public IWebElement isPartner
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("isPartner"));
            }
        }

        public IWebElement pivRef
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("CustomerRefNo"));
            }
        }

        public IWebElement OrganisationName
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("OrganisationName"));
            }
        }

        public IWebElement Address1
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("Address1"));
            }
        }

        public IWebElement Address2
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("Address2"));
            }
        }

        public IWebElement Town
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("Town"));
            }
        }

        public IWebElement Postcode
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("Postcode"));
            }
        }

        public IWebElement CustomerEmail
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("CustomerEmail"));
            }
        }

        public IWebElement CustomerName
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("CustomerName"));
            }
        }

        public IWebElement CustomerSurname
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("CustomerSurname"));
            }
        }

        public IWebElement TelephoneNumber
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("TelephoneNumber"));
            }
        }

        public IWebElement customerType
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("customerType"));
            }
        }

        public IWebElement PaymentOptions
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("contractType"));
            }
        }

        public IWebElement OpModuleSelected
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("OpModuleSelected"));
            }
        }

        public IWebElement OpModuleDiscount
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("OpModuleDiscount"));
            }
        }

        public IWebElement AeModuleSelected
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("AeModuleSelected"));
            }
        }

        public IWebElement AeModuleDiscount
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("AeModuleDiscount"));
            }
        }

        public IWebElement OeModuleSelected
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("OeModuleSelected"));
            }
        }

        public IWebElement OeModuleDiscount
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("OeModuleDiscount"));
            }
        }

        public IWebElement RpeModuleSelected
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("RpeModuleSelected"));
            }
        }

        public IWebElement RpeModuleDiscount
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("RpeModuleDiscount"));
            }
        }

        public IWebElement OpNumberOfEmployees
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("OpNumberOfEmployees"));
            }
        }

        public IWebElement AeNumberOfEmployees
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("AeNumberOfEmployees"));
            }
        }

        public IWebElement OeNumberOfEmployees
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("OeNumberOfEmployees"));
            }
        }

        public IWebElement RpeCompanies
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("RpeCompanies"));
            }
        }

        public IWebElement StartDate
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("StartDate"));
            }
        }

        public IWebElement AeStartDate
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("AeStartDate"));
            }
        }

        public IWebElement OeStartDate
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("OeStartDate"));
            }
        }

        public IWebElement RpeStartDate
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("RpeStartDate"));
            }
        }

        public IWebElement calculate
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("calculate-button"));
            }
        }

        public IWebElement nextButtonSales
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("nextButtonSales"));
            }
        }

        public IWebElement firstAuthorisationTickbox
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("firstAuthorisationTickbox"));
            }
        }

        public IWebElement secondAuthorisationTickbox
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("secondAuthorisationTickbox"));
            }
        }

        public IWebElement NameOfBank
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("DirectDebitDetails.NameOfBank"));
            }
        }

        public IWebElement NameOfAccountHolder
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("DirectDebitDetails.NameOfAccountHolder"));
            }
        }

        public IWebElement AccountNumber
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("DirectDebitDetails.AccountNumber"));
            }
        }

        public IWebElement sortcode1
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[name='sortcode1'"));
            }
        }

        public IWebElement sortcode2
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[name='sortcode2'"));
            }
        }

        public IWebElement sortcode3
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[name='sortcode3'"));
            }
        }

        public IWebElement sendCustomerDetailsBtn
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("sendCustomerDetailsBtn"));
            }
        }

        public IWebElement verify
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("verify-ok"));
            }
        }

        public IWebElement opTermsLink
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("tnclink"));
            }
        }

        public IWebElement opTermsAccept
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("tnctickbox"));
            }
        }

        public IWebElement rpeTermsLink
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("tnclinkrpe"));
            }
        }

        public IWebElement rpeTermsAccept
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("tnctickboxrpe"));
            }
        }

        public IWebElement confirmContractButton
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("confirmButton-review"));
            }
        }


        public IWebElement AccountCreatedButton
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector(".widget-content.btn"));
            }
        }

        public IWebElement newPassword
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("NewPassword"));
            }
        }

        public IWebElement confirmPassword
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("ConfirmPassword"));
            }
        }

        public IWebElement setPasswordButton
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("btnSubmit"));
            }
        }
        








    }
}
