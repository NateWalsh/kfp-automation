﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qa_selenium_framework
{
    public class AdminElementsOP
    {
       
        public IWebElement clientSearch
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("companyName"));
            }
        }

        public IWebElement accountId
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("AccountId"));
            }
        }

        public IWebElement clientSearchButton
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("searchCompanyButton"));
            }
        }

        public IWebElement editclientButton
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector(".t-grid-edit"));
            }
        }

        public IWebElement ibsl
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("ibsl"));
            }
        }

        public IWebElement intex
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("intex"));
            }
        }


        public IWebElement updateClientButton
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector(".t-grid-update"));
            }
        }





    }
}
