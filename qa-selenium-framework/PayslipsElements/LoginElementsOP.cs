﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qa_selenium_framework
{
    public class LoginElementsOP
    {
       
        public IWebElement username
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("UserName"));
            }
        }

        public IWebElement password
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("Password"));
            }
        }

        public IWebElement NewPassword
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("NewPassword"));
            }
        }

        public IWebElement ConfirmPassword
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("ConfirmPassword"));
            }
        }

        public IWebElement submit
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[type='submit']"));
            }
        }

        public IWebElement loginButton
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[value='Log in to openPAYE']"));
            }
        }
        

        public IWebElement submitSales
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector(".btn-confirm"));
            }
        }

        public IWebElement logOffAndProceed
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector(".widget-content .btn"));
            }
        }
        
    }
}
