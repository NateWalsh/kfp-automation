﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qa_selenium_framework.Data
{
    public class Tenant
    {
        public static UserCredentials User
        {
            get
            {
                return LoginDetails.GetUserCredentials(Driver.UserContext.User, Driver.UserContext.Environment);
            }
        }

        public static Tenant GetDefault()
        {

            string[] emailSplit = User.GmailEmail.Split('@');

            return new Tenant
            {

                #region Client Details
                ExistingCustomer = false,
                FirstName = "Test",
                LastName = "Tenant",
                Email = emailSplit[0] + "+" + DateTime.Now.AddSeconds(1).ToString("ddMMyyyyHHmmss") + "@" + emailSplit[1],
                Password = "Welcome1!",
                CustomerType = CustomerType.Accountant,
                CompanyName = "Iris Testing",
                Address1 = "Iris Road",
                Address2 = "Towsnville",
                Town = "Iris World",
                Postcode = "WC2 1LE",
                Telephone = "01234 555333",
                #endregion
                #region Costs

                AE = true,
                AEStartDate = DateTime.Today,
                NumberOfCompanies = 9999,
                NumberOfEmployees = 9999,
                NumberOfESS = 9999,
                NumberOfAE = 9999,
                CostPerMonth = 0m,
                CostPerEmployee = 0m,
                CostPerAE = 0m,
                AnnualCost = 0m,

                #endregion
                #region Banking Information
                SortCode1 = "56",
                SortCode2 = "00",
                SortCode3 = "36",
                AccountName = "Monty Moneybags",
                AccountNumber = "40308669",

                #endregion

            };
        }

        public static Tenant GetAccountant()
        {
            Tenant accountant = new Tenant();
            accountant = GetDefault();
            accountant.CustomerType = CustomerType.Accountant;
            accountant.AE = false;
            return accountant;
        }

        public static Tenant GetAccountantWithAE()
        {
            Tenant accountant = new Tenant();
            accountant = GetDefault();
            accountant.CustomerType = CustomerType.Accountant;
            return accountant;
        }

        public static Tenant GetSME()
        {
            Tenant SME = new Tenant();
            SME = GetDefault();
            SME.CustomerType = CustomerType.SME;
            SME.AE = false;
            return SME;
        }

        public static Tenant GetSMEWithAE()
        {
            Tenant SME = new Tenant();
            SME = GetDefault();
            SME.CustomerType = CustomerType.SME;
            return SME;
        }

        #region Client Details
        public bool ExistingCustomer { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public CustomerType CustomerType { get; set; }
        public string CompanyName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Town { get; set; }
        public string Postcode { get; set; }
        public string Telephone { get; set; }
        #endregion
        #region Costs
        public bool AE { get; set; }
        public DateTime AEStartDate { get; set; }
        public int NumberOfCompanies { get; set; }
        public int NumberOfEmployees { get; set; }
        public int NumberOfESS { get; set; }
        public int NumberOfAE { get; set; }
        public decimal CostPerMonth { get; set; }
        public decimal CostPerEmployee { get; set; }
        public decimal CostPerAE { get; set; }
        public decimal AnnualCost { get; set; }
        #endregion
        #region Banking Information
        public string SortCode1 { get; set; }
        public string SortCode2 { get; set; }
        public string SortCode3 { get; set; }
        public string AccountName { get; set; }
        public string AccountNumber { get; set; }
        #endregion
    }
}
