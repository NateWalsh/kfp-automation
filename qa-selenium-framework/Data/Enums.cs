﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qa_selenium_framework
{
    public enum TestUser
    {
        Nate,
        Ram,
        Steve,
        Farzana
    }

    public enum TestEnvironment
    {
        Stage,
        Prod,
        Sprint
    }

    public enum TestBrowser
    {
        Firefox,
        Chrome,
        InternetExplorer,
        Remote

    }

    public enum BusinessType
    {
        Accountant = 104,
        Automative = 20,
        Business = 26,
        Engineering = 47,
        Financial = 50,
        Healthcare = 61,
        It = 34,
        Media = 89,
        None = 0,
        Other = 54,
    }

    public enum PaymentMethod
    {
        BACS = 4,
        Cash = 1,
        Cheque = 2,
        Other = 5
    }

    public enum PaymentType
    {
        Not_Selected = 0,
        A1_BACS = 1,
        A1_ACS_2000 = 5,
        A1_BACS_old = 4,
        Albany_RTI = 20,
        BACS_generic = 22,
    }

    public enum StarterDeclaration
    {
        A = 0,
        B = 1,
        C = 2,
        Unknown = 3,
    }

    public enum Title
    {
        Mr = 5,
        Ms = 7,
        Mrs = 6,
        Miss = 13,
        Dr = 8,
        Capt = 16,
        Prof = 17,
        Hon = 18,
        Jdge = 19,
        Rev = 20,
        Fr = 21,
        Str = 22,
        Sir = 11,
        Dame = 15,
        Lord = 12,
        Lady = 14,
        selected = 0,
    }

    public enum MaritalStatus
    {
        Single = 1,
        Married = 2,
        Divorced = 4,
        Widowed = 8,
        CivilParnership = 16
    }

    public enum Country
    {
        UnitedKingdom = 1,
        Jersey = 2,
        Guernsey = 7,
        IsleOfMan = 3,
        France = 4,
        Spain = 6,
        UnitedStates = 5
    }

    public enum NiCat
    {
        A = 19,
        B = 22,
        C = 23,
        D = 26,
        E = 27,
        H = 1071,
        I = 73,
        J = 24,
        K = 74,
        L = 28,
        M = 71,
        X = 66,
        Z = 72
    }

    public enum PayCycle
    {
        Weekly,
        Monthly
    }

    public enum StudentLoan
    {
        NoStudentLoan = 0,
        Plan1 = 1,
        Plan2 = 2,
    }

    public enum PayslipDelivery
    {
        ESS = 8,
        PDF = 1
    }

    public enum CustomerType
    {
        Accountant = 128,
        SME = 256
    }

    public enum Postponement
    {
        Weeks = 1,
        Months = 2,
        NoDefferal = 3,
        StartOfNextPayPeriod = 4,
        StartOfSecondPayPeriod = 5,
    }

    public enum PostponementType
    {
        NoDefferal,
        StartersAndStaging,
        EligibleJobholders,
        Both,
    }

    public enum OutputFile
    {
        NEST,
        NowPensions,
        PeoplesPension,
    }

    public enum PensionRule
    {
        ReliefAtSource,
        SalarySacrifice,
        NetPayArrangement,
    }

    public enum IncludeInOutputFile
    {
        AllEmployees = 3, 
        ProviderAndNoFund = 2,
        ProviderOnly = 1,
    }

    public enum WorkerStatus
    {
        None = 0,
        Excluded = 1,
        EligibleJobholder = 2,
        NonEligibleJobHolder = 3,
        EntitledWorker = 4,
    }

    public enum EnrolLetters
    {
        L0 = 1,
        L1 = 2, 
        L1Opt = 3,
        L1T = 4,
        L1P = 5,
        L2and3 = 6,
        L4 = 7,
        L6 = 8
    }

    public enum CommsStatus
    {
        Print = 1,
        Printed = 2,
        Published = 4,
        All = 7,
    }

}
