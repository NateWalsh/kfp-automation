﻿using OpenQA.Selenium;
using qa_selenium_framework.Elements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace qa_selenium_framework.Data
{
    public class PayItem: _pageBase
    {
        public static PayrollPageElements Elements
        {
            get
            {
                return new PayrollPageElements();
            }
        }



        public static void FindPayItem(PayItemName name)
        {
            switch (name)
            {
                case PayItemName.RefundTax:
                    Elements.Adjustment.ClickWait();
                    Elements.Tax.ClickWait();
                    Elements.RefundTax.ClickWait();
                    break;
                case PayItemName.OverrideTax:
                    Elements.Adjustment.ClickWait();
                    Elements.Tax.ClickWait();
                    Elements.OverrideTax.ClickWait();
                    break;

                case PayItemName.ErNICs:
                    Elements.Adjustment.ClickWait();
                    Elements.NICs.ClickWait();
                    Elements.ErNICs.ClickWait();
                    break;
                case PayItemName.RefundNICs:
                    Elements.Adjustment.ClickWait();
                    Elements.NICs.ClickWait();
                    Elements.RefundNICs.ClickWait();
                    break;
                case PayItemName.OverrideNICs:
                    Elements.Adjustment.ClickWait();
                    Elements.NICs.ClickWait();
                    Elements.OverrideNICs.ClickWait();
                    break;
                case PayItemName.OVerrideErNICs:
                    Elements.Adjustment.ClickWait();
                    Elements.NICs.ClickWait();
                    Elements.OverrideErNICs.ClickWait();
                    break;

                case PayItemName.MonthlySalary:
                    Elements.Basic.ClickWait();
                    Elements.Salary.ClickWait();
                    Elements.Monthly.ClickWait();
                    break;
            }

        }
        
    }


    public enum PayItemName
    {
        RefundTax,
        OverrideTax,
        MonthlySalary,

        ErNICs,
        RefundNICs,
        OverrideNICs,
        OVerrideErNICs,

    }
}
