﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qa_selenium_framework.Data
{
    public class Pension
    {
        #region AE Details
        //AE Details
        public bool AE { get; set; }
        public DateTime StagingDate { get; set; }
        public bool Postponement { get; set; }
        public PostponementType PostponementType { get; set; }
        public Postponement PostponementStarters { get; set; }
        public int PostponementStartersLegnth { get; set; }
        public Postponement PostponementJobholders { get; set; }
        public int PostponementJobholdersLegnth { get; set; }
        #endregion

        #region Output File Details
        //Output File Details
        public bool Outputfile { get; set; }
        public OutputFile OUtputfileType { get; set; }
        public string FileReference { get; set; }
        public bool WithholdContributions { get; set; }
        public int WithheldWeeks { get; set; }
        public string Group { get; set; }
        public string PaymentSource { get; set; }
        public string Category { get; set; }
        public string GroupID { get; set; }
        public bool AddEmployeeToEmployer { get; set; }
        #endregion

        #region Pension Scheme Details
        //Pension Scheme Details
        public string PensionName { get; set; }
        public string SCON { get; set; }
        public string UniqueID { get; set; }
        public string EmployeePortal { get; set; }
        public string Website { get; set; }
        public string PensionProviderAddress1 { get; set; }
        public string PensionProviderAddress2 { get; set; }
        public string PensionProviderPostcode { get; set; }
        public string PensionProviderTelephone { get; set; }
        public string PensionAdminName { get; set; }
        public string PensionAdminEmailAddress { get; set; }
        public string PensionAdminTelephoneNumber { get; set; }
        public string PensionAdminAddress1 { get; set; }
        public string PensionAdminAddress2 { get; set; }
        public string PensionAdminPostcode { get; set; }
        #endregion

        #region Pension Configuration
        //Pension Condifuration
        public PensionRule PensionRule { get; set; }
        public decimal EmployeeContribution { get; set; }
        public decimal EmployerContribution { get; set; }
        public bool QualifyingScheme { get; set; }
        public bool AEType { get; set; }
        public bool DefaultScheme { get; set; }
        public bool SubtractBasicRateTax { get; set; }
        public bool GenerateOutputFile { get; set; }
        public IncludeInOutputFile IncludeInOutputFile { get; set; }
        #endregion

        public static  Pension GetDefault()
        {
            return new Pension
            {
                AE = true,
                StagingDate = new DateTime(2016, 04, 01),
                PostponementType = PostponementType.NoDefferal,
                PostponementJobholders = qa_selenium_framework.Postponement.NoDefferal,
                PostponementJobholdersLegnth = 1,
                PostponementStarters = qa_selenium_framework.Postponement.NoDefferal,
                PostponementStartersLegnth = 1,
                Outputfile = false,
                PensionName = "MadeUp Pension",
                UniqueID = "ABC123",
                EmployeePortal = "EmployeePortal.com",
                Website = "PesnionWebsite.com",
                PensionProviderAddress1 = "1 Provider Road",
                PensionProviderAddress2 = "Provider Hill",
                PensionProviderPostcode = "PD1 1EY",
                PensionProviderTelephone = "01234 12345678",
                PensionAdminName = "Nate Walsh",
                PensionAdminEmailAddress = "Admin#Pension.com",
                PensionAdminTelephoneNumber = "012345 1234565678",
                PensionAdminAddress1 = "Admin Road",
                PensionAdminAddress2 = "Admin Hill",
                PensionAdminPostcode = "PA3 1EY",
                PensionRule = PensionRule.ReliefAtSource,
                EmployeeContribution = 3.00m,
                EmployerContribution = 4.00m,
                QualifyingScheme = true,
                AEType = true,
                DefaultScheme = true,
                SubtractBasicRateTax = false,
                GenerateOutputFile = false,
            };
        }

        public static Pension GetSalSac()
        {
            Pension pension = Pension.GetDefault();
            pension.PensionRule = PensionRule.SalarySacrifice;
            return pension;
        }

        public static Pension GetNetPayArrangement()
        {
            Pension pension = Pension.GetDefault();
            pension.PensionRule = PensionRule.NetPayArrangement;
            return pension;
        }

        public static Pension GetNonAE()
        {
            Pension p = Pension.GetDefault();
            p.PensionName = "OldStyle Pension";
            p.AE = false;
            p.DefaultScheme = false;
            p.AEType = false;
            return p;
        }

        public static Pension GetStarterPostponement()
        {
            Pension pension = Pension.GetDefault();
            pension.PostponementType = PostponementType.StartersAndStaging;
            pension.PostponementStarters = qa_selenium_framework.Postponement.Months;
            pension.PostponementStartersLegnth = 1;
            return pension;
        }

        public static Pension GetStarterPostponementInFuture()
        {
            Pension pension = Pension.GetStarterPostponement();
            pension.StagingDate = new DateTime(2016, 05, 01);
            return pension;
        }

        public static Pension GetEJPostponement()
        {
            Pension pension = Pension.GetDefault();
            pension.PostponementType = PostponementType.EligibleJobholders;
            pension.PostponementJobholders = qa_selenium_framework.Postponement.Months;
            pension.PostponementJobholdersLegnth = 1;
            return pension;
        }

        public static Pension GetEJPostponementInFuture()
        {
            Pension pension = Pension.GetEJPostponement();
            pension.StagingDate = new DateTime(2016, 05, 01);
            return pension;
        }

        public static Pension GetBothPostponemnet()
        {
            Pension pension = Pension.GetDefault();
            pension.PostponementType = PostponementType.Both;
            pension.PostponementJobholders = qa_selenium_framework.Postponement.Months;
            pension.PostponementJobholdersLegnth = 1;
            pension.PostponementStarters = qa_selenium_framework.Postponement.Months;
            pension.PostponementStartersLegnth = 1;
            return pension;
        }

        public static Pension GetBothPostponemnetInFuture()
        {
            Pension pension = Pension.GetBothPostponemnet();
            pension.StagingDate = new DateTime(2016, 05, 01);
            return pension;
        }

        public static Pension GetHasNEST()
        {
            Pension pension = Pension.GetDefault();
            pension.Outputfile = true;
            pension.OUtputfileType = OutputFile.NEST;
            pension.FileReference = "123456789";
            pension.WithholdContributions = false;
            pension.Group = "Group1";
            pension.PaymentSource = "PaymentSource1:";
            pension.AddEmployeeToEmployer = false;

            return pension;
        }

        public static Pension GetHasPeoples()
        {
            Pension pension = Pension.GetDefault();
            pension.Outputfile = true;
            pension.OUtputfileType = OutputFile.PeoplesPension;
            pension.FileReference = "12345";
            pension.WithholdContributions = false;
            pension.GroupID = "Group1";
            pension.AddEmployeeToEmployer = false;

            return pension;
        }

        public static Pension GetHasNOW()
        {
            Pension pension = Pension.GetDefault();
            pension.Outputfile = true;
            pension.OUtputfileType = OutputFile.NowPensions;
            pension.FileReference = "AB12";
            pension.Category = "Category1";
            pension.AddEmployeeToEmployer = false;

            return pension;
        }


    }
}
