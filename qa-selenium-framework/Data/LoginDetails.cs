﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qa_selenium_framework.Data
{
    public class LoginDetails
    {
        internal static UserCredentials GetUserCredentials(TestUser user, TestEnvironment environment)
        {
            UserCredentials userCredentials = new UserCredentials();


            switch (user)
            {
                case TestUser.Ram:

                    userCredentials.GmailEmail = "Rbarigala.iris+15082016@gmail.com";
                    userCredentials.GmailPassword = "Welcome1!@";

                    switch (environment)
                    {

                        case TestEnvironment.Stage:
                            userCredentials.UserName = "Rbarigala.iris+15082016@gmail.com";
                            userCredentials.AdminName = "Rbarigala.iris+15082016@gmail.com";
                            userCredentials.Password = "Welcome1@";
                            break;

                        case TestEnvironment.Prod:
                            userCredentials.UserName = "rbarigala.iris@gmail.com";
                            userCredentials.AdminName = "rbarigala.iris+sme4270@gmail.com";
                            userCredentials.Password = "Welcome1@";
                            break;
                        case TestEnvironment.Sprint:
                            userCredentials.UserName = "rbarigala.iris@gmail.com";
                            userCredentials.AdminName = "rbarigala.iris+sme4270@gmail.com";
                            userCredentials.Password = "Welcome1@";
                            break;
                    }
                    break;

                case TestUser.Nate:

                    userCredentials.GmailEmail = "nathanwalsh.iris@gmail.com";
                    userCredentials.GmailPassword = "Welcomez1!";

                    switch (environment)
                    {
                        case TestEnvironment.Prod:
                            userCredentials.UserName = "nathanwalsh.iris+qa@gmail.com";
                            userCredentials.AdminName = "nathan.walsh@iris.co.uk";
                            userCredentials.Password = "Welcome1!";
                            userCredentials.PayslipsUserName = "nathanwalsh.iris+testing@gmail.com";
                            userCredentials.PayslipsSales = "nathanwalsh.iris+irisid01@gmail.com";
                            userCredentials.PayslipsAdmin = "londondev";
                            userCredentials.PayslipsAdminPass = "OpenPAIN4Tina";

                            break;

                        case TestEnvironment.Stage:
                            userCredentials.UserName = "nathanwalsh.iris+qa@gmail.com";
                            userCredentials.AdminName = "nathan.walsh@iris.co.uk";
                            userCredentials.ESSEmail = "nathanwalsh.iris+ess@gmail.com";
                            userCredentials.Password = "Welcome1!";
                            userCredentials.PayslipsUserName = "nathanwalsh.iris+testing@gmail.com";
                            userCredentials.PayslipsAdmin = "tomcharries157+adminuser@gmail.com";
                            userCredentials.PayslipsAdminPass = "tom157";
                            userCredentials.PayslipsSales = "nathanwalsh.iris+irisid01@gmail.com";
                            break;

                        case TestEnvironment.Sprint:
                            userCredentials.UserName = "nathanwalsh.iris+qa@gmail.com";
                            userCredentials.AdminName = "nathan.walsh@iris.co.uk";
                            userCredentials.Password = "Welcome1!";
                            userCredentials.PayslipsUserName = "nathanwalsh.iris+testing@gmail.com";
                            break;
                    }
                    break;

                case TestUser.Steve:

                    userCredentials.GmailEmail = "nathanwalsh.iris@gmail.com";
                    userCredentials.GmailPassword = "Welcomez1!";

                    switch (environment)
                    {
                        case TestEnvironment.Prod:
                            userCredentials.UserName = "stephen.clarke@iris.co.uk";
                            userCredentials.AdminName = "stephen.clarke@iris.co.uk";
                            userCredentials.Password = "Waspstar01!";
                            break;

                        case TestEnvironment.Stage:
                            userCredentials.UserName = "stephen.clarke@iris.co.uk";
                            userCredentials.AdminName = "stephen.clarke@iris.co.uk";
                            userCredentials.Password = "Waspstar01!";
                            break;
                    }
                    break;

                case TestUser.Farzana:

                    userCredentials.GmailEmail = "farzana.ahmed2492@gmail.com";
                    userCredentials.GmailPassword = "k@shflow";

                    switch (environment)
                    {
                        case TestEnvironment.Prod:
                            userCredentials.UserName = "farzana.ahmed@iris.co.uk";
                            userCredentials.AdminName = "farzana.ahmed@iris.co.uk";
                            userCredentials.Password = "farzana123$";
                            break;

                        case TestEnvironment.Stage:
                            userCredentials.UserName = "farzana.ahmed2492@gmail.com";
                            userCredentials.AdminName = "farzana.ahmed2492@gmail.com";
                            userCredentials.Password = "k@shflow";
                            break;

                        case TestEnvironment.Sprint:
                            userCredentials.UserName = "farzana.ahmed2492+sprint2@gmail.com";
                            userCredentials.AdminName = "farzana.ahmed2492+sprint2@gmail.com";
                            userCredentials.Password = "farzana123$";
                            break;
                    }

                    break;


            }



            return userCredentials;
        }
    }

    public class UserCredentials
    {
        internal string UserName { get; set; }
        internal string AdminName { get; set; }
        internal string ESSEmail { get; set; }
        internal string Password { get; set; }
        internal string GmailEmail { get; set; }
        internal string GmailPassword { get; set; }
        internal string PayslipsUserName { get; set; }
        internal string PayslipsPassword { get; set; }
        internal string PayslipsAdmin { get; set; }
        internal string PayslipsAdminPass { get; set; }
        internal string PayslipsSales { get; set; }

    }
}
