﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qa_selenium_framework.Data
{
    public class Employee: _pageBase
    {

        public static Employee GetDefault()
        {

            string[] emailSplit = User.GmailEmail.Split('@');


            return new Employee
            {
                //The Basics
                FirstName = "John",
                LastName = "Doe",
                JoinDate = new DateTime(2016, 04, 01),
                NewToCompany = true,
                StarterDeclaration = StarterDeclaration.A,
                isDirector = false,

                Title = Title.Jdge,
                KnownAs = "Boaty McBoatface",
                Gender = false, //false = male
                DOB = new DateTime(1982, 04, 13),
                MaritalStatus = MaritalStatus.Married,

                Address1 = "1 Iris Road",
                Address2 = "Iris World",
                Postcode = "SE3 2UF",
                Country = Country.UnitedKingdom,
                WorkPhone = "123456",
                HomePhone = "654321",
                MobilePhone = "111222",

                TaxCode = "1100L",
                Cumulative = true,
                StudentLoan = StudentLoan.NoStudentLoan,
                NiNo = "BA482468A",
                NiCat = NiCat.A,

                PayCycle = PayCycle.Monthly,
                Intermittently = true,
                HoursPerWeek = "20",
                PaymentMethod = PaymentMethod.Other,
                PayslipDelivery = PayslipDelivery.PDF,
                PayslipEmail = "",
                BankAccountName = "Ronald Moneybags",
                SortCode = "123123",
                AccountNumber = "12344321",
                BACSRef = "B4C5ref",

            };

        }

        public static UserCredentials User
        {
            get
            {
                return LoginDetails.GetUserCredentials(Driver.UserContext.User, Driver.UserContext.Environment);
            }
        }

        public static Employee getWeekly()
        {
            Employee employee = Employee.GetDefault();
            employee.PayCycle = PayCycle.Weekly;
            return employee;
        }

        public static Employee getESSEmployee()
        {

            string[] emailSplit = User.GmailEmail.Split('@');

            Employee employee = Employee.GetDefault();
            employee.PayslipDelivery = PayslipDelivery.ESS;
            employee.PayslipEmail = emailSplit[0] + "+" + DateTime.Now.AddSeconds(1).ToString("ddMMyyyyHHmmss") + "@" + emailSplit[1];
            return employee;
        }

        public static Employee getFemale()
        {
            Employee employee = Employee.GetDefault();
            employee.FirstName = "Jane";
            employee.Gender = true;
            return employee;
        }

        public static Employee getDirector()
        {
            Employee employee = Employee.GetDefault();
            employee.isDirector = true;
            employee.DirectorStartDate = new DateTime(2016, 04, 01);
            employee.CumulativeNICs = true;
            return employee;
        }

        public static Employee getFromExcelSingle() 
        {
            DataSet Result = ReadExcelWithHeader("EmployeeData.xlsx");
            var row = Result.Tables[0].Rows[0];

            Employee employee = new Employee();

            populateEmployeeFromExcel(employee, row);

            return employee;

        }

        public static Employee getEmpByNumber(int EmployeeNumber)
        {
            Employee employee = Employee.GetDefault();
            employee.LastName = "Employee";
            employee.FirstName = "Number" + EmployeeNumber.ToString();
            return employee;
        }

        public static IList<Employee> getFromExcelMultiple()
        {
            DataSet Result = ReadExcelWithHeader("EmployeeData.xlsx");
            var rowCount = Result.Tables[1].Rows.Count;

            List<Employee> employees = new List<Employee>();

            for (int i = 0; i < rowCount; i++)
            {
                var row = Result.Tables[1].Rows[i];
                var employee = new Employee();

                populateEmployeeFromExcel(employee, row);
                
                employees.Add(employee);
            }
            return employees;
        }

        static void populateEmployeeFromExcel(Employee employee, DataRow row)
        {
            employee.FirstName = row[0].ToString();
            employee.LastName = row[1].ToString();
            employee.JoinDate = DateTime.Parse(row[2].ToString());
            employee.NewToCompany = (bool)row[3];
            employee.StarterDeclaration = (StarterDeclaration)Enum.Parse(typeof(StarterDeclaration), row[4].ToString(), false);
            employee.isDirector = (bool)row[5];
            if (employee.isDirector)
            {
                employee.DirectorStartDate = DateTime.Parse(row[6].ToString());
                employee.CumulativeNICs = (bool)row[7];
            }
            employee.Title = (Title)Enum.Parse(typeof(Title), row[8].ToString(), false);
            employee.KnownAs = row[9].ToString();
            employee.Gender = (bool)row[10];
            employee.DOB = DateTime.Parse(row[11].ToString());
            employee.MaritalStatus = (MaritalStatus)Enum.Parse(typeof(MaritalStatus), row[12].ToString(), false);
            employee.Address1 = row[13].ToString();
            employee.Address2 = row[14].ToString();
            employee.Postcode = row[15].ToString();
            employee.Country = (Country)Enum.Parse(typeof(Country), row[16].ToString(), false);
            employee.WorkPhone = row[17].ToString();
            employee.HomePhone = row[18].ToString();
            employee.MobilePhone = row[19].ToString();
            employee.TaxCode = row[20].ToString();
            employee.Cumulative = (bool)row[21];
            employee.StudentLoan = (StudentLoan)Enum.Parse(typeof(StudentLoan), row[22].ToString(), false);
            employee.NiNo = row[23].ToString();
            employee.NiCat = (NiCat)Enum.Parse(typeof(NiCat), row[24].ToString(), false);
            employee.PayCycle = (PayCycle)Enum.Parse(typeof(PayCycle), row[25].ToString(), false);
            employee.Intermittently = (bool)row[26];
            employee.HoursPerWeek = row[27].ToString();
            employee.PaymentMethod = (PaymentMethod)Enum.Parse(typeof(PaymentMethod), row[28].ToString(), false);
            employee.PayslipDelivery = (PayslipDelivery)Enum.Parse(typeof(PayslipDelivery), row[29].ToString(), false);
            employee.PayslipEmail = row[30].ToString();
            employee.BankAccountName = row[31].ToString();
            employee.SortCode = row[32].ToString();
            employee.AccountNumber = row[33].ToString();
            employee.BACSRef = row[34].ToString();
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime JoinDate { get; set; }
        public bool NewToCompany { get; set; }
        public StarterDeclaration StarterDeclaration { get; set; }
        public bool isDirector { get; set; }
        public DateTime DirectorStartDate { get; set; }
        public bool CumulativeNICs { get; set; }

        public Title Title { get; set; }
        public string KnownAs { get; set; }
        public bool Gender { get; set; }
        public DateTime DOB { get; set; }
        public MaritalStatus MaritalStatus { get; set; }

        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Postcode { get; set; }
        public Country Country { get; set; }
        public string WorkPhone { get; set; }
        public string HomePhone { get; set; }
        public string MobilePhone { get; set; }

        public string TaxCode { get; set; }
        public bool Cumulative { get; set; }
        public StudentLoan StudentLoan { get; set; }
        public string NiNo { get; set; }
        public NiCat NiCat { get; set; }

        public PayCycle PayCycle { get; set; }
        public bool Intermittently { get; set; }
        public string HoursPerWeek { get; set; }
        public PaymentMethod PaymentMethod { get; set; }
        public PayslipDelivery PayslipDelivery { get; set; }
        public string PayslipDeliveryMethod { get; set; }
        public string PayslipEmail { get; set; }
        public string BankAccountName { get; set; }
        public string SortCode { get; set; }
        public string AccountNumber { get; set; }
        public string BACSRef { get; set; }

    }


}

