﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qa_selenium_framework
{
    public class Company : _pageBase
    {
        static string uniqueString = " " + DateTime.Now.ToString() + DateTime.Now.Second;

        public static Company GetDefault()
        {
            return new Company
            {
                //The Basics
                CompanyName = "Monthly" + uniqueString,
                PreviouslyInRTI = true,
                HasWeekly = false,
                WeeklyPayDate = new DateTime(2016, 04, 06),
                HasMonthly = true,
                MonthlyPayDate = new DateTime(2016, 04, 30),

                //Commercial
                BusinessType = BusinessType.Accountant,
                Address1 = "1 Payroll Street",
                Address2 = "Iris World",
                Postcode = "WC1 2PL",
                Telephone = "01642 888555",

                //Government
                OfficeNumber = "049",
                RefNo = "Q1",
                AccountsOfficeRef = "F00037465",
                CorpTaxRef = "",
                SelfAssessmentRef = "",
                EmploymentAllowance = true,
                SmallRelief = true,
                RTI = false,
                OnlineFiling = true,
                PaymentMethod = PaymentMethod.Other,
                PaymentType = PaymentType.Not_Selected,
                BankAccountName = "Boaty McBoatFace",
                SortCode = "110011",
                AccountNumber = "12341234",
                BACSRef = "123REF",
            };
        }

        public static Company getMonthly()
        {
            Company monthly = new Company();
            monthly = GetDefault();
            return monthly;
        }

        public static Company getTestAccount()
        {
            Company monthly = new Company();
            monthly = GetDefault();
            monthly.CompanyName = "123TestAccount";
            return monthly;
        }

        public static Company getMonthlyRTI()
        {
            Company monthly = new Company();
            monthly = GetDefault();
            monthly.CompanyName = "MonthlyRTI" + uniqueString;
            monthly.RTI = true;
            return monthly;
        }

        public static Company getWeekly()
        {
            Company weekly = new Company();
            weekly = GetDefault();
            weekly.CompanyName = "Weekly" + uniqueString;
            weekly.HasWeekly = true;
            weekly.HasMonthly = false;
            return weekly;
        }

        public static Company getWeeklyRTI()
        {
            Company weekly = new Company();
            weekly = GetDefault();
            weekly.CompanyName = "WeeklyRTI" + uniqueString;
            weekly.HasWeekly = true;
            weekly.HasMonthly = false;
            weekly.RTI = true;
            return weekly;
        }

        public static Company getFromExcel()
        {
            DataSet Result = ReadExcelWithHeader("CompanyData.xlsx");
            var row = Result.Tables[0].Rows[0];
            
            Company company = new Company();
            company = GetDefault();

            company.CompanyName = row[0].ToString() + uniqueString;
            company.PreviouslyInRTI = (bool)row[1];
            company.HasWeekly = (bool)row[2];
            if (company.HasWeekly)
            {
                company.WeeklyPayDate = DateTime.Parse(row[3].ToString());
            }
            company.HasMonthly = (bool)row[4];
            if (company.HasMonthly)
            {
                company.MonthlyPayDate = DateTime.Parse(row[5].ToString());
            }

            company.BusinessType = (BusinessType) Enum.Parse(typeof(BusinessType), row[7].ToString(),true);
            company.Address1 = row[8].ToString();
            company.Address2 = row[9].ToString();
            company.Postcode = row[10].ToString();
            company.Telephone = row[11].ToString();

            company.OfficeNumber = row[13].ToString();
            company.RefNo = row[14].ToString();
            company.AccountsOfficeRef = row[15].ToString();
            company.CorpTaxRef = row[16].ToString();
            company.SelfAssessmentRef = row[17].ToString();
            company.EmploymentAllowance = (bool)row[18];
            company.SmallRelief = (bool)row[19];
            company.RTI = (bool)row[20];
            company.OnlineFiling = (bool)row[21];
            company.PaymentMethod = (PaymentMethod)Enum.Parse(typeof(PaymentMethod), row[22].ToString(), true);
            if (company.PaymentMethod == PaymentMethod.BACS)
            {
                company.PaymentType = (PaymentType)Enum.Parse(typeof(PaymentType), row[23].ToString(), true);
            }
            company.BankAccountName = row[24].ToString();
            company.SortCode = row[25].ToString();
            company.AccountNumber = row[26].ToString();
            company.BACSRef = row[27].ToString();

            return company;
        }

        #region The Basics
        //The Basics
        public string CompanyName { get; set; }
        public bool PreviouslyInRTI { get; set; }
        public bool HasWeekly { get; set; }
        public DateTime WeeklyPayDate { get; set; }
        public bool HasMonthly { get; set; }
        public DateTime MonthlyPayDate { get; set; }
        #endregion
        #region Commercial
        //Commercial
        public BusinessType BusinessType { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Postcode { get; set; }
        public string Telephone { get; set; }
        #endregion
        #region Government
        //Government
        public string OfficeNumber { get; set; }
        public string RefNo { get; set; }
        public string AccountsOfficeRef { get; set; }
        public string CorpTaxRef { get; set; }
        public string SelfAssessmentRef { get; set; }
        public bool EmploymentAllowance { get; set; }
        public bool SmallRelief { get; set; }
        public bool RTI { get; set; }
        public bool OnlineFiling { get; set; }
        #endregion

        public PaymentMethod PaymentMethod { get; set; } 
        public PaymentType PaymentType { get; set; }
        public string BankAccountName { get; set; }
        public string SortCode { get; set; }
        public string AccountNumber { get; set; }
        public string BACSRef { get; set; }
    }
}
