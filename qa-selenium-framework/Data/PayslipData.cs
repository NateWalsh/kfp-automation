﻿using qa_selenium_framework.Elements;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qa_selenium_framework.Data
{
    public class Payslip : _pageBase
    {
        public static PayrollPageElements Elements
        {
            get
            {
                return new PayrollPageElements();
            }
        }

        public decimal NetPay { get; set; }
        public decimal TaxableGross { get; set; }
        public decimal PAYETax { get; set; }
        public decimal NICS { get; set; }
        public decimal ErsNICS { get; set; }


        public static Payslip GetFromExcel(Employee emp, int Period)
        {
            DataSet Result = ReadExcelWithHeader("PayrollData.xlsx");
            DataRow row;
            if (emp.PayCycle == PayCycle.Monthly)
            {
                row = Result.Tables[0].Rows[Period];
            }
            else
            {
                row = Result.Tables[1].Rows[Period];
            }

            return new Payslip
            {
                NetPay = Decimal.Parse(row[16].ToString()),
                TaxableGross = Decimal.Parse(row[1].ToString()),
                PAYETax = Decimal.Parse(row[10].ToString()),
                NICS = Decimal.Parse(row[14].ToString()),
                ErsNICS = Decimal.Parse(row[15].ToString()),
            };
        }

        public static Payslip GetFromElements()
        {
            Elements.NetPayLabel.ClickWait();
            return new Payslip
            {
                NetPay = Decimal.Parse(Elements.NetPayLabel.Text, NumberStyles.Currency),
                TaxableGross = Decimal.Parse(Elements.TaxableGrossLabel.Text.Replace("Refund ", "-"), NumberStyles.Currency),
                PAYETax = Decimal.Parse(Elements.TaxLabel.Text.Replace("Refund ", "-"), NumberStyles.Currency),
                NICS = Decimal.Parse(Elements.NICsLabel.Text, NumberStyles.Currency),
                ErsNICS = Decimal.Parse(Elements.NICsErLabel.Text, NumberStyles.Currency)
            };
        }
    }

    public class AEThreshold : _pageBase
    {
        public decimal LEL { get; set; }
        public decimal AboveLEL { get; set; }
        public decimal Trigger { get; set; }
        public decimal AboveTrigger { get; set; }
        public decimal AboveUEL { get; set; }
        public decimal UEL { get; set; }

        public static AEThreshold Get2016()
        {
            DataSet Result = ReadExcelWithHeader("AE_Thresholds.xlsx");
            DataRowCollection rows;
            rows = Result.Tables[0].Rows;

            return new AEThreshold
            {
                LEL = Decimal.Parse(rows[0][1].ToString()),
                AboveLEL = Decimal.Parse(rows[0][1].ToString()) + 0.01m,
                Trigger = Decimal.Parse(rows[1][1].ToString()),
                AboveTrigger = Decimal.Parse(rows[1][1].ToString()) + 0.01m,
                UEL = Decimal.Parse(rows[2][1].ToString()),
                AboveUEL = Decimal.Parse(rows[2][1].ToString() + 0.01m),
            };
        }
    }
}

