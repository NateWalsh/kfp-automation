﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qa_selenium_framework.Data
{
    public class EnrolTemplates
    {
        public static UserCredentials User
        {
            get
            {
                return LoginDetails.GetUserCredentials(Driver.UserContext.User, Driver.UserContext.Environment);
            }
        }

        static EnrolTemplates GetDefault()
        {

            string[] emailSplit = User.GmailEmail.Split('@');

            return new EnrolTemplates
            {
                userName = User.PayslipsUserName,
                password = User.Password,
                firstName = "Nate",
                lastName = "L1" + DateTime.Now.AddSeconds(1).ToString("ddMMyyyyHHmmss"),
                email = emailSplit[0] + "+" + DateTime.Now.AddSeconds(1).ToString("ddMMyyyyHHmmss") + "@" + emailSplit[1],
                company = "Testco",
                nino = "AB123456C",
                employeeCode = "3",
                templateCode = TemplateCode.L1,
                xmlData = XMLs.L1,
                iterations = 1
            };
        }

        public static EnrolTemplates GetL1()
        {
            EnrolTemplates t = GetDefault();
            return t;
        }

        public static EnrolTemplates GetL1P()
        {
            EnrolTemplates t = GetDefault();
            t.lastName = "L1P" + DateTime.Now.AddSeconds(1).ToString("ddMMyyyyHHmmss");
            t.templateCode = TemplateCode.L1P;
            t.xmlData = XMLs.L1P;
            return t;
        }

        public static EnrolTemplates GetL1T()
        {
            EnrolTemplates t = GetDefault();
            t.lastName = "L1T" + DateTime.Now.AddSeconds(1).ToString("ddMMyyyyHHmmss");
            t.templateCode = TemplateCode.L1T;
            t.xmlData = XMLs.L1T;
            return t;
        }

        public static EnrolTemplates GetL1Opt()
        {
            EnrolTemplates t = GetDefault();
            t.lastName = "L1Opt" + DateTime.Now.AddSeconds(1).ToString("ddMMyyyyHHmmss");
            t.templateCode = TemplateCode.L1Opt;
            t.xmlData = XMLs.L1Opt;
            return t;
        }

        public static EnrolTemplates GetL2_3()
        {
            EnrolTemplates t = GetDefault();
            t.lastName = "L2_3" + DateTime.Now.AddSeconds(1).ToString("ddMMyyyyHHmmss");
            t.templateCode = TemplateCode.L2_3;
            t.xmlData = XMLs.L2_3;
            return t;
        }

        public static EnrolTemplates GetL4()
        {
            EnrolTemplates t = GetDefault();
            t.lastName = "L4" + DateTime.Now.AddSeconds(1).ToString("ddMMyyyyHHmmss");
            t.templateCode = TemplateCode.L4;
            t.xmlData = XMLs.L4;
            return t;
        }

        public static EnrolTemplates GetL6()
        {
            EnrolTemplates t = GetDefault();
            t.lastName = "L6" + DateTime.Now.AddSeconds(1).ToString("ddMMyyyyHHmmss");
            t.templateCode = TemplateCode.L6;
            t.xmlData = XMLs.L6;
            return t;
        }

        public static EnrolTemplates GetL0()
        {
            EnrolTemplates t = GetDefault();
            t.lastName = "L0" + DateTime.Now.AddSeconds(1).ToString("ddMMyyyyHHmmss");
            t.templateCode = TemplateCode.L0;
            t.xmlData = XMLs.L0;
            return t;
        }


        public string userName { get; set; }
        public string password { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string email { get; set; }
        public string company { get; set; }
        public string nino { get; set; }
        public string employeeCode { get; set; }
        public TemplateCode templateCode { get; set; }
        public string xmlData { get; set; }
        public int iterations { get; set; }
    }
}
