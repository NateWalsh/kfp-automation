﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qa_selenium_framework.Data
{
    public class TenantOP
    {
        public static UserCredentials User
        {
            get
            {
                return LoginDetails.GetUserCredentials(Driver.UserContext.User, Driver.UserContext.Environment);
            }
        }

        public static TenantOP GetDefault()
        {

            string[] emailSplit = User.GmailEmail.Split('@');

            return new TenantOP
            {

                #region Client Details
                isPartner = false,
                PivRef = "piv123",
                OrgName = "Banana Co.",
                Add1 = "1 Road",
                Add2 = "Place",
                Town = "Townsville",
                Postcode = "SE3 7PF",
                AdminEmail = emailSplit[0] + "+" + DateTime.Now.AddSeconds(1).ToString("ddMMyyyyHHmmss") + "@" + emailSplit[1],
                Password = User.Password,
                Forename = "Nate",
                Surname = "Walsh",
                Telephone = "01642888000",
                CustomerTypeOP = CustomerTypeOP.User,

                #endregion
                #region Charges

                PaymentOptions = PaymentOption.PrePaid,
                OPSelected = true,
                AESelected = true,
                OESelected = true,
                RPESelected = true,
                OPDiscount = 0m,
                AEDiscount = 0m,
                OEDiscount = 0m,
                RPEDiscount = 0m,
                NumberOfOP = 100,
                NumberOfAE = 100,
                NumberOfOE = 100,
                NumberOfRPE = 100,
                OPContractStart = DateTime.Now,
                AEContractStart = DateTime.Now,
                OEContractStart = DateTime.Now,
                RPEContractStart = DateTime.Now,

                #endregion
                #region Banking Information
                SortCode1 = "56",
                SortCode2 = "00",
                SortCode3 = "36",
                AccountName = "Monty Moneybags",
                AccountNumber = "40308669",
                BankName = "National Bank"

                #endregion

            };

        }

        public static TenantOP RPEOnly()
        {
            TenantOP t = GetDefault();
            t.OPSelected = false;
            t.AESelected = false;
            t.OESelected = false;
            return t;
        }

        public static TenantOP OPOnly()
        {
            TenantOP t = GetDefault();
            t.RPESelected = false;
            t.AESelected = false;
            t.OESelected = false;
            return t;
        }

        public static TenantOP AEOnly()
        {
            TenantOP t = GetDefault();
            t.RPESelected = false;
            t.OPSelected = false;
            t.OESelected = false;
            return t;
        }

        #region Client Details
        public bool isPartner { get; set; }
        public string PivRef { get; set; }
        public string OrgName { get; set; }
        public string Add1 { get; set; }
        public string Add2 { get; set; }
        public string Town { get; set; }
        public string Postcode { get; set; }
        public string AdminEmail { get; set; }
        public string Password { get; set; }
        public string Forename { get; set; }
        public string Surname { get; set; }
        public string Telephone { get; set; }
        public CustomerTypeOP CustomerTypeOP {get;set;}
        #endregion
        #region Charges
        public string PaymentOptions { get; set; }
        public bool OPSelected { get; set; }
        public decimal OPDiscount { get; set; }
        public bool AESelected { get; set; }
        public decimal AEDiscount { get; set; }
        public bool OESelected { get; set; }
        public decimal OEDiscount { get; set; }
        public bool RPESelected { get; set; }
        public decimal RPEDiscount { get; set; }
        public int NumberOfOP { get; set; }
        public int NumberOfAE { get; set; }
        public int NumberOfOE { get; set; }
        public int NumberOfRPE { get; set; }
        public DateTime OPContractStart { get; set; }
        public DateTime AEContractStart { get; set; }
        public DateTime OEContractStart { get; set; }
        public DateTime RPEContractStart { get; set; }

        #endregion
        #region Banking Information
        public string SortCode1 { get; set; }
        public string SortCode2 { get; set; }
        public string SortCode3 { get; set; }
        public string BankName { get; set; }
        public string AccountName { get; set; }
        public string AccountNumber { get; set; }
        #endregion
    }
}
