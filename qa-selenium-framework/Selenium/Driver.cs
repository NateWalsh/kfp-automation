﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qa_selenium_framework
{
    public class Driver
    {
        public static IWebDriver Instance { get; set; }

        public static void initialise(TestUser user, TestEnvironment environment, TestBrowser browser)
        {
            UserContext.User = user;
            UserContext.Environment = environment;
            UserContext.Browser = browser;



            switch (environment)
            {

                case TestEnvironment.Prod:
                    baseAddress = @"https://go.kashflowpayroll.com";
                    baseAddressOP = @"https://www.irisopenpayslips.co.uk";
                    baseAddressOE = @"http://www.irisopenenrol.co.uk";
                    break;

                case TestEnvironment.Stage:
                    baseAddress = @"https://trunk-go.kashflowpayroll.com";
                    baseAddressOP = @"https://openpayslips-trunk.project-aurora.co.uk";
                    baseAddressOE = @"http://staging.irisopenenrol.co.uk";
                    break;

                case TestEnvironment.Sprint:
                    baseAddress = @"https://sprint-go.kashflowpayroll.com/";
                    break;

                default:
                    baseAddress = String.Empty;
                    break;
            }

            var options = new ChromeOptions();
            options.AddUserProfilePreference("download.default_directory", @"C:\Downloads");

            switch (browser)
            {
                case TestBrowser.Firefox:
                    Instance = new FirefoxDriver();
                    break;
                case TestBrowser.Chrome:
                    Instance = new ChromeDriver(@"C:\Users\NathanWalsh\Documents\qa-selenium-framework\qa-selenium-framework");
                    break;
                case TestBrowser.InternetExplorer:
                    Instance = new InternetExplorerDriver();
                    break;
                case TestBrowser.Remote:
                    Instance = new RemoteWebDriver(new Uri("http://localhost:4444/wd/hub"), options.ToCapabilities(), new TimeSpan(0, 172, 30));
                    break;
            }

            Instance.Manage().Window.Maximize();
            Instance.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(5));
        }

        public static void close()
        {
            Instance.Close();
        }

        private static string baseAddress = String.Empty;

        internal static string BaseAddress
        {
            get { return baseAddress; }
        }

        private static string baseAddressOP = String.Empty;

        internal static string BaseAddressOP
        {
            get { return baseAddressOP; }
        }

        private static string baseAddressOE = String.Empty;

        internal static string BaseAddressOE
        {
            get { return baseAddressOE; }
        }



        public class UserContext
        {
            private static string url;

            public static TestUser User { get; set; }
            public static TestEnvironment Environment { get; set; }
            public static TestBrowser Browser { get; set; }

            public static int CompanyId { get; set; }
            public static string CompanyName { get; set; }



            public static void SetCurrentUrl()
            {
                url = Driver.Instance.Url;
            }

            public static void ReturnToPreviousUrl()
            {
                Driver.Instance.Navigate().GoToUrl(url);
            }

        }
    }
}
