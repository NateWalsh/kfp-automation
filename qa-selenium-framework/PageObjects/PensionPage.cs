﻿using OpenQA.Selenium;
using qa_selenium_framework.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace qa_selenium_framework.PageObjects
{
    public class PensionPage: _pageBase
    {

        #region Pension Navigation
        PensionPage GoToPensionWizard()
        {
            _pensionPage.PensionMenu.ClickWait();
            _pensionPage.AESetup.ClickWait();
            return this;
        }

        PensionPage GoToAddNewPension()
        {
            _pensionPage.PensionMenu.ClickWait();
            _pensionPage.AddNewPension.ClickWait();
            return this;
        }

        PensionPage GoToAllPensions()
        {
            _pensionPage.PensionMenu.ClickWait();
            _pensionPage.AllPensionSchemes.ClickWait();
            return this;
        }

        PensionPage GoToOutputFile()
        {
            _pensionPage.PensionMenu.ClickWait();
            _pensionPage.SetupDetails.ClickWait();
            _pensionPage.OutputfileTab.ClickWait();
            
            return this;
        }

        PensionPage GoToAE()
        {
            _pensionPage.PensionMenu.ClickWait();
            _pensionPage.SetupDetails.ClickWait();
            _pensionPage.AETab.ClickWait();
            return this;
        }

        #endregion

        PensionPage AEAndOutputFile(Pension p)
        {
            if (p.AE)
            {
                AddStagingDate(p);
            }
            if (p.Outputfile)
            {
                AddOutputFile(p);
            }
            return this;
        }

        public PensionPage AddStagingDate(Pension p)
        {
            GoToAE();
            CompleteStagingDate(p);
            CompletePostponement(p);
            return this;
        }

        public PensionPage AddOutputFile(Pension p)
        {
            GoToOutputFile();
            CompleteOutputFile(p);
            return this;
        }

        public PensionPage CreateStagingDateInFuture()
        {
            Pension p = Pension.GetDefault();
            p.StagingDate = new DateTime(2016, 05, 01);
            CreatePension(p);
            return this;
        }

        public PensionPage CreateDefaultStagingNoPension()
        {
            Pension p = Pension.GetDefault();
            AddStagingWithoutPension(p);
            return this;
        }

        public PensionPage CreateDefaultPension()
        {
            Pension p = Pension.GetDefault();
            CreatePension(p);
            return this;
        }

        public PensionPage CreateCustomStaging(DateTime date)
        {
            Pension p = Pension.GetDefault();
            p.StagingDate = date;
            CreatePension(p);
            return this;
        }


        public PensionPage CreateNonAE()
        {
            Pension p = Pension.GetNonAE();
            CreatePension(p);
            return this;
        }

        public PensionPage CreateEJPostponementPension()
        {
            Pension p = Pension.GetEJPostponement();
            CreatePension(p);
            return this;
        }

        public PensionPage CreateEJPostponementPensionWithDate(DateTime date)
        {
            Pension p = Pension.GetEJPostponement();
            p.StagingDate = date;
            CreatePension(p);
            return this;
        }

        public PensionPage CreateStarterPostponementPensionWithDate(DateTime date)
        {
            Pension p = Pension.GetStarterPostponementInFuture();
            p.StagingDate = date;
            CreatePension(p);
            return this;
        }

        public PensionPage CreateStarterPostponementPension()
        {
            Pension p = Pension.GetStarterPostponement();
            CreatePension(p);
            return this;
        }

        public PensionPage CreateStarterPostponementInFuture()
        {
            Pension p = Pension.GetStarterPostponementInFuture();
            CreatePension(p);
            return this;
        }

        public PensionPage CreateEJPostponementInFuture()
        {
            Pension p = Pension.GetEJPostponementInFuture();
            CreatePension(p);
            return this;
        }

        public PensionPage CreateBothPostponement()
        {
            Pension p = Pension.GetBothPostponemnet();
            CreatePension(p);
            return this;
        }


        public PensionPage CreateBothPostponementInFuture()
        {
            Pension p = Pension.GetBothPostponemnetInFuture();
            CreatePension(p);
            return this;
        }

        public PensionPage CreateNestPension()
        {
            Pension p = Pension.GetHasNEST();
            CreatePension(p);
            return this;
        }

        public PensionPage CreateDefaultPensionWizard()
        {
            Pension pension = Pension.GetDefault();
            CompleteAEWizard(pension);

            return this;
        }

        public PensionPage CreatePension(Pension p)
        {
            GoToAddNewPension()
                .CompletePension(p)
                .CompletePensionConfiguration(p)
                .CompleteSchemeOptions(p)
                .SubmitPension()
                .AEAndOutputFile(p);
            return this;
        }

        public PensionPage AddStagingWithoutPension(Pension p)
        {
            AEAndOutputFile(p);
            return this;
        }

        public PensionPage CompleteAEWizard(Pension p)
        {
            GoToPensionWizard();
            _pensionPage.NextStep1.ClickWait();
            CompleteStagingDate(p).
                CompletePostponement(p)
                .CompleteOutputFile(p)
                .CompletePension(p)
                .CompletePensionConfiguration(p)
                .CompleteSchemeOptions(p)
                .CompletePensionableItems(p)
                .ExitAESetup();

            return this;
        }

        PensionPage CompleteStagingDate(Pension p)
        {
            _pensionPage.StagingDate.ClearAndKeys(p.StagingDate.ToShortDateString());
            if (IsElementPresent(By.CssSelector("[class='btn btn-primary ml10']")))
            {
                _pensionPage.Next.ClickWait();
            }

            return this; 
        }

        public PensionPage CompletePostponement(Pension p)
        {
            if (p.PostponementType != PostponementType.NoDefferal)
            {
                if (IsElementPresent(By.CssSelector("[class='btn btn-primary ml10']")))
                {
                    _pensionPage.PostponementTrue.ClickWait();
                }

                    SelectByValue(_pensionPage.PostponementStarters, p.PostponementStarters);
                    _pensionPage.PostponementStartersValue.ClearAndKeys(p.PostponementStartersLegnth.ToString());

                    SelectByValue(_pensionPage.PostponementJobholders, p.PostponementJobholders);
                    _pensionPage.PostponementJobholdersValue.ClearAndKeys(p.PostponementJobholdersLegnth.ToString());
            }
            else
            {
                if (IsElementPresent(By.CssSelector("[class='btn btn-primary ml10']")))
                {
                    _pensionPage.PostponementFalse.ClickWait();
                }
            }

            if (IsElementPresent(By.CssSelector("[class='btn btn-primary ml10']")))
            {
                _pensionPage.Next.ClickWait();
            }

            else if (IsElementPresent(By.CssSelector("[type='submit']")))
            {
                _pensionPage.SubmitPension.ClickWait();
            }

            return this;
        }

        PensionPage CompleteOutputFile(Pension p)
        {
            if (p.Outputfile)
            {
                switch (p.OUtputfileType)
                {
                    case OutputFile.NEST:
                        SelectByText(_pensionPage.OutputFileType, "NEST");
                        if (p.WithholdContributions)
                        {
                            _pensionPage.WithholdingTrue.ClickWait();
                            _pensionPage.WithheldWeeks.SendKeysQuick(p.WithheldWeeks.ToString());
                        }

                        _pensionPage.Group.SendKeysQuick(p.Group);
                        _pensionPage.AddGroup.ClickWait();
                        _pensionPage.PaymentSource.SendKeysQuick(p.PaymentSource);
                        _pensionPage.AddPaymentSource.ClickWait();
                        break;

                    case OutputFile.NowPensions:
                        SelectByText(_pensionPage.OutputFileType, "Now Pensions");
                        _pensionPage.Category.SendKeysQuick(p.Category);
                        _pensionPage.AddCategory.ClickWait();

                        break;
                    case OutputFile.PeoplesPension:
                        SelectByText(_pensionPage.OutputFileType, "Peoples Pension");
                        if (p.WithholdContributions)
                        {
                            _pensionPage.WithholdingTrue.ClickWait();
                            _pensionPage.WithheldWeeks.SendKeysQuick(p.WithheldWeeks.ToString());
                        }

                        _pensionPage.WorkerGroupID.SendKeysQuick(p.GroupID);
                        _pensionPage.AddWorkerGroupID.ClickWait();
                        break;
                }

                _pensionPage.OutputFileReference.SendKeys(p.FileReference);

                if (p.AddEmployeeToEmployer)
                {
                    _pensionPage.AddEEToEr.ClickWait();
                }
            }

            if (IsElementPresent(By.CssSelector("[class='btn btn-primary ml10']")))
            {
                _pensionPage.Next.ClickWait();
            }

            else if(IsElementPresent(By.CssSelector("[type='submit']")))
            {
                _pensionPage.SubmitPension.ClickWait();
            }

            return this;
        }

        PensionPage CompletePension (Pension p)
        {
            Thread.Sleep(300);
            if(IsElementPresent(By.PartialLinkText("Add New")))
            {
                _pensionPage.AddNew.ClickWait();
            }
            _pensionPage.PensionName.SendKeysQuick(p.PensionName);
            if (p.SCON != null)
            {
                _pensionPage.SCON.SendKeysQuick(p.SCON);
            }
            _pensionPage.UniqueID.SendKeysQuick(p.UniqueID);
            _pensionPage.EmployeePortal.SendKeysQuick(p.EmployeePortal);
            _pensionPage.Website.SendKeysQuick(p.Website);
            _pensionPage.ProviderAddress1.SendKeysQuick(p.PensionProviderAddress1);
            _pensionPage.ProviderAddress2.SendKeysQuick(p.PensionProviderAddress2);
            _pensionPage.ProviderPostcode.SendKeysQuick(p.PensionProviderPostcode);
            if (IsElementPresent(By.Id("ProviderPhoneNumber")))
            {
                _pensionPage.WizardProviderPhoneNumber.SendKeysQuick(p.PensionProviderTelephone);
            }
            else
            {
                _pensionPage.PensionProviderPhoneNumber.SendKeysQuick(p.PensionProviderTelephone);
            }

            _pensionPage.AdminName.SendKeysQuick(p.PensionAdminName);
            _pensionPage.AdminEmail.SendKeysQuick(p.PensionAdminEmailAddress);
            _pensionPage.AdminPhoneNumber.SendKeysQuick(p.PensionAdminTelephoneNumber);
            _pensionPage.AdminAddress1.SendKeysQuick(p.PensionAdminAddress1);
            _pensionPage.AdminAddress2.SendKeysQuick(p.PensionAdminAddress2);
            _pensionPage.AdminPostcode.SendKeysQuick(p.PensionAdminPostcode);

            if (IsElementPresent(By.CssSelector("[class='btn btn-primary ml10']")))
            {
                _pensionPage.Next.ClickWait();
            }

            return this;
        }

        PensionPage CompletePensionConfiguration (Pension p)
        {
            switch (p.PensionRule)
            {
                case PensionRule.ReliefAtSource:
                    _pensionPage.ReliefAtSource.ClickWait();
                    break;
                case PensionRule.SalarySacrifice:
                    _pensionPage.SalarySacrifice.ClickWait();
                    break;
                case PensionRule.NetPayArrangement:
                    _pensionPage.NetPayArrangement.ClickWait();
                    break;
            }

            _pensionPage.EEContribution.ClearAndKeys(p.EmployeeContribution.ToString());
            _pensionPage.ERContribution.ClearAndKeys(p.EmployerContribution.ToString());

            if (IsElementPresent(By.CssSelector("[class='btn btn-primary ml10']")))
            {
                _pensionPage.Next.ClickWait();
            }
            return this;
        }

        PensionPage CompleteSchemeOptions (Pension p)
        {
            if (p.QualifyingScheme)
            {
                if (!_pensionPage.IsQualifying.Selected)
                {
                    _pensionPage.IsQualifying.ClickWait();
                }
                if (p.AEType)
                {
                    _pensionPage.AEType.ClickWait();
                }
                if (p.DefaultScheme)
                {
                    _pensionPage.Default.ClickWait();
                }
            }

            if (p.SubtractBasicRateTax)
            {
                _pensionPage.Default.ClickWait();
            }
            if (p.GenerateOutputFile)
            {
                _pensionPage.GenerateOutputFiles.ClickWait();
            }

            if (IsElementPresent(By.CssSelector("[class='btn btn-primary ml10']")))
            {
                _pensionPage.Next.ClickWait();
            }
            return this;
        }

        PensionPage CompletePensionableItems (Pension p)
        {
            _pensionPage.CompleteSetup.ClickWait();
            return this;
        }

        PensionPage ExitAESetup()
        {
            _pensionPage.ExitAESetup.ClickWait();
            return this;
        }

        PensionPage SubmitPension()
        {
            _pensionPage.SubmitPension.ClickWait();
            _pensionPage.ExitPensionSetup.ClickWait();
            return this;
        }

        public PensionPage UpdatePostponement(Pension p)
        {
            GoToAE();
            CompleteStagingDate(p);
            CompletePostponement(p);
            return this;
        }

        public PensionPage DeleteAllPensions()
        {
            var counter = 0;
            GoToAllPensions();
            if (IsElementPresent(By.CssSelector("[class='giant-choice']")) == false)
            {
                var numberOfPensions = _pensionPage.AllPensions.Count;
                while (counter < numberOfPensions)
                {
                    try
                    {
                        var deleteurl = _pensionPage.DeletePensionButton.GetAttribute("href");
                        Driver.Instance.Navigate().GoToUrl(deleteurl);

                        _pensionPage.ConfirmDeleteTickbox.ClickWait();
                        _pensionPage.ConfirmDeleteButton.ClickWait();
                    }
                    catch
                    {
                        Console.WriteLine("No Pensions to Delete");
                    }

                    if (_pensionPage.AllPensions.Count < numberOfPensions)
                    {
                        counter++;
                    }
                }
            }
                return this;
        }
    }
}
