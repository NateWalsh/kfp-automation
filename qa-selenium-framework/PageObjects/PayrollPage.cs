﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using qa_selenium_framework.Data;
using qa_selenium_framework.Elements;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace qa_selenium_framework.PageObjects
{
    public class PayrollPage : _pageBase
    {
        public PayrollPage GoToPayslips()
        {
            _payrollPage.PayrollMenu.ClickWait();
            _payrollPage.PayslipsAndPeriods.ClickWait();
            return this;
        }

        void PayCycleTab(Employee emp)
        {
            if (emp.PayCycle == PayCycle.Monthly)
            {
                _payrollPage.MonthlyPayTab.Click();
            }
            else
            {
                _payrollPage.WeeklyPayTab.Click();
            }

        }

        public PayrollPage RollBackWithName(string Name)
        {
            companyPage().SelectCompanyByName("AE Tests");
            payrollPage().RollRack();
            return this;
        }

        public PayrollPage RollRack()
        {
            GoToPayslips();
            _payrollPage.MonthlyPayTab.Click();
            RollbackIfPossible();
            // _payrollPage.WeeklyPayTab.Click();
            //RollbackIfPossible();
            return this;
        }

        PayrollPage RollbackIfPossible()
        {
            var rollbackPossible = true;
            while (rollbackPossible == true)
            {
                try
                {
                    if (IsElementPresent(By.PartialLinkText("Go to last finalised")))
                    {
                        _payrollPage.LastPeriodExist.Click();
                    }
                    if (IsElementPresent(By.PartialLinkText("«")))
                    {

                        var previousURL = _payrollPage.PreviousPeriod.GetAttribute("href");
                        Driver.Instance.Navigate().GoToUrl(previousURL);
                        _payrollPage.RestoreButton.Click();
                        _payrollPage.ConfirmRestoreCheckbox.Click();
                        _payrollPage.ConfirmRestoreButton.Click();
                    }
                    else
                    {
                        rollbackPossible = false;
                    }
                }
                catch
                {
                    rollbackPossible = false;
                }
            }
            return this;
        }

        public PayrollPage CreateAllPayslips(decimal[] Values)
        {
            int index = 0;

            GoToPayslips();

            var noOfEmps = _payrollPage.AllPayslips.Count;

            while (index < noOfEmps)
            {
                try
                {
                    if (index < Values.Length)
                    {
                        decimal value = Values[index];
                        if (value > 0)
                        {
                            _payrollPage.AllPayslips[index].ClickWait();
                            AddPayment(PayItemName.MonthlySalary, value);
                            ConfirmAndApprove();
                            GoToPayslips();
                        }
                    }


                }
                catch
                {
                }
                index++;
            }

            FinalisePayroll(); 
            Thread.Sleep(300);

            return this;
        }

        public PayrollPage CreateAllPayslips(decimal Value)
        {
            int index = 0;

            GoToPayslips();

            var noOfEmps = _payrollPage.AllPayslips.Count;

            while (index < noOfEmps)
            {
                try
                {
                    _payrollPage.AllPayslips[index].ClickWait();
                    AddPayment(PayItemName.MonthlySalary, Value);
                    ConfirmAndApprove();
                    GoToPayslips();
                }
                catch
                {
                }
                index++;
            }

            return this;
        }

        public PayrollPage CreateAllWeeklyPayslips(decimal Value)
        {
            int index = 0;

            GoToPayslips();

            var noOfEmps = _payrollPage.AllPayslips.Count;

            while (index < noOfEmps)
            {
                try
                {
                    _payrollPage.AllPayslips[index].ClickWait();
                    AddPayment(PayItemName.MonthlySalary, Value);
                    ConfirmAndApprove();
                    GoToPayslips();
                }
                catch
                {
                }
                index++;
            }

            return this;
        }

        public PayrollPage DeleteAllPayslips()
        {
            int index = 0;

            GoToPayslips();

            var noOfEmps = _payrollPage.DeleteAllPayslips.Count;

            while (index < noOfEmps)
            {
                try
                {
                    var deleteurl = _payrollPage.DeleteAllPayslips[0].GetAttribute("href");
                    Driver.Instance.Navigate().GoToUrl(deleteurl);

                    _payrollPage.ConfirmDeletePayslip.ClickWait();
                    _payrollPage.DeleteButton.ClickWait();
                }
                catch
                {
                }
                index++;
            }

            return this;
        }

        public PayrollPage OpenPayslip(Employee emp)
        {
            GoToPayslips().PayCycleTab(emp);

            var payslip = Driver.Instance.FindElement(By.XPath("//table[@id='paydate-items']//span[text()='" + emp.FirstName + " " + emp.LastName + "']"));

            payslip.Click();

            return this;
        }

        public PayrollPage AddPayments(List<Employee> Employees)
        {
            foreach (Employee employee in Employees)
            {
                OpenPayslip(employee);
                AddPayment(PayItemName.MonthlySalary, 1000.00m);
            }
            return this;
        }

        public PayrollPage AddPayment(PayItemName name, decimal value)
        {
            _payrollPage.AddNewPayment.ClickWait();
            PayItem.FindPayItem(name);
            if (IsElementPresent(By.CssSelector("tr:last-of-type [title = 'Repeats on following payslips. Click to end on this payslip.']")))
            {
                _payrollPage.RepeatOn.Click();
            }
            _payrollPage.Amount.SendKeys(Keys.End + Keys.Shift + Keys.Home + Keys.Delete);
            _payrollPage.Amount.SendKeys(value.ToString());


            _payrollPage.PayDateTitle.ClickWait();

            return this;
        }

        public PayrollPage ConfirmAndApprove()
        {
            _payrollPage.CreateSaveButton.ClickWait();
            if (IsElementPresent(By.CssSelector("[class='psl-approval-ctrl'] [class='icon-uncheck']")))
            {
                _payrollPage.ApprovePayslip.ClickWait();
                _payrollPage.CreateSaveButton.ClickWait();
            }
            _payrollPage.DoneSavingButton.ClickWait();
            return this;
        }

        public PayrollPage FinalisePayroll()
        {
            GoToPayslips();

            _payrollPage.FinaliseButton.ClickWait();
            if (IsElementPresent(By.Id("paydate-issues-rticrit")))
            {
                _payrollPage.CriticalError.Click();
                _payrollPage.ReasonForLateSubmission.ClickWait();
                _payrollPage.ReasonForLateOKButton.ClickWait();
            }
            else
            {
            }
            _payrollPage.ConfirmFinaliseButton.ClickWait();

            Thread.Sleep(300);
            try
            {
                _payrollPage.MonthlyPayTab.ClickWait();
            }
            catch
            {
                _payrollPage.ConfirmFinaliseButton.ClickWait();
                _payrollPage.MonthlyPayTab.ClickWait();
            }

            return this;
        }

        public PayrollPage RollBackPayTestCompany()
        {
            companyPage().OpenTestCompany();
            RollRack();
            employeePage().DeleteAllEmployees();

            return this;
        }

        public PayrollPage AssertResult(Employee emp, int Period)
        {
            Payslip payslip = Payslip.GetFromElements();
            Payslip payslipAssert = Payslip.GetFromExcel(emp, Period);

            Assert.AreEqual(payslip.NetPay, payslipAssert.NetPay);
            Assert.AreEqual(payslip.TaxableGross, payslipAssert.TaxableGross);
            Assert.AreEqual(payslip.PAYETax, payslipAssert.PAYETax);
            Assert.AreEqual(payslip.NICS, payslipAssert.NICS);
            Assert.AreEqual(payslip.ErsNICS, payslipAssert.ErsNICS);

            return this;
        }

        public PayrollPage PayMonthly(decimal Values)
        {

            GoToPayslips();
            _payrollPage.AllPayslips[0].ClickWait();
            AddPayment(PayItemName.MonthlySalary, Values);
            ConfirmAndApprove();
            FinalisePayroll();

            return this;
        }

        public PayrollPage PayMonthly(decimal[] Values)
        {
            int period = 0;
            while (period < Values.Length)
            {
                GoToPayslips();
                _payrollPage.AllPayslips[0].ClickWait();

                AddPayment(PayItemName.MonthlySalary, Values[period]);
                ConfirmAndApprove();
                FinalisePayroll();
                period++;
            }
            return this;
        }

        public PayrollPage PayAndAssertFromPayrollData(Employee emp)
        {
            DataSet Result = ReadExcelWithHeader("PayrollData.xlsx");
            int rowCount;
            if (emp.PayCycle == PayCycle.Monthly)
            {
                rowCount = Result.Tables[0].Rows.Count;
            }
            else
            {
                rowCount = Result.Tables[1].Rows.Count;
            }

            int period = 0;

            while (period < rowCount)
            {
                Payslip payslip = Payslip.GetFromExcel(emp, period);

                int maxPeriods;
                if (emp.PayCycle == PayCycle.Monthly) { maxPeriods = 12; }
                else { maxPeriods = 52; }

                GoToPayslips()
                    .OpenPayslip(emp)
                    .AddPayment(PayItemName.MonthlySalary, payslip.TaxableGross)
                    .ConfirmAndApprove()
                    .AssertResult(emp, period);
                if (period < maxPeriods - 1)
                {
                    FinalisePayroll();
                }

                period++;
            }
            return this;
        }
    }
}
