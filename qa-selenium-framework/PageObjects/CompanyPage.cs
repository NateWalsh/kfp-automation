﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qa_selenium_framework
{
    public class CompanyPage: _pageBase
    {

        CompanyPage GoToCreateCompany()
        {
            _companyPage.companyMenu.Click();
            _companyPage.addCompany.Click();
            return this;
        }

        CompanyPage GoToAllCurrentCompanies()
        {
            Driver.Instance.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(0));

            try
            {
                _companyPage.AllCompaniesTab.Click();
            }
            catch
            {
                _companyPage.companyMenu.Click();
                _companyPage.CompanyDashboard.Click();
                _companyPage.AllCompaniesTab.Click();
            }

            Driver.Instance.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(5));

            return this;
        }

        CompanyPage SelectCompanyByIndex(int index)
        {
            GoToAllCurrentCompanies();
            _companyPage.CompanyRows[index].ClickWait();
            return this;
        }

        public CompanyPage CreateCompanyByName(string Name)
        {
            Driver.Instance.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(2));
            bool CompanyExist = false;
            try
            {
                Dashboard.CompanyPicker.Click();
                Driver.Instance.FindElement(By.LinkText(Name)).ClickWait();
                CompanyExist = true;
                payrollPage().RollRack();
            }
            catch
            {
                CompanyExist = false;
            }
            if (CompanyExist == false)
            {
                CreateCompanyWithName(Name);
            }
            Driver.Instance.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(5));

            return this;
        }

        public CompanyPage SelectCompanyByName(string Name)
        {
            Driver.Instance.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(2));
            try
            {
                Dashboard.CompanyPicker.Click();
                Driver.Instance.FindElement(By.PartialLinkText(Name)).ClickWait();
            }
            catch
            {
            }

            Driver.Instance.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(5));

            return this;
        }

        public CompanyPage DeleteAllCompanies()
        {
            GoToAllCurrentCompanies();
            int index = 0;
            int companyCount = _companyPage.CompanyRows.Count();

            while (index < companyCount)
            {
                GoToAllCurrentCompanies();
                if (_companyPage.CompanyRows[index].Text == "Demo Company")
                {
                    index++;
                }
                else
                {
                    SelectCompanyByIndex(index);
                    payrollPage().RollRack();
                    employeePage().DeleteAllEmployees();
                    index++;
                }
            }

            DeleteCompany();

            return this;
        }

        CompanyPage DeleteCompany()
        {
            var deletePossible = true;

            while (deletePossible == true)
            {
                try
                {
                    GoToAllCurrentCompanies();

                    _companyPage.DeleteIcon.Click();
                    _companyPage.ConfirmDeleteCheckbox.ClickWait();
                    _companyPage.ConfirmDeleteButton.Click();
                }
                catch
                {
                    deletePossible = false;
                }
            }
            return this;
        }

        public CompanyPage DeleteCompanyByName(string companyName)
        {
            bool TestCompanyExist = false;
            try
            {
                SelectCompany(companyName);
                TestCompanyExist = true;
            }
            catch
            {
                TestCompanyExist = false;
            }
            if (TestCompanyExist)
            {
                string companyNumber = CompanyNumber();
                payrollPage().RollRack();
                employeePage().DeleteAllEmployees();
                GoToAllCurrentCompanies();
                _companyPage.DeleteWithCompanyNumber(companyNumber).Click();
                _companyPage.ConfirmDeleteCheckbox.ClickWait();
                _companyPage.ConfirmDeleteButton.Click();
            }

            return this;
        }

        public CompanyPage RollBackTestCompany()
        {
            OpenTestCompany();
            
            return this;

        }

        public CompanyPage DeleteTestCompany()
        {
            Company c = Company.getTestAccount();

            DeleteCompanyByName(c.CompanyName);

            return this;
        }

        public CompanyPage OpenTestCompany()
        {
            Company c = Company.getTestAccount();
            bool TestCompanyExist = false;
            try
            {
                Timeout(1);
                SelectCompany(c);
                TestCompanyExist = true;
                Timeout(5);
            }
            catch
            {
                TestCompanyExist = false;
            }
            if (TestCompanyExist == false)
            {
                CreateCompany(c);
            }
            return this;
        }

        public CompanyPage CreateDefaultCompany()
        {
            CreateCompany(Company.GetDefault());
            return this;
        }

        public CompanyPage CreateWeeklyCompany()
        {
            CreateCompany(Company.getWeekly());
            return this;
        }

        public CompanyPage CreateExcelCompany()
        {
            CreateCompany(Company.getFromExcel());
            return this;
        }

        CompanyPage CreateCompanyWithName(string CompanyName)
        {
            Company company = Company.GetDefault();
            company.CompanyName = CompanyName;
            CreateCompany(company);
            return this;
        }

        public CompanyPage CreateCompany(Company c)
        {

            GoToCreateCompany();
            AddBasics(c).AddCommercial(c).AddGovernment(c).AddPaymentOptions(c).AddPayrollControl(c).AddOpeningBalances(c);

            _companyPage.ExitCompanySetup.ClickWait();
            return this;
        }

        #region CompanyWizard
        CompanyPage AddBasics(Company c)
        {
            _companyPage.companyName.SendKeysQuick(c.CompanyName);
            _companyPage.PreviousRtiFalse.Click();
            if (c.HasWeekly)
            {
                _companyPage.HasWeeklyTrue.Click();
                _companyPage.WeeklyDate.SendKeysQuick(c.WeeklyPayDate.ToShortDateString());
            }
            else { _companyPage.HasWeeklyFalse.Click(); }

            if (c.HasMonthly)
            {
                _companyPage.HasMonthlyTrue.Click();
                _companyPage.MonthlyDate.SendKeysQuick(c.MonthlyPayDate.ToShortDateString());
            }
            else { _companyPage.HasMonthlyFalse.ClickWait(); }

            _companyPage.Step1Next.ClickWait();

            return this;
        }

        CompanyPage AddCommercial(Company c)
        {
            SelectByValue(_companyPage.BusinessType, c.BusinessType);
            _companyPage.Address1.SendKeysQuick(c.Address1);
            _companyPage.Address2.SendKeysQuick(c.Address2);
            _companyPage.Postcode.SendKeysQuick(c.Postcode);
            _companyPage.Telephone.SendKeysQuick(c.Telephone);
            _companyPage.Next.Click();

            return this;
        }

        CompanyPage AddGovernment(Company c)
        {
            _companyPage.OfficeNumber.SendKeysQuick(c.OfficeNumber);
            _companyPage.ReferenceNumber.SendKeysQuick(c.RefNo);
            _companyPage.AccountsOfficeRef.SendKeysQuick(c.AccountsOfficeRef);
            _companyPage.CorpTaxRef.SendKeysQuick(c.CorpTaxRef);
            _companyPage.SelfAssessmentRef.SendKeysQuick(c.SelfAssessmentRef);

            if (c.EmploymentAllowance) { _companyPage.EmployerAllowanceTrue.Click(); }
            else { _companyPage.EmployerAllowanceFalse.Click(); }

            if (c.SmallRelief) { _companyPage.SmallEmployerTrue.Click(); }
            else { _companyPage.SmallEmployerFalse.Click(); }

            if (c.RTI) { _companyPage.RTITrue.Click(); }
            else { _companyPage.RTIFalse.Click(); }

            if (c.OnlineFiling) { _companyPage.OnlineFilingKashFlow.Click(); }
            else { _companyPage.OnlineFilingOwnCreds.Click(); }

            _companyPage.Next.Click();

            return this;
        }

        CompanyPage AddPaymentOptions(Company c)
        {
            SelectByValue(_companyPage.PaymentMenthod, c.PaymentMethod);
            SelectByValue(_companyPage.ElectronicPayment, c.PaymentType);
            _companyPage.BankAccountName.SendKeysQuick(c.BankAccountName);
            _companyPage.SortCode.SendKeysQuick(c.SortCode);
            _companyPage.AccountNumber.SendKeysQuick(c.AccountNumber);
            _companyPage.BACSRef.SendKeysQuick(c.BACSRef);

            _companyPage.Next.Click();

            return this;
        }

        CompanyPage AddPayrollControl(Company c)
        {
            _companyPage.Next.Click();

            return this;
        }

        CompanyPage AddOpeningBalances(Company c)
        {
            _companyPage.Next.ClickWait();
            return this;
        }

        #endregion
    }
}
