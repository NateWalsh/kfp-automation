﻿using OpenQA.Selenium;
using qa_selenium_framework.Data;
using qa_selenium_framework.Elements;
using System.Linq;
using System;
using System.IO;
using Excel;
using System.Data;
using NUnit.Framework;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System.Diagnostics;

namespace qa_selenium_framework.PageObjects
{
    public class SalesAgentPage : _pageBase
    {
        SalesAgentPage GoToSubscriptions()
        {

            return this;
        }

        SalesAgentPage CompleteTenant(Tenant t)
        {
            CompleteClientDetails(t).CompleteCost(t).Next().CompleteBankingInformation(t);
            _salesAgentPage.ReviewSubmit.Click();
            return this;
        }

        public SalesAgentPage CompleteClientDetails(Tenant t)
        {
            if (t.ExistingCustomer)
            {
                _salesAgentPage.ExistingCustomerTrue.Click();
                _salesAgentPage.CustomerEmail.SendKeys(t.Email);
            }
            else
            {
                _salesAgentPage.ExistingCustomerFalse.Click();
                _salesAgentPage.FirstName.SendKeys(t.FirstName);
                _salesAgentPage.LastName.SendKeys(t.LastName);
                _salesAgentPage.CustomerEmail.SendKeys(t.Email);
                SelectByValue(_salesAgentPage.CustomerType, t.CustomerType);
                _salesAgentPage.CompanyName.SendKeys(t.CompanyName);
                _salesAgentPage.Address1.SendKeys(t.Address1);
                _salesAgentPage.Address2.SendKeys(t.Address2);
                _salesAgentPage.Town.SendKeys(t.Town);
                _salesAgentPage.Postcode.SendKeys(t.Postcode);
                _salesAgentPage.Telephone.SendKeys(t.Telephone);
                _salesAgentPage.NextButton.Click();
            }
            return this;
        }

        public SalesAgentPage Next()
        {
            _salesAgentPage.NextButton.Click();
            return this;
        }



        SalesAgentPage CompleteCost(Tenant t)
        {
            if (t.AE)
            {
                _salesAgentPage.AutomaticEnrolmentTrue.Click();
                string getText = _salesAgentPage.AEDate.GetAttribute("value");
                if (getText == "")
                {
                    _salesAgentPage.AEDate.SendKeys(t.AEStartDate.ToShortDateString() + Keys.Escape);
                }
                _salesAgentPage.NumberOfAe.ClearAndKeys(t.NumberOfAE.ToString());


            }
            else
            {
                _salesAgentPage.AutomaticEnrolmentFalse.Click();
            }

            _salesAgentPage.NumberOfEmployees.ClearAndKeys(t.NumberOfEmployees.ToString());

            switch (t.CustomerType)
            {
                case CustomerType.SME:
                    _salesAgentPage.NumberOfCompanies.ClearAndKeys(t.NumberOfCompanies.ToString());
                    if (t.AE == true)
                    {
                        _salesAgentPage.NumberOfAe.ClearAndKeys(t.NumberOfAE.ToString());
                    }
                    else
                    {
                        _salesAgentPage.NumberOfEss.ClearAndKeys(t.NumberOfESS.ToString());
                    }
                    break;
                case CustomerType.Accountant:
                    break;
            }

            _salesAgentPage.CalculateButton.Click();
            Thread.Sleep(400);

            return this;
        }

        void UpdateTenantFromXL(DataRow row, Tenant t)
        {
            switch (t.CustomerType)
            {
                case CustomerType.Accountant:
                    var employees = row[0].ToString();
                    var costPerEmp = row[1].ToString();

                    t.NumberOfEmployees = Int32.Parse(employees);
                    t.CostPerEmployee = Decimal.Parse(costPerEmp);
                    if (t.AE)
                    {
                        var numberOfAE = row[4].ToString();
                        var costPerAE = row[5].ToString();
                        var costPerMonth = row[6].ToString();
                        var annualCost = row[7].ToString();
                        t.NumberOfAE = Int32.Parse(numberOfAE);
                        t.CostPerAE = Decimal.Parse(costPerAE);
                        t.CostPerMonth = Decimal.Parse(costPerMonth);
                        t.AnnualCost = Decimal.Parse(annualCost);
                    }
                    else
                    {
                        var costPerMonth = row[2].ToString();
                        var annualCost = row[3].ToString();
                        t.CostPerMonth = Decimal.Parse(costPerMonth);
                        t.AnnualCost = Decimal.Parse(annualCost);
                    }
                    break;

                case CustomerType.SME:
                    var companies = row[0].ToString();
                    employees = row[1].ToString();
                    t.NumberOfCompanies = Int32.Parse(companies);
                    t.NumberOfEmployees = Int32.Parse(employees);
                    t.CostPerEmployee = 1;
                    t.CostPerAE = 1;

                    if (t.AE)
                    {
                        var numberOfAE = row[3].ToString();
                        var costPerMonth = row[10].ToString();
                        var annualCost = row[12].ToString();
                        t.NumberOfAE = Int32.Parse(numberOfAE);
                        t.CostPerMonth = Decimal.Parse(costPerMonth);
                        t.AnnualCost = Decimal.Parse(annualCost);
                    }
                    else
                    {
                        var numberOfESS = row[2].ToString();
                        var costPerMonth = row[9].ToString();
                        var annualCost = row[11].ToString();
                        t.NumberOfESS = Int32.Parse(numberOfESS);
                        t.CostPerMonth = Decimal.Parse(costPerMonth);
                        t.AnnualCost = Decimal.Parse(annualCost);
                    }

                    break;
            }

        }

        public SalesAgentPage SelectExcelTables(Tenant t)
        {
            DataSet result = ReadExcelWithHeader("SalesAgentData.xlsx");
            DataRowCollection rows;
            int rowCount = 0;

            switch (t.CustomerType)
            {
                case CustomerType.Accountant:
                    rows = result.Tables[0].Rows;
                    rowCount = rows.Count;

                    break;

                case CustomerType.SME:
                    rows = result.Tables[1].Rows;
                    rowCount = rows.Count;

                    break;

                default:
                    rows = result.Tables[0].Rows;
                    rowCount = rows.Count;
                    t = Tenant.GetSME();
                    break;
            }

            UpdateTenantFromXL(rows[3], t);
            return this;
        }

        public SalesAgentPage AssertSAConfirmationScreen(Tenant tenant)
        {
            loginPage().SalesLogin();

            salesAgentPage().
                CompleteClientDetails(tenant)
                .SelectExcelTables(tenant)
                .CompleteCost(tenant).Next()
                .CompleteBankingInformation(tenant)
                .AssertReviewContract(tenant);

            return this;
        }
    
        public SalesAgentPage CheckAccountantBands(Tenant t)
        {
            DataSet result = ReadExcelWithHeader("SalesAgentData.xlsx");
            DataRowCollection rows;
            int rowCount = 0;

            #region CustomerType Switch
            switch (t.CustomerType)
            {
                case CustomerType.Accountant:
                    rows = result.Tables[0].Rows;
                    rowCount = rows.Count;

                    break;

                case CustomerType.SME:
                    rows = result.Tables[1].Rows;
                    rowCount = rows.Count;

                    break;

                default:
                    rows = result.Tables[0].Rows;
                    rowCount = rows.Count;
                    t = Tenant.GetSME();
                    break;
            }
            #endregion

            for (int row = 0; row < rowCount; row++)
            {

                UpdateTenantFromXL(rows[row], t);
                CompleteCost(t);

                string costPerEmpValue = (string)_salesAgentPage.CostPerAdditionalEmployee.GetAttribute("value");
                string costPerMonthValue = (string)_salesAgentPage.CostPerMonth.GetAttribute("value");
                string costPerAnnumValue = (string)_salesAgentPage.CostOfAnnualContract.GetAttribute("value");

                var empAssert = decimal.Parse(costPerEmpValue);
                var monthAssert = decimal.Parse(costPerMonthValue);
                var annualAssert = decimal.Parse(costPerAnnumValue);

                Thread.Sleep(200);

                Assert.AreEqual(empAssert, t.CostPerEmployee);
                Assert.AreEqual(monthAssert, t.CostPerMonth);
                Assert.AreEqual(annualAssert, t.AnnualCost);


            }

            return this;
        }

        public SalesAgentPage SelfSignUp(Tenant t)
        {
            CompleteSelfSignUpScreen(t);
            gmailPage().ActivateAccount(t);
            CreatePassword(t);
            return this;
        }

        SalesAgentPage UpgradeToBusiness(Tenant t)
        {
            return this;
        }

        SalesAgentPage CompleteSelfSignUpScreen(Tenant t)
        {
            _loginPage.FirstName.SendKeys(t.FirstName);
            _loginPage.LastName.SendKeys(t.LastName);
            _loginPage.EmailAddress.SendKeys(t.Email);
            _loginPage.Phone.SendKeys(t.Telephone);
            _loginPage.TermsAccepted.ClickWait();
            _loginPage.submit.ClickWait();
            return this;
        }

        SalesAgentPage CompleteBankingInformation(Tenant t)
        {
            _salesAgentPage.ConfirmAccountHolder.Click();
            _salesAgentPage.ConfirmAuthorised.Click();
            _salesAgentPage.SortCode1.SendKeys(t.SortCode1);
            _salesAgentPage.SortCode2.SendKeys(t.SortCode2);
            _salesAgentPage.SortCode3.SendKeys(t.SortCode3);
            _salesAgentPage.AccountName.SendKeys(t.AccountName);
            _salesAgentPage.AccountNumber.SendKeys(t.AccountNumber);

            _salesAgentPage.Verify.Click();

            var watch = new Stopwatch();
            watch.Start();
            while (watch.Elapsed.TotalSeconds < 10)
            {
                try
                {
                    _salesAgentPage.NextButton.Click();
                    _salesAgentPage.ReviewName.Click();
                    break;
                }
                catch
                {
                }

            }
            
            return this;
        }

        public SalesAgentPage AssertReviewContract(Tenant t)
        {
            Assert.AreEqual(t.FirstName + " " + t.LastName, _salesAgentPage.ReviewName.Text);
            Assert.AreEqual((int)t.CustomerType, ValueAsInt(_salesAgentPage.CustomerType));
            Assert.AreEqual(t.NumberOfEmployees, ValueAsInt(_salesAgentPage.ReviewEmployees));
            Assert.AreEqual(t.CostPerMonth, ValueAsDec(_salesAgentPage.ReviewCostPerMonth));
            Assert.AreEqual(t.CostPerEmployee, ValueAsDec(_salesAgentPage.ReviewCostPerEmp));
            Assert.AreEqual(t.AnnualCost, ValueAsDec(_salesAgentPage.ReviewAnnual));

            return this;
        }

        public SalesAgentPage CreateTenant(Tenant t)
        {
            loginPage().SalesLogin();

            salesAgentPage().CompleteTenant(t).LogOut();

            gmailPage().ClickGmailLink(t.Email, "Payroll Terms", "go.kashflowpayroll.com");

            salesAgentPage().ConfirmContract();

            gmailPage().ClickGmailLink(t.Email,"Activate","go.kashflowpayroll.com");

            salesAgentPage().CreatePassword(t);

            System.Console.WriteLine(t.Email);
            System.Console.WriteLine(t.Password);
            return this;
        }

        SalesAgentPage SignUpTenant(Tenant t)
        {
            CompleteClientDetails(t).CompleteCost(t).Next().CompleteBankingInformation(t);
            _salesAgentPage.ReviewSubmit.Click();
            return this;
        }

        SalesAgentPage ConfirmContract()
        {
            _salesAgentPage.AgreeTerms.ClickWait();
            _salesAgentPage.OKButton.ClickWait();
            _salesAgentPage.SubmitContract.ClickWait();

            return this;
        }

        SalesAgentPage LogOut()
        {

            _salesAgentPage.UserMenu.ClickWait();
            _salesAgentPage.LogOut.ClickWait();
            return this;
        }

        SalesAgentPage CreatePassword(Tenant t)
        {
            _salesAgentPage.ChoosePassword.SendKeys(t.Password);
            _salesAgentPage.ConfirmPassword.SendKeys(t.Password);
            _salesAgentPage.ActivateButton.ClickWait();
            if (IsElementPresent(By.CssSelector(".giant-choice .alt")))
            {
                _salesAgentPage.DemoButton.ClickWait();
            }

            ClearReleaseNotes();
            return this;
        }

    }
}
    