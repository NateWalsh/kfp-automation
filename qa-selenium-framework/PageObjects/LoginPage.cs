﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.PageObjects;
using qa_selenium_framework.Data;

namespace qa_selenium_framework
{
    public class LoginPage: _pageBase
    {
        private readonly string signInURL = Driver.BaseAddress + "/Users/SignIn";
        private readonly string mainUrl = Driver.BaseAddress;
        private readonly string salesURL = Driver.BaseAddress + "/Sales";

        public UserCredentials User
        {
            get
            {
                return LoginDetails.GetUserCredentials(Driver.UserContext.User, Driver.UserContext.Environment);
            }
        }

        LoginPage GoToLogin()
        {
            Driver.Instance.Navigate().GoToUrl(signInURL);
            return this;
        }

        public LoginPage GoToSelfSignUp()
        {
            Driver.Instance.Navigate().GoToUrl(signInURL);
            _loginPage.SignUpForFree.ClickWait();
            return this;
        }

        public LoginPage Login()
        {
            GoToLogin();
            _loginPage.username.Clear();
            _loginPage.username.SendKeys(User.UserName);
            _loginPage.password.Clear();
            _loginPage.password.SendKeys(User.Password);
            _loginPage.submit.Click();
            Driver.Instance.Navigate().GoToUrl(mainUrl);
            if (IsElementPresent(By.CssSelector(".giant-choice .alt")))
            {
                _loginPage.demo.ClickWait();
            }

            ClearReleaseNotes();

            return this;
        }

        public LoginPage SalesLogin()
        {
            GoToSales();
            _loginPage.username.Clear();
            _loginPage.username.SendKeys(User.AdminName);
            _loginPage.password.Clear();
            _loginPage.password.SendKeys(User.Password);
            _loginPage.submit.Click();
            Driver.Instance.Navigate().GoToUrl(salesURL);
            return this;
        }


    }
}
