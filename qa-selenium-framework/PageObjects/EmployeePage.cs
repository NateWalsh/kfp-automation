﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using qa_selenium_framework.Data;
using qa_selenium_framework.Elements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qa_selenium_framework.PageObjects
{
    public class EmployeePage:_pageBase
    {

        #region Navigation
        EmployeePage GoToAllCurrentEmps()
        {
            _employeePage.EmployeeMenu.Click();
            _employeePage.Overview.Click();
            if (IsElementPresent(By.CssSelector("[class='giant-choice']")) == false)
            {
                _employeePage.AllCurrentTab.Click();
            }
                return this;
        }

        EmployeePage GoToEmployeePension()
        {
            _employeePage.EmployeeMenu.ClickWait();
            _employeePage.Pension.ClickWait();
            return this;
        }

        #endregion

        public EmployeePage DeleteAllEmployees()
        {
            var index = 0;

            GoToAllCurrentEmps();

            //If there are any employees
            if (IsElementPresent(By.CssSelector("[class='giant-choice']")) == false)
            {
                var noOfEmps = _employeePage.AllEmployees.Count;

                while (index < noOfEmps)
                {
                    Timeout(2);
                    try
                    {

                        var deleteurl = _employeePage.TrashEmployee.GetAttribute("href");
                        Driver.Instance.Navigate().GoToUrl(deleteurl);

                        _employeePage.ConfirmDelete.Click();
                        _employeePage.DeleteButton.Click();
                        if (IsElementPresent(By.Id("DeleteIsConfirmed")))
                        {
                            break;
                        }
                    }
                    catch
                    { 
                    }
                    if (_employeePage.AllEmployees.Count < noOfEmps)
                    {
                        index++;
                    }
                }
            }
            Timeout(5);

            return this;
        }


        public EmployeePage CreateDefaultEmployee()
        {
            CreateEmployee(Employee.GetDefault());
            return this;
        }

        public EmployeePage CreateEssEmployee()
        {
            CreateEmployee(Employee.getESSEmployee());
            return this;
        }

        public EmployeePage CreateWeeklyEmployee()
        {
            CreateEmployee(Employee.getWeekly());
            return this;
        }

        public EmployeePage CreateEmployee(Employee e)
        {
            _employeePage.EmployeeMenu.Click();
            _employeePage.AddNewEmployee.Click();

            AddBasics(e).AddPersonal(e).AddContact(e).AddEmployment(e).AddGovernment(e).AddPayment(e);

            _employeePage.OBALSLink.Click();

            AddObals(e);

            return this;
        }

        public EmployeePage CreateEmployeesFromExcel(bool Single)
        {
            if (Single)
            {
                Employee employee = Employee.getFromExcelSingle();
                CreateEmployee(employee);
            }
            else
            {
                IList<Employee> employees = Employee.getFromExcelMultiple();
                CreateEmployees(employees);
            }


            return this;
        }

        public EmployeePage CreateEmployees(IList<Employee> employees)
        {
            foreach (var employee in employees)
            {
                CreateEmployee(employee);
            }
            return this;
        }

        IList<Employee> PopulateEmployeeList(int numberOfEmployees)
        {
            List<Employee> list = new List<Employee>();

            int start = 1;
            for (int i = start; start < (numberOfEmployees + 1); start++)
            {
                var employee = Employee.GetDefault();
                employee.LastName = "Employee";
                employee.FirstName = "Number" + start.ToString();
                list.Add(employee);
            }
            return list;
        }

        IList<Employee> PopulateWeeklyEmployeeList(int numberOfEmployees)
        {
            List<Employee> list = new List<Employee>();

            int start = 1;
            for (int i = start; start < (numberOfEmployees + 1); start++)
            {
                var employee = Employee.getWeekly();
                employee.LastName = "Employee";
                employee.FirstName = "Number" + start.ToString();
                list.Add(employee);
            }
            return list;
        }



        public EmployeePage CreateEmployees(int numberofEmployees)
        {
            IList<Employee> employees = PopulateEmployeeList(numberofEmployees);
            foreach (Employee e in employees)
            {
                CreateEmployee(e);
            }

            return this;
        }

        public EmployeePage CreateWeeklyEmployees(int numberofEmployees)
        {
            IList<Employee> employees = PopulateWeeklyEmployeeList(numberofEmployees);
            foreach (var employee in employees)
            {
                CreateEmployee(employee);
            }

            return this;
        }

        #region Add Employee Details
        #region The Basics

        EmployeePage AddBasics(Employee e)
        {
            _employeePage.FirstName.SendKeysQuick(e.FirstName);
            _employeePage.LastName.SendKeysQuick(e.LastName);
            _employeePage.JoinDate.SendKeysQuick(e.JoinDate.ToShortDateString());

            if (e.NewToCompany)
            {
                _employeePage.NewToCompanyTrue.ClickWait();
                switch (e.StarterDeclaration)
                {
                    case StarterDeclaration.A:
                        _employeePage.StarterDeclarationA.Click();
                        break;
                    case StarterDeclaration.B:
                        _employeePage.StarterDeclarationB.Click();
                        break;
                    case StarterDeclaration.C:
                        _employeePage.StarterDeclarationC.Click();
                        break;
                    case StarterDeclaration.Unknown:
                        _employeePage.StarterDeclarationUnknown.Click();
                        break;
                }
            }
            else { _employeePage.NewToCompanyFalse.Click(); }

            if (e.isDirector)
            {
                _employeePage.IsDirectorTrue.Click();
                _employeePage.DirectorStartDate.SendKeysQuick(e.DirectorStartDate.ToShortDateString());
                if (e.CumulativeNICs)
                {
                    _employeePage.CummulativeNICSTrue.ClickWait();
                }
                else
                {
                    _employeePage.CummulativeNICSFalse.ClickWait();
                }

            }
            else { _employeePage.IsDirectorFalse.Click(); }

            _employeePage.NextStep1.Click();

            return this;
        }

        #endregion

        #region Personal

        EmployeePage AddPersonal(Employee e)
        {

            SelectByValue(_employeePage.Title, e.Title);
            _employeePage.KnownAs.ClearAndKeys(e.KnownAs);
            if (e.Gender) { _employeePage.GenderFemale.Click(); }
            else { _employeePage.GenderMale.Click(); }

            _employeePage.DOB.SendKeysQuick(e.DOB.ToShortDateString());
            SelectByValue(_employeePage.MaritalStatus, e.MaritalStatus);

            _employeePage.Next.ClickWait();

            return this;
        }

        #endregion

        #region Contact
        EmployeePage AddContact(Employee e)
        {
            _employeePage.Address1.SendKeysQuick(e.Address1);
            _employeePage.Address2.SendKeysQuick(e.Address2);
            _employeePage.Postcode.SendKeysQuick(e.Postcode);
            SelectByValue(_employeePage.Country, e.Country);
            _employeePage.WorkPhone.SendKeysQuick(e.WorkPhone);
            _employeePage.HomePhone.SendKeysQuick(e.HomePhone);
            _employeePage.MobilePhone.SendKeysQuick(e.MobilePhone);

            _employeePage.Next.Click();


            return this;
        }
        #endregion

        #region Employment
        EmployeePage AddEmployment(Employee e)
        {
            _employeePage.Next.Click();

            

            return this;
        }
        #endregion

        #region Government
        EmployeePage AddGovernment(Employee e)
        {
            _employeePage.TaxCode.Clear();
            _employeePage.TaxCode.SendKeysQuick(e.TaxCode);
            if (e.Cumulative) { _employeePage.TaxIsCumulative.Click(); }
            else { _employeePage.TaxIsNonCumulative.Click(); }

            SelectByValue(_employeePage.StudentLoanSelector, e.StudentLoan);
            //if (e.StudentLoan) { _employeePage.StudentLoanTrueOld.Click(); }
            //else { _employeePage.StudentLoanTrueOld.Click(); }
            _employeePage.NiNo.SendKeysQuick(e.NiNo);
            SelectByValue(_employeePage.NiCat, e.NiCat);

            _employeePage.Next.Click();

            return this;
        }
        #endregion

        #region Payment
        EmployeePage AddPayment(Employee e)
        {
            switch (e.PayCycle)
            {
                case PayCycle.Monthly:
                    _employeePage.MonthlyPayCycle.Click();
                    break;
                case PayCycle.Weekly:
                    _employeePage.WeeklyPayCycle.Click();
                    break;
            }

            if (e.Intermittently) { _employeePage.IntermittentlyTrue.Click(); }
            else { _employeePage.IntermittentlyFalse.Click(); }

            _employeePage.HoursPerWeek.ClearAndKeys(e.HoursPerWeek);

            SelectByValue(_employeePage.PayMethod, e.PaymentMethod);
            SelectByValue(_employeePage.PayslipDelivery, e.PayslipDelivery);

            _employeePage.EmailAddress.SendKeysQuick(e.PayslipEmail);
            _employeePage.AccountName.ClearAndKeys(e.BankAccountName);
            _employeePage.SortCode.SendKeysQuick(e.SortCode);
            _employeePage.AccountNumber.SendKeysQuick(e.AccountNumber);
            _employeePage.BACSRef.SendKeysQuick(e.BACSRef);

            _employeePage.Next.Click();

            return this;
        }
            #endregion

        #region OBALS
        public EmployeePage AddObals(Employee e)
        {
            _employeePage.EmployeeMenu.Click();
            _employeePage.OpeningBalances.Click();

            SelectEmployee(e);

            _employeePage.Next.Click();

            return this;
        }
        #endregion

        #endregion

        #region Employee Asserts

        public EmployeePage AssertWorkerStatus(WorkerStatus workerStatus)
        {
            GoToEmployeePension();
            int v = SelectedOptionAsValue(_employeePage.EeWorkerStatus);
            Assert.AreEqual((int)workerStatus, v);
            return this;
        }

        public EmployeePage OptOut(Employee emp, DateTime date)
        {
            employeePage().GoToEmployeePension().SelectEmployee(emp);
            _employeePage.EeOptOutDate.SendKeysQuick(date.ToShortDateString());
            _employeePage.SubmitEEPension.ClickWait();

            return this;
        }

        public EmployeePage OptOut(int empNo, DateTime date)
        {
            employeePage().GoToEmployeePension().SelectEmployee(empNo);
            _employeePage.EeOptOutDate.SendKeysQuick(date.ToShortDateString());
            _employeePage.SubmitEEPension.ClickWait();

            return this;
        }

        public EmployeePage OptIn(Employee emp, DateTime date, Pension pen)
        {
            employeePage().GoToEmployeePension().SelectEmployee(emp);
            _employeePage.EEPensionJoinOptInDate.SendKeysQuick(date.ToShortDateString() + Keys.Escape);
            SelectByText(_employeePage.EePensionScheme, pen.PensionName);
            _employeePage.SubmitEEPension.ClickWait();

            return this;
        }

        public EmployeePage OptIn(int empNo, DateTime date,Pension pen)
        {
            employeePage().GoToEmployeePension().SelectEmployee(empNo);
            _employeePage.EEPensionJoinOptInDate.SendKeysQuick(date.ToShortDateString() + Keys.Escape);
            SelectByText(_employeePage.EePensionScheme, pen.PensionName);
            _employeePage.SubmitEEPension.ClickWait();

            return this;
        }

        public EmployeePage OptIn(Employee emp, DateTime date)
        {
            employeePage().GoToEmployeePension().SelectEmployee(emp);
            _employeePage.EEPensionJoinOptInDate.SendKeysQuick(date.ToShortDateString());
            SelectByText(_employeePage.EePensionScheme, Pension.GetDefault().PensionName);
            _employeePage.SubmitEEPension.ClickWait();

            return this;
        }

        public EmployeePage OptIn(int empNo, DateTime date)
        {
            employeePage().GoToEmployeePension().SelectEmployee(empNo);
            _employeePage.EEPensionJoinOptInDate.SendKeysQuick(date.ToShortDateString());
            SelectByText(_employeePage.EePensionScheme, Pension.GetDefault().PensionName);
            _employeePage.SubmitEEPension.ClickWait();

            return this;
        }

        public EmployeePage AddTransitionalPeriod(Employee emp)
        {
            employeePage().GoToEmployeePension().SelectEmployee(emp);
            _employeePage.TransitionalPeriod.ClickWait();
            _employeePage.SubmitEEPension.ClickWait();
            return this;
        }

        public EmployeePage AddTransitionalPeriod(int empNo)
        {
            employeePage().GoToEmployeePension().SelectEmployee(empNo);
            _employeePage.TransitionalPeriod.ClickWait();
            _employeePage.SubmitEEPension.ClickWait();
            return this;
        }

        #endregion
    }
}
