﻿using OpenQA.Selenium;
using qa_selenium_framework.Data;
using qa_selenium_framework.Elements;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace qa_selenium_framework.PageObjects
{
    public class GmailPage : _pageBase
    {
        private readonly string GMailURL = @"https://accounts.google.com/ServiceLogin?service=mail&continue=https://mail.google.com/mail/#identifier";

        public GmailPageElements Elements
        {
            get
            {
                return new GmailPageElements();
            }
        }

        public UserCredentials User
        {
            get
            {
                return LoginDetails.GetUserCredentials(Driver.UserContext.User, Driver.UserContext.Environment);
            }
        }

        GmailPage GoToGmail()
        {
            Driver.Instance.Navigate().GoToUrl(GMailURL);
            return this;
        }

        GmailPage EnterPasswordIfAsked()
        {
            if (IsElementPresent(By.Id("Email")))
            {
                try
                {
                    Elements.Email.ClearAndKeys(User.GmailEmail);
                    Elements.Next.Click();
                    WaitUntilClickable(Elements.Password);
                    Elements.Password.ClearAndKeys(User.GmailPassword);
                    Elements.SignIn.Click();
                }
                catch
                {

                }

            }

            if (IsElementPresent(By.Id("Passwd")))
            {
                Elements.Password.ClearAndKeys(User.GmailPassword);
                Elements.SignIn.Click();
            }
            return this;
        }

        GmailPage SearchEmails(string emailSearch, string emailHeader)
        {
            GoToGmail();
            EnterPasswordIfAsked();

            var watch = new Stopwatch();
            watch.Start();
            while (watch.Elapsed.TotalSeconds < 200)
            {
                try
                {
                    Elements.SearchText.ClearAndKeys(emailSearch);
                    Elements.SearchButton.Click();
                    IWebElement emailclick = Driver.Instance.FindElement(By.PartialLinkText(emailHeader));
                    emailclick.Click();
                    break;
                }
                catch
                {
                    Thread.Sleep(5000);
                }
            }
            return this;
        }

        GmailPage ClickEmailLink(String emailLink)
        {
            IWebElement hrefElement = Driver.Instance.FindElement(By.PartialLinkText(emailLink));
            string href = hrefElement.GetAttribute("href");
            Driver.Instance.Navigate().GoToUrl(href);

            return this;
        }

        public GmailPage ClickGmailLink(string emailAddress, string emailHeader, string linkSearch)
        {
            string  emailSearch = emailAddress + " " + emailHeader;
            SearchEmails(emailSearch, emailHeader);
            ClickEmailLink(linkSearch);
            Thread.Sleep(1000);
            return this;
        }

        public GmailPage ActivateAccount(Tenant t)
        {
            string emailHeader = "Activate";
            string linkSearch = "Users/Activate";
            GoToGmail();

            ClickGmailLink(t.Email, emailHeader,linkSearch);

            return this;
        }

        public GmailPage ConfirmTermsAndConditions(Tenant t)
        {
            string emailsearch = t.Email + " Payroll Terms & Conditions";
            GoToGmail();

            SearchEmails("Terms",emailsearch);

            return this;
        }

        public GmailPage ConfirmOPContract(TenantOP t)
        {
            GoToGmail();

            ClickGmailLink(t.AdminEmail, "Welcome to", "ReviewSalesRegistrationData");

            return this;
        }

        public GmailPage OPRegistrationConfirmation(TenantOP t)
        {
            GoToGmail();

            ClickGmailLink(t.AdminEmail, "Registration Confirmation", "account/confirm");

            return this;
        }
    }
}
