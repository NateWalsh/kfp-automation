﻿using NUnit.Framework;
using OpenQA.Selenium;
using qa_selenium_framework.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace qa_selenium_framework.PageObjects
{
    public class CommsPage: _pageBase
    {
        CommsPage GoToEnrolmentLetters()
        {
            _pensionPage.PensionMenu.ClickWait();
            _pensionPage.Communications.ClickWait();
            return this;
        }

        CommsPage GoToCommsToPrint()
        {
            _pensionPage.PensionMenu.ClickWait();
            _pensionPage.Communications.ClickWait();
            return this;
        }

        CommsPage GoToCommsPrinted()
        {
            _pensionPage.PensionMenu.ClickWait();
            _pensionPage.Communications.ClickWait();
            SelectByValue(_commsPage.DeliveryStatus, (int)CommsStatus.Printed);
            Thread.Sleep(500);
            return this;
        }

        CommsPage GoToCommsPublished()
        {
            _pensionPage.PensionMenu.ClickWait();
            _pensionPage.Communications.ClickWait();
            SelectByValue(_commsPage.DeliveryStatus, (int)CommsStatus.Published);
            Thread.Sleep(500);
            return this;
        }

        CommsPage GoToCommsAll()
        {
            _pensionPage.PensionMenu.ClickWait();
            _pensionPage.Communications.ClickWait();
            SelectByValue(_commsPage.DeliveryStatus, (int)CommsStatus.Published);
            return this;
        }

        string GetWorkerStatus(decimal pay)
        {
            AEThreshold ae = AEThreshold.Get2016();

            string workerStatus;
            if (pay <= ae.LEL)
            {
                workerStatus = "Entitled Worker";
            }
            else if (pay <= ae.Trigger)
            {
                workerStatus = "Non Eligible Jobholder";
            }
            else
            {
                workerStatus = "Eligible Jobholder";
            }

            return workerStatus;
        }

        string GetEnrolLetterString(EnrolLetters l)
        {
            string letterString;
            switch (l)
            {
                case EnrolLetters.L0:
                    letterString = "L0";// - Auto Enrolment is coming";
                    break;
                case EnrolLetters.L1:
                    letterString = "L1";// - For Eligible Jobholders";
                    break;
                case EnrolLetters.L1Opt:
                    letterString = "L1Opt";// - Opt in";
                    break;
                case EnrolLetters.L1P:
                    letterString = "L1P";// - For Eligible Jobholder (Postponement version)";
                    break;
                case EnrolLetters.L1T:
                    letterString = "L1T";// - Transitional Period Applied";
                    break;
                case EnrolLetters.L2and3:
                    letterString = "L2 & L3";// - Non Eligible Jobholders and Entitled workers";
                    break;
                case EnrolLetters.L4:
                    letterString = "L4";// - Already a member of a qualifying scheme";
                    break;
                case EnrolLetters.L6:
                    letterString = "L6";// - Postponement all workers";
                    break;
                default:
                    letterString = "No letter?? WTF?";
                    break;
            }

            return letterString;
        }



        string GetEnrolStringByNumber(int i)
        {
            List<string> strings = new List<string>();
            foreach (IWebElement e in _commsPage.Letters)
            {

                if (e.Text.Contains("Number" + i + " Employee"))
                {
                    strings.Add(e.Text);
                }

            }
            return strings[strings.Count - 1];
        }

        string GetEnrolStringByName(Employee emp)
        {
            List<string> strings = new List<string>();
            foreach (IWebElement e in _commsPage.Letters)
            {

                if (e.Text.Contains(emp.FirstName + " " + emp.LastName))
                {
                    strings.Add(e.Text);
                }

            }

            return strings[strings.Count -1];
        }

        string GetLastEnrolLetter()
        {
            List<string> strings = new List<string>();
            foreach (IWebElement e in _commsPage.Letters)
            {
                strings.Add(e.Text);
            }

            return strings[strings.Count - 1];
        }

        public CommsPage ClickLastCommsRow()
        {
            int rowCount = _commsPage.Letters.Count;
            IWebElement lastRow = _commsPage.Letters[rowCount - 1];

            lastRow.ClickWait();
            _commsPage.DownloadLetterButton.ClickWait();

            return this;
        }

        public CommsPage ClickLastCommsRowByNumber(int empNo)
        {
            List<IWebElement> RowsByNumber = new List<IWebElement>();
            foreach (IWebElement e in _commsPage.Letters)
            {
                if (e.Text.Contains("Number" + empNo + " Employee"))
                {
                    RowsByNumber.Add(e);
                }
            }
            int rowCount = RowsByNumber.Count;
            IWebElement lastRow = RowsByNumber[rowCount - 1];

            lastRow.ClickWait();

            return this;
        }

        public CommsPage ClickLastCommsRowByName(Employee emp)
        {
            List<IWebElement> RowsByName = new List<IWebElement>();

            foreach (IWebElement e in _commsPage.Letters)
            {

                if (e.Text.Contains(emp.FirstName + " " + emp.LastName))
                {
                    RowsByName.Add(e);
                }
            }

            int rowCount = RowsByName.Count;
            IWebElement lastRow = RowsByName[rowCount - 1];

            lastRow.ClickWait();

            return this;
        }

        public CommsPage AssertCommByNumber(int i, decimal pay, EnrolLetters enrolLetter)
        {

            GoToEnrolmentLetters();
            Thread.Sleep(1000);
            string letterString = GetEnrolStringByNumber(i);
            AssertComms(letterString,i, pay, enrolLetter);
            return this;
        }

        public CommsPage AssertCommByName(Employee emp, decimal pay, EnrolLetters enrolLetter)
        {
            GoToEnrolmentLetters();
            string letterString = GetEnrolStringByName(emp);
            AssertComms(letterString, emp, pay, enrolLetter);
            return this;
        }

        public CommsPage AssertSingleComms(Employee emp, decimal pay, EnrolLetters enrolLetter)
        {
            if (emp.PayslipEmail == "")
            {
                GoToEnrolmentLetters();
            }
            else
            {
                GoToCommsPrinted();
            }

            string letterString = GetLastEnrolLetter();
            AssertComms(letterString, emp, pay, enrolLetter);
            return this;
        }

        public CommsPage AssertSingleCommsPrinted(Employee emp, decimal pay, EnrolLetters enrolLetter)
        {
            GoToCommsPrinted();
            string letterString = GetLastEnrolLetter();
            AssertComms(letterString, emp, pay, enrolLetter);
            return this;
        }

        CommsPage AssertComms(string letterString, Employee emp, decimal pay, EnrolLetters enrolLetter)
        {
            AEThreshold AEThreshold = AEThreshold.Get2016();

            string letter = GetEnrolLetterString(enrolLetter);
            string workerStatus;
            if (letterString.Contains(GetEnrolLetterString(EnrolLetters.L1T)) ||
                letterString.Contains(GetEnrolLetterString(EnrolLetters.L0)) ||
                letterString.Contains(GetEnrolLetterString(EnrolLetters.L6)) ||
                letterString.Contains(GetEnrolLetterString(EnrolLetters.L4)))
            {
                workerStatus = "None";
            }
            else
            {
                workerStatus = GetWorkerStatus(pay);
            }


            Assert.IsTrue(letterString.Contains(emp.FirstName + " " + emp.LastName));
            Assert.IsTrue(letterString.Contains(workerStatus));
            Assert.IsTrue(letterString.Contains(letter));
            return this;
        }

        CommsPage AssertComms(string letterString,int i, decimal pay, EnrolLetters enrolLetter)
        {
            AEThreshold AEThreshold = AEThreshold.Get2016();

            string letter = GetEnrolLetterString(enrolLetter);
            string workerStatus;
            if (letterString.Contains(GetEnrolLetterString(EnrolLetters.L1T)) ||
                letterString.Contains(GetEnrolLetterString(EnrolLetters.L0)) ||
                letterString.Contains(GetEnrolLetterString(EnrolLetters.L6)) ||
                letterString.Contains(GetEnrolLetterString(EnrolLetters.L4)))
            {
                workerStatus = "None";
            }
            else
            {
                workerStatus = GetWorkerStatus(pay);
            }


            Assert.IsTrue(letterString.Contains("Number" + i + " Employee"));
            Assert.IsTrue(letterString.Contains(workerStatus));
            Assert.IsTrue(letterString.Contains(letter));
            return this;
        }

        public CommsPage TestL0L2L1(Employee emp)
        {
            AEThreshold pay = AEThreshold.Get2016();

            companyPage().DeleteTestCompany().OpenTestCompany();
            pensionPage().CreateStagingDateInFuture();
            employeePage().CreateEmployee(emp);
            payrollPage().PayMonthly(pay.Trigger);
            commsPage().AssertSingleComms(emp, pay.Trigger, EnrolLetters.L0);
            payrollPage().PayMonthly(pay.Trigger);
            commsPage().AssertSingleComms(emp, pay.Trigger, EnrolLetters.L2and3);
            payrollPage().PayMonthly(pay.AboveTrigger);
            commsPage().AssertSingleComms(emp, pay.AboveTrigger, EnrolLetters.L1);
            return this;
        }

        public CommsPage TestL0L6(Employee emp)
        {
            AEThreshold pay = AEThreshold.Get2016();

            companyPage().OpenTestCompany();
            payrollPage().RollRack();
            employeePage().DeleteAllEmployees();
            pensionPage().DeleteAllPensions().CreateStarterPostponementInFuture();
            employeePage().CreateEmployee(emp);
            payrollPage().PayMonthly(pay.AboveUEL);
            commsPage().AssertSingleComms(emp, pay.AboveTrigger, EnrolLetters.L0);
            payrollPage().PayMonthly(pay.AboveUEL);
            commsPage().AssertSingleComms(emp, pay.AboveTrigger, EnrolLetters.L6);

            return this;
        }

        public CommsPage TestL4(Employee emp)
        {
            AEThreshold pay = AEThreshold.Get2016();

            companyPage().OpenTestCompany();
            payrollPage().RollRack();
            employeePage().DeleteAllEmployees();
            pensionPage().DeleteAllPensions().CreateDefaultPension().CreateNonAE();
            employeePage().CreateEmployee(emp).OptIn(emp,new DateTime(2016,04,01),Pension.GetNonAE());
            payrollPage().PayMonthly(pay.Trigger);
            commsPage().AssertSingleComms(emp, pay.Trigger, EnrolLetters.L4);

            return this;
        }

        public CommsPage TestL1T(Employee emp)
        {
            AEThreshold pay = AEThreshold.Get2016();

            companyPage().OpenTestCompany();
            payrollPage().RollRack();
            employeePage().DeleteAllEmployees();
            pensionPage().DeleteAllPensions().CreateDefaultPension();
            employeePage().CreateEmployee(emp).AddTransitionalPeriod(emp);
            payrollPage().PayMonthly(pay.AboveTrigger);
            commsPage().AssertSingleComms(emp, pay.AboveTrigger, EnrolLetters.L1T);

            return this;
        }


        public CommsPage TestL1Opt(Employee emp)
        {
            AEThreshold pay = AEThreshold.Get2016();

            companyPage().OpenTestCompany();
            payrollPage().RollRack();
            employeePage().DeleteAllEmployees();
            pensionPage().DeleteAllPensions().CreateDefaultPension();
            employeePage().CreateEmployee(emp);
            payrollPage().PayMonthly(pay.Trigger);
            commsPage().AssertSingleComms(emp, pay.Trigger, EnrolLetters.L2and3);
            employeePage().OptIn(emp, new DateTime(2016, 05, 01));
            payrollPage().PayMonthly(pay.Trigger);
            commsPage().AssertSingleComms(emp, pay.Trigger, EnrolLetters.L1Opt);

            return this;
        }


        public CommsPage TestL2AndL3(Employee emp)
        {
            AEThreshold pay = AEThreshold.Get2016();

            companyPage().OpenTestCompany();
            payrollPage().RollRack();
            employeePage().DeleteAllEmployees();
            pensionPage().DeleteAllPensions().CreateDefaultPension();
            employeePage().CreateDefaultEmployee();
            payrollPage().PayMonthly(pay.Trigger);
            commsPage().AssertSingleComms(emp, pay.Trigger, EnrolLetters.L2and3);
            payrollPage().PayMonthly(pay.AboveTrigger);
            commsPage().AssertSingleComms(emp, pay.AboveTrigger, EnrolLetters.L1);

            return this;
        }

        public CommsPage TesstWorkerStatus(AEThreshold pay)
        {
            loginPage().Login();

            companyPage().DeleteTestCompany().OpenTestCompany();
            pensionPage().CreateDefaultPension();
            employeePage().CreateDefaultEmployee();
            payrollPage().PayMonthly(pay.Trigger);
            employeePage().AssertWorkerStatus(WorkerStatus.NonEligibleJobHolder);
            payrollPage().PayMonthly(pay.AboveTrigger);
            employeePage().AssertWorkerStatus(WorkerStatus.EligibleJobholder);
            payrollPage().RollRack().DeleteAllPayslips().PayMonthly(pay.LEL);
            employeePage().AssertWorkerStatus(WorkerStatus.EntitledWorker);
            payrollPage().PayMonthly(pay.AboveLEL);
            employeePage().AssertWorkerStatus(WorkerStatus.NonEligibleJobHolder);
            payrollPage().PayMonthly(pay.AboveTrigger);
            employeePage().AssertWorkerStatus(WorkerStatus.EligibleJobholder);
            payrollPage().PayMonthly(pay.AboveLEL);
            employeePage().AssertWorkerStatus(WorkerStatus.EligibleJobholder);

            return this;
        }
    }
}
