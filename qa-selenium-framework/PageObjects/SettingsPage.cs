﻿using NUnit.Framework;
using OpenQA.Selenium;
using qa_selenium_framework.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace qa_selenium_framework.PageObjects
{
    public class SettingsPage: _pageBase
    {
        #region Navigation
        SettingsPage GoToSubscriptions()
        {
            _settingsPage.SettingsMenu.ClickWait();
            _settingsPage.Subscriptions.ClickWait();
            return this;
        }

        SettingsPage GoToProfile()
        {
            _settingsPage.SettingsMenu.ClickWait();
            _settingsPage.Profile.ClickWait();
            return this;
        }

        SettingsPage GoToBilling()
        {
            GoToSubscriptions();
            _settingsPage.ChangePasswordTab.ClickWait();
            return this;
        }

        SettingsPage GoToChangePassword()
        {
            GoToProfile();
            _settingsPage.ChangePasswordTab.ClickWait();
            return this;
        }
        #endregion 

        SettingsPage UpgradeAccount()
        {
            GoToSubscriptions();
            _settingsPage.FreeAccount.ClickWait();
            return this;
        }

    }
}
