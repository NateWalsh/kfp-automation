﻿using Excel;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using qa_selenium_framework.Data;
using qa_selenium_framework.Elements;
using qa_selenium_framework.PageObjects;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace qa_selenium_framework
{
    public class _pageBase
    {
        public DashboardElements Dashboard
        {
            get
            {
                return new DashboardElements();
            }
        }

        #region Add page references
        public LoginPage loginPage()
        {
            var loginPage = new LoginPage();
            return loginPage;
        }

        public CompanyPage companyPage()
        {
            var companyPage = new CompanyPage();
            return companyPage;
        }

        public SalesAgentPage salesAgentPage()
        {
            var salesAgentPage = new SalesAgentPage();
            return salesAgentPage;
        }

        public CommsPage commsPage()
        {
            var commsPage = new CommsPage();
            return commsPage;
        }

        public EmployeePage employeePage()
        {
            var employeePage = new EmployeePage();
            return employeePage;
        }

        public GmailPage gmailPage()
        {
            var gmailPage = new GmailPage();
            return gmailPage;
        }

        public PayrollPage payrollPage()
        {
            var payrollPage = new PayrollPage();
            return payrollPage;
        }

        public PensionPage pensionPage()
        {
            var pensionPage = new PensionPage();
            return pensionPage;
        }

        public SettingsPage settingsPage()
        {
            var settingsPage = new SettingsPage();
            return settingsPage;
        }

        #endregion
        #region Add page elements



        public CommsPageElements _commsPage
        {
            get
            {
                return new CommsPageElements();
            }
        }

        public CompanyPageElements _companyPage
        {
            get
            {
                return new CompanyPageElements();
            }
        }

        public EmployeePageElements _employeePage
        {
            get
            {
                return new EmployeePageElements();
            }
        }

        public GmailPageElements _gmailPage
        {
            get
            {
                return new GmailPageElements();
            }
        }

        public LoginPageElements _loginPage
        {
            get
            {
                return new LoginPageElements();
            }
        }

        public PayrollPageElements _payrollPage
        {
            get
            {
                return new PayrollPageElements();
            }
        }

        public PensionPageElements _pensionPage
        {
            get
            {
                return new PensionPageElements();
            }
        }

        public SalesAgentPageElements _salesAgentPage
        {
            get
            {
                return new SalesAgentPageElements();
            }
        }

        public SettingsPageElements _settingsPage
        {
            get
            {
                return new SettingsPageElements();
            }
        }

        #endregion


        #region Payslips page references
        public AdminPageOP adminPageOP()
        {
            var adminPageOP = new AdminPageOP();
            return adminPageOP;
        }

        public EnrolHarnessPage enrolHarnessPage()
        {
            var enrolHarnessPage = new EnrolHarnessPage();
            return enrolHarnessPage;
        }

        public InvoiceHarnessPage invoiceHarnessPage()
        {
            var invoiceHarnessPage = new InvoiceHarnessPage();
            return invoiceHarnessPage;
        }

        public LoginPageOP loginPageOP()
        {
            var loginPageOP = new LoginPageOP();
            return loginPageOP;
        }

        public SalesAgentPageOP salesAgentPageOP()
        {
            var salesAgentPageOP = new SalesAgentPageOP();
            return salesAgentPageOP;
        }



        #endregion
        #region Payslips elements

        public AdminElementsOP _adminPageOP
        {
            get
            {
                return new AdminElementsOP();
            }
        }

        public EnrolHarnessElements _enrolHarnessPage
        {
            get
            {
                return new EnrolHarnessElements();
            }
        }

        public InvoiceHarnessElements _invoiceHarnessPage
        {
            get
            {
                return new InvoiceHarnessElements();
            }
        }

        public LoginElementsOP _loginPageOP
        {
            get
            {
                return new LoginElementsOP();
            }
        }

        public SalesAgentElementsOP _salesAgentPageOP
        {
            get
            {
                return new SalesAgentElementsOP();
            }
        }





        #endregion

        #region Functions
        private readonly string homeURL = Driver.BaseAddress;
        private readonly string salesURL = Driver.BaseAddress + "/Sales";
        private readonly string loginOP = Driver.BaseAddressOP + "/Account/LogOn";

        public void GoTo(string url)
        {
            Driver.Instance.Navigate().GoToUrl(url);
        }

        public void GoToHome()
        {
            GoTo(homeURL);
        }

        public void GoToSales()
        {
            GoTo(salesURL);
        }

        public void GoToPayslipsLogin()
        {
            GoTo(loginOP);
        }

        public void ClearReleaseNotes()
        {
            var releaseNotesExist = true;
            while (releaseNotesExist == true)
            {
                try
                {
                    _companyPage.CompanyDashboard.Click();
                    releaseNotesExist = false;
                }
                catch
                {
                    try
                    {
                        _loginPage.releaseNotes.ClickWait();
                    }
                    catch { }
                }

            }
        }

        public void SelectByValue(IWebElement e, Object o)
        {
            new SelectElement(e).SelectByValue(((int)o).ToString());
        }

        public void SelectByText(IWebElement e, string str)
        {
            new SelectElement(e).SelectByText(str);
        }

        public void ScrollToElement(IWebElement e)
        {
            ((IJavaScriptExecutor)Driver.Instance).ExecuteScript("arguments[0].scrollIntoView(true);", e);
            Thread.Sleep(500);
        }

        public void sendKeysQuick(IWebElement e, String s)
        {
            ((IJavaScriptExecutor)Driver.Instance).ExecuteScript("arguments[0].value = arguments[1];",e,s);
        }

        public string SelectedOptionAsString(IWebElement element)
        {
            SelectElement selectedValue = new SelectElement(element);
            string value = selectedValue.SelectedOption.Text;
            return value;
        }

        public int SelectedOptionAsValue(IWebElement element)
        {
            SelectElement selectedValue = new SelectElement(element);
            int value = int.Parse(selectedValue.SelectedOption.GetAttribute("value"));
            return value;
        }

        public void SelectEmployee(Employee e)
        {
            var employeeName = e.FirstName + " " + e.LastName;
            Dashboard.EmployeePicker.Click();
            Driver.Instance.FindElement(By.PartialLinkText(employeeName)).ClickWait();
        }

        public void SelectEmployee(int empNumber)
        {
            var employeeName = "Number" + empNumber + " Employee";
            Dashboard.EmployeePicker.Click();
            Driver.Instance.FindElement(By.PartialLinkText(employeeName)).ClickWait();
        }
    
        public string CompanyNumber()
        {
            string URL = Driver.Instance.Url;
            string searchString = "Company";
            int index = URL.LastIndexOf(searchString);
            string[] seqNum = new string[] { URL.Substring(0, index), URL.Substring(index + 8) };

            string searchString2 = "/";
            int index2 = seqNum[1].IndexOf(searchString2);
            if (index2  > 0)
            {
                string[] seqNum2 = new string[] { seqNum[1].Substring(0, index2), seqNum[1].Substring(index2) };
                System.Console.WriteLine(seqNum2[0]);
                return seqNum2[0];
            }
            else
            {
                return seqNum[1];
            }

        }


        public void SelectCompany(Company c)
        {
            var companyName = c.CompanyName;
            Dashboard.CompanyPicker.Click();
            Driver.Instance.FindElement(By.PartialLinkText(companyName)).ClickWait();
        }

        public void SelectCompany(string c)
        {
            Dashboard.CompanyPicker.Click();
            Driver.Instance.FindElement(By.PartialLinkText(c)).ClickWait();
        }

        public void print(string s)
        {
            Console.WriteLine(s);
        }
        public void Timeout(int n)
        {
            Driver.Instance.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(n));
        }

        public bool IsElementPresent(By by)
        {
            Driver.Instance.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(0));

            try
            {
                Driver.Instance.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
            finally
            {
                Driver.Instance.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(5));
            }
        }

        protected static string DataSourceDirectory(string fileName)
        {
            return ProjectRootDirectory() + fileName;
        }

        public static string ProjectRootDirectory()
        {
            return Directory.GetParent(Directory.GetParent(Directory.GetParent(Directory.GetParent(AppDomain.CurrentDomain.BaseDirectory).FullName).FullName).FullName).FullName + @"\qa-selenium-framework\";
        }

        public static DataSet ReadExcelWithHeader(string fileName)
        {
            string path = ProjectRootDirectory() + @"Spreadsheets/" + fileName;
            FileStream stream = File.Open(path, FileMode.Open, FileAccess.Read);

            //2. Reading from a OpenXml Excel file (2007 format; *.xlsx)
            IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);

            //4. DataSet - Create column names from first row
            excelReader.IsFirstRowAsColumnNames = true;
            DataSet result = excelReader.AsDataSet();

            excelReader.Close();

            return result;
        }



        public static DataSet ReadExcelWithoutHeader(string fileName)
        {
            string path = ProjectRootDirectory() + fileName;

            FileStream stream = File.Open(path, FileMode.Open, FileAccess.Read);
            //2. Reading from a OpenXml Excel file (2007 format; *.xlsx)
            IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);

            //3. DataSet - The result of each spreadsheet will be created in the result.Tables
            DataSet result = excelReader.AsDataSet();

            return result;
        }

        public IWebElement WaitUntil(IWebElement e)
        {
            WebDriverWait wait = new WebDriverWait(Driver.Instance, TimeSpan.FromSeconds(5));
            IWebElement loadingElement = wait.Until(d => { return e; }) ;
            return loadingElement;
        }

        public void KillPopup()
        {
            var driver = Driver.Instance;
            driver.SwitchTo().Window(driver.WindowHandles[1]).Close();
            driver.SwitchTo().Window(driver.WindowHandles[0]);
        }


        public void WaitUntilClickable(IWebElement Element)
        {
            bool isClickable = false;
            var watch = new Stopwatch();
            watch.Start();
            while (watch.Elapsed.TotalSeconds < 10)
            {
                try
                {
                    Element.Click();
                    isClickable = true;
                    break;
                }
                catch 
                {
                }

            }
            if (isClickable == false)
                Assert.Fail("Unable to click element");
        }

        public int ValueAsInt(IWebElement e)
        {
            int number = Int32.Parse(e.GetAttribute("value"));
            return number;
        }

        public string ValueAsString(IWebElement e)
        {
            string text = e.GetAttribute("value");
            return text;
        }

        public decimal ValueAsDec(IWebElement e)
        {
            decimal dec = Decimal.Parse(e.GetAttribute("value"));
            return dec;
        }

    }

    #endregion

        #region Extensions

    public static class ExtensionMethodsForEvents
    {
        /// <summary>
        /// Invoking click action for item with 10 sec int.
        /// </summary>
        /// <param name="Element"></param>
        /// 

        public static bool ElementExist(this IWebElement element)
        {
            try
            {
                return element.Size != null;
            }
            catch
            {
                return false;
            }
        }

        public static void SendKeysQuick(this IWebElement e, String s)
        {
            ((IJavaScriptExecutor)Driver.Instance).ExecuteScript("arguments[0].value = arguments[1];", e, s);
        }
  
        public static void ClickWait(this IWebElement Element)
        {

            //Thread.Sleep(100);
            bool isClickable = false;
            var watch = new Stopwatch();
            watch.Start();
            while (watch.Elapsed.TotalSeconds < 30)
            {

                try
                {
                    Element.Click();
                    isClickable = true;
                    break;
                }
                catch
                {
                }

            }
            if (isClickable == false)
            {
                System.Console.WriteLine("Unable to click element");
            }
        }

        public static void ClearAndKeys(this IWebElement Element, string String)
        {
            Element.Clear();
            Element.SendKeys(String);
        }

        #endregion

    }
}
