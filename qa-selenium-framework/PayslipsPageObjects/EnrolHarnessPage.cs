﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.PageObjects;
using qa_selenium_framework.Data;
using System.Threading;

namespace qa_selenium_framework
{
    public class EnrolHarnessPage : _pageBase
    {
        private readonly string harnessURL = Driver.BaseAddressOE + "/test/end2endtest";
        

        public EnrolHarnessPage GoToHarness()
        {
            GoTo(harnessURL);
            return this;
        }

        public EnrolHarnessPage CompleteHarnessScreen(EnrolTemplates e)
        {
            _enrolHarnessPage.UserName.SendKeysQuick(e.userName);
            _enrolHarnessPage.Password.SendKeysQuick(e.password);
            _enrolHarnessPage.EmployeeFirstName.SendKeysQuick(e.firstName);
            _enrolHarnessPage.EmployeeLastName.SendKeysQuick(e.lastName);
            _enrolHarnessPage.EmployeeEmail.SendKeysQuick(e.email);
            _enrolHarnessPage.CompanyName.SendKeysQuick(e.company);
            _enrolHarnessPage.Nino.SendKeysQuick(e.nino);
            _enrolHarnessPage.EmployeeCode.SendKeysQuick(e.employeeCode);
            _enrolHarnessPage.TemplateCode.SendKeysQuick(e.templateCode.ToString());
            _enrolHarnessPage.XmlData.SendKeysQuick(e.xmlData);
            _enrolHarnessPage.iterations.SendKeysQuick(e.iterations.ToString());
            _enrolHarnessPage.XmlData.SendKeysQuick(e.xmlData);
            _enrolHarnessPage.SubmitButton.ClickWait();
            Thread.Sleep(200);
            return this;
        }
 
        public EnrolHarnessPage SendL1()
        {
            GoTo(harnessURL);
            CompleteHarnessScreen(EnrolTemplates.GetL1());
            return this;
        }

        public EnrolHarnessPage SendL1P()
        {
            GoTo(harnessURL);
            CompleteHarnessScreen(EnrolTemplates.GetL1P());
            return this;
        }

        public EnrolHarnessPage SendL1T()
        {
            GoTo(harnessURL);
            CompleteHarnessScreen(EnrolTemplates.GetL1T());
            return this;
        }

        public EnrolHarnessPage SendL1Opt()
        {
            GoTo(harnessURL);
            CompleteHarnessScreen(EnrolTemplates.GetL1Opt());
            return this;
        }

        public EnrolHarnessPage SendL2_3()
        {
            GoTo(harnessURL);
            CompleteHarnessScreen(EnrolTemplates.GetL2_3());
            return this;
        }

        public EnrolHarnessPage SendL4()
        {
            GoTo(harnessURL);
            CompleteHarnessScreen(EnrolTemplates.GetL4());
            return this;
        }

        public EnrolHarnessPage SendL6()
        {
            GoTo(harnessURL);
            CompleteHarnessScreen(EnrolTemplates.GetL6());
            return this;
        }

        public EnrolHarnessPage SendL0()
        {
            GoTo(harnessURL);
            CompleteHarnessScreen(EnrolTemplates.GetL0());
            return this;
        }

        public EnrolHarnessPage SendEnrolLetter(EnrolTemplates Letter)
        {
            GoTo(harnessURL);
            CompleteHarnessScreen(Letter);
            return this;
        }

        public EnrolHarnessPage SendAllLetters()
        {
            SendL1().SendL1P().SendL1T().SendL1Opt().SendL2_3().SendL4().SendL6().SendL0();
            return this;
        }

        public EnrolHarnessPage LogInAsEmp(EnrolTemplates e)
        {
            SendEnrolLetter(e);
            loginPageOP().LogOut();

            string[] emailSplit = e.email.Split('@');

            string enrolEmail = emailSplit[0] + "+0@" + emailSplit[1];

            e.email = enrolEmail;

            gmailPage().ClickGmailLink(enrolEmail, "Employee Registration", "https://openpayslips-trunk.project-aurora.co.uk");

            loginPageOP().CreatePassword().LoginWithEmployee(e);

            return this;
        }

    }
}
