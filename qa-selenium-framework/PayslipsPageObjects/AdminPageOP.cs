﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.PageObjects;
using qa_selenium_framework.Data;
using System.Threading;

namespace qa_selenium_framework
{
    public class AdminPageOP : _pageBase
    {
        private static readonly string adminURL = Driver.BaseAddressOP + "/Administration";
        private readonly string financeURL = adminURL + "/Finance";

        public AdminPageOP GoToAdmin()
        {
            GoTo(adminURL);
            if (IsElementPresent(By.CssSelector(".widget-content .btn")))
            {
                _loginPageOP.logOffAndProceed.ClickWait();
                loginPageOP().AdminLogin();
                GoTo(adminURL);
            }

            return this;
        }

        public AdminPageOP GoToFinance()
        {
            GoToAdmin();
            GoTo(financeURL);
            return this;
        }

        public AdminPageOP SearchTenant(TenantOP t)
        {
            _adminPageOP.clientSearch.SendKeysQuick(t.AdminEmail);
            _adminPageOP.clientSearchButton.ClickWait();
            return this;
        }

        public AdminPageOP setIBSL(TenantOP t)
        {
            GoToFinance();
            SearchTenant(t);
            Thread.Sleep(1000);
            _adminPageOP.editclientButton.ClickWait();
            _adminPageOP.ibsl.ClickWait();
            Thread.Sleep(200);
            _adminPageOP.updateClientButton.ClickWait();
            return this;
        }

        public AdminPageOP setIntex(TenantOP t)
        {
            GoToFinance();
            SearchTenant(t);
            Thread.Sleep(1000);
            _adminPageOP.editclientButton.ClickWait();
            _adminPageOP.intex.ClickWait();
            _adminPageOP.updateClientButton.ClickWait();
            return this;
        }

        public String GetAccountID(TenantOP t)
        {
            GoToFinance();
            var accountID = "";
            GoToFinance();
            SearchTenant(t);
            Thread.Sleep(1000);
            accountID = _adminPageOP.accountId.Text;
            return accountID;
        }

    }
}
