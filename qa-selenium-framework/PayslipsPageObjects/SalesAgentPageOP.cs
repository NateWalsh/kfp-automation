﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.PageObjects;
using qa_selenium_framework.Data;

namespace qa_selenium_framework
{
    public class SalesAgentPageOP : _pageBase
    {
        SalesAgentPageOP CompleteSalesPage(TenantOP t)
        {
            _salesAgentPageOP.pivRef.ClearAndKeys(t.PivRef);
            _salesAgentPageOP.OrganisationName.ClearAndKeys(t.OrgName);
            _salesAgentPageOP.Address1.ClearAndKeys(t.Add1);
            _salesAgentPageOP.Address2.ClearAndKeys(t.Add2);
            _salesAgentPageOP.Town.ClearAndKeys(t.Town);
            _salesAgentPageOP.Postcode.ClearAndKeys(t.Postcode);
            _salesAgentPageOP.CustomerEmail.SendKeys(t.AdminEmail);
            _salesAgentPageOP.CustomerName.SendKeys(t.Forename);
            _salesAgentPageOP.CustomerSurname.SendKeys(t.Surname);
            _salesAgentPageOP.TelephoneNumber.SendKeys(t.Telephone);
            SelectByValue(_salesAgentPageOP.customerType, t.CustomerTypeOP);
            SelectByText(_salesAgentPageOP.PaymentOptions, t.PaymentOptions);
            ScrollToElement(_salesAgentPageOP.nextButtonSales);
            if (!t.OPSelected)
            {
                _salesAgentPageOP.OpModuleSelected.ClickWait();
            }
            else
            {
                _salesAgentPageOP.OpModuleDiscount.ClearAndKeys(t.OPDiscount.ToString());
                _salesAgentPageOP.OpNumberOfEmployees.ClearAndKeys(t.NumberOfOP.ToString());
                _salesAgentPageOP.StartDate.ClearAndKeys(t.OPContractStart.ToShortDateString() + Keys.Enter + Keys.Tab);
            }

            if (t.AESelected)
            {
                _salesAgentPageOP.AeModuleSelected.ClickWait();
                _salesAgentPageOP.AeModuleDiscount.ClearAndKeys(t.AEDiscount.ToString());
                _salesAgentPageOP.AeNumberOfEmployees.ClearAndKeys(t.NumberOfAE.ToString());
                _salesAgentPageOP.AeStartDate.SendKeys(t.AEContractStart.ToShortDateString() + Keys.Enter + Keys.Tab);
            }

            if (t.OESelected)
            {
                _salesAgentPageOP.OeModuleSelected.ClickWait();
                _salesAgentPageOP.OeModuleDiscount.ClearAndKeys(t.OEDiscount.ToString());
                _salesAgentPageOP.OeNumberOfEmployees.ClearAndKeys(t.NumberOfOE.ToString());
                _salesAgentPageOP.OeStartDate.SendKeys(t.OEContractStart.ToShortDateString() + Keys.Enter + Keys.Tab);
            }

            if (t.RPESelected)
            {
                _salesAgentPageOP.RpeModuleSelected.ClickWait();
                _salesAgentPageOP.RpeModuleDiscount.ClearAndKeys(t.RPEDiscount.ToString());
                _salesAgentPageOP.RpeCompanies.ClearAndKeys(t.NumberOfRPE.ToString());
                _salesAgentPageOP.RpeStartDate.SendKeys(t.RPEContractStart.ToShortDateString() + Keys.Enter + Keys.Tab);
            }

            _salesAgentPageOP.calculate.ClickWait();
            ScrollToElement(_salesAgentPageOP.nextButtonSales);
            _salesAgentPageOP.nextButtonSales.ClickWait();

            _salesAgentPageOP.firstAuthorisationTickbox.ClickWait();
            _salesAgentPageOP.secondAuthorisationTickbox.ClickWait();

            _salesAgentPageOP.NameOfBank.ClearAndKeys(t.BankName);
            _salesAgentPageOP.NameOfAccountHolder.ClearAndKeys(t.AccountName);
            _salesAgentPageOP.AccountNumber.ClearAndKeys(t.AccountNumber);
            _salesAgentPageOP.sortcode1.SendKeys(t.SortCode1);
            _salesAgentPageOP.sortcode2.SendKeys(t.SortCode2);
            _salesAgentPageOP.sortcode3.SendKeys(t.SortCode3);

            _salesAgentPageOP.verify.ClickWait();
            _salesAgentPageOP.sendCustomerDetailsBtn.ClickWait();

            return this;
        }

        public SalesAgentPageOP AgreeContract(TenantOP t)
        {
            if (t.OPSelected || t.AESelected || t.OESelected)
            {
                ScrollToElement(_salesAgentPageOP.opTermsLink);
                _salesAgentPageOP.opTermsLink.ClickWait();
                _salesAgentPageOP.opTermsAccept.ClickWait();
            } else
            {
                ScrollToElement(_salesAgentPageOP.rpeTermsLink);
            }
            if (t.RPESelected)
            {
                _salesAgentPageOP.rpeTermsLink.ClickWait();
                _salesAgentPageOP.rpeTermsAccept.ClickWait();
            }
            if (t.OPSelected || t.AESelected || t.OESelected)
            {
                KillPopup();
                if (t.RPESelected)
                {
                    KillPopup();
                }
            }
            else
            {
                KillPopup();
            }

            _salesAgentPageOP.confirmContractButton.ClickWait();

            return this;
        }

        public SalesAgentPageOP CreatePassword(TenantOP t)
        {
            _salesAgentPageOP.newPassword.SendKeysQuick(t.Password);
            _salesAgentPageOP.confirmPassword.SendKeysQuick(t.Password);
            _salesAgentPageOP.setPasswordButton.ClickWait();
            return this;
        }

        public SalesAgentPageOP CreateAccount(TenantOP t)
        {
            CompleteSalesPage(t);
            gmailPage().ConfirmOPContract(t);
            AgreeContract(t);
            gmailPage().OPRegistrationConfirmation(t);
            CreatePassword(t);
            return this;
        }

        public SalesAgentPageOP CreateDefaultAccount()
        {
            var t = TenantOP.GetDefault();
            CreateAccount(t);

            return this;
        }

        public SalesAgentPageOP CreateRPEOnly()
        {
            var t = TenantOP.RPEOnly();
            CreateAccount(t);
            return this;
        }

        public SalesAgentPageOP CreateOPOnly()
        {
            var t = TenantOP.OPOnly();
            CreateAccount(t);
            return this;
        }

        public SalesAgentPageOP CreateAEOnly()
        {
            var t = TenantOP.AEOnly();
            CreateAccount(t);
            return this;
        }

    }
}
