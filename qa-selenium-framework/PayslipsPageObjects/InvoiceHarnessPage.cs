﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.PageObjects;
using qa_selenium_framework.Data;
using System.Threading;

namespace qa_selenium_framework
{
    public class InvoiceHarnessPage : _pageBase
    {
        private readonly string billingHarness = Driver.BaseAddressOP + "/openpayetest/testbillingmenu";


        public InvoiceHarnessPage GoToHarness()
        {
            adminPageOP().GoToAdmin();
            GoTo(billingHarness);
            return this;
        }

        public InvoiceHarnessPage RunIBSLInvoiceForTenant(TenantOP t, string billingDate)
        {
            var accountId = adminPageOP().setIBSL(t).GetAccountID(t);
            GoToHarness();
            _invoiceHarnessPage.singleInvoiceLink.ClickWait();
            _invoiceHarnessPage.accountId.SendKeys(accountId);
            _invoiceHarnessPage.billingMonth.SendKeys(billingDate);
            _invoiceHarnessPage.generateButton.ClickWait();
            adminPageOP().GoToFinance().SearchTenant(t);


            return this;
        }

        public InvoiceHarnessPage RunIntexInvoiceForTenant(TenantOP t, string billingDate)
        {
            var accountId = adminPageOP().setIntex(t).GetAccountID(t);
            GoToHarness();
            _invoiceHarnessPage.singleInvoiceLink.ClickWait();
            _invoiceHarnessPage.accountId.SendKeys(accountId);
            _invoiceHarnessPage.billingMonth.SendKeys(billingDate);
            _invoiceHarnessPage.generateButton.ClickWait();
            adminPageOP().GoToFinance().SearchTenant(t);

            return this;
        }

    }
}
