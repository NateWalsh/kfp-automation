﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.PageObjects;
using qa_selenium_framework.Data;

namespace qa_selenium_framework
{
    public class LoginPageOP : _pageBase
    {
        private readonly string signInURL = Driver.BaseAddressOP + "/Account/LogOn";
        private readonly string salesSignInURL = Driver.BaseAddressOP + "/Sales";
        private readonly string logOutURL = Driver.BaseAddressOP + "/Account/LogOff";

        public UserCredentials User
        {
            get
            {
                return LoginDetails.GetUserCredentials(Driver.UserContext.User, Driver.UserContext.Environment);
            }
        }



        LoginPageOP GoToLogin()
        {
            GoTo(signInURL);
            return this;
        }

        LoginPageOP GoToSalesLogin()
        {
            GoTo(salesSignInURL);
            return this;
        }

        public LoginPageOP LogOut()
        {
            GoTo(logOutURL);
            return this;
        }

        public LoginPageOP Login()
        {
            GoToLogin();
            _loginPageOP.username.ClearAndKeys(User.PayslipsUserName);
            _loginPageOP.password.ClearAndKeys(User.Password);
            _loginPageOP.submit.Click();
            return this;
        }

        public LoginPageOP LoginWithEmployee(EnrolTemplates e)
        {
            string[] emailSplit = e.email.Split('@');
            string enrolEmail = emailSplit[0] + "+0@" + emailSplit[1];
            _loginPageOP.username.ClearAndKeys(e.email);
            _loginPageOP.password.ClearAndKeys(User.Password);
            _loginPageOP.submit.ClickWait();
            return this;
        }

        public LoginPageOP SalesLogin()
        {
            GoToSalesLogin();
            _loginPageOP.username.ClearAndKeys(User.PayslipsSales);
            _loginPageOP.password.ClearAndKeys(User.Password);
            _loginPageOP.submitSales.ClickWait();
            return this;
        }

        public LoginPageOP AdminLogin()
        {
            GoToLogin();
            _loginPageOP.username.ClearAndKeys(User.PayslipsAdmin);
            _loginPageOP.password.ClearAndKeys(User.PayslipsAdminPass);
            _loginPageOP.submit.ClickWait();
            return this;
        }

        public LoginPageOP CreatePassword()
        {
            _loginPageOP.NewPassword.SendKeys(User.Password);
            _loginPageOP.ConfirmPassword.SendKeys(User.Password);
            _loginPageOP.submit.ClickWait();
            _loginPageOP.loginButton.ClickWait();
            return this;
        }
    }
}
