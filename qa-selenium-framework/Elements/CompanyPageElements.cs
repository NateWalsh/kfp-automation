﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qa_selenium_framework
{
    public class CompanyPageElements
    {
        #region Company Navigation
        //Company Navigation
        public IWebElement companyMenu
        {
            get { return Driver.Instance.FindElement(By.LinkText("Company")); }
        }

        public IWebElement CompanyDashboard
        {
            get { return Driver.Instance.FindElement(By.PartialLinkText("Dashboard")); }
        }



        public IWebElement addCompany
        {
            get {return Driver.Instance.FindElement(By.PartialLinkText("Add New Company")); }
        }
        #endregion

        #region Tabs
        public IWebElement AllCompaniesTab
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[href^='/Company'][href$='/List']")); }
        }

        #endregion

        #region All Companies

        public IList<IWebElement> CompanyRows
        {
            get { return Driver.Instance.FindElements(By.CssSelector(".pl0")); }
        }

        public IWebElement DeleteWithCompanyNumber(string CompanyNo)
        {
            { return Driver.Instance.FindElement(By.CssSelector("[href*='" + CompanyNo + "'][href$='Delete']")); }
        }

        public IWebElement DeleteIcon
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[title='Delete Company']")); }
        }

        public IWebElement ConfirmDeleteCheckbox
        {
            get { return Driver.Instance.FindElement(By.Id("DeleteIsConfirmed")); }
        }

        public IWebElement ConfirmDeleteButton
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[type='submit']")); }
        }



        #endregion

        #region Add Company Wizard

        #region The Basics
        public IWebElement companyName
        {
            get { return Driver.Instance.FindElement(By.Id("CompanyName")); }
        }

        public IWebElement PreviousRtiTrue
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[name='PreviouslyInRTI'][value='True']")); }
        }

        public IWebElement PreviousRtiFalse
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[name='PreviouslyInRTI'][value='False']")); }
        }

        public IWebElement HasWeeklyTrue
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[name='HasWeekly'][value='True']")); }
        }

        public IWebElement HasWeeklyFalse
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[name='HasWeekly'][value='False']")); }
        }

        public IWebElement WeeklyDate
        {
            get { return Driver.Instance.FindElement(By.Id("CurrentWeeklyPayDate")); }
        }

        public IWebElement HasMonthlyTrue
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[name='HasMonthly'][value='True']")); }
        }

        public IWebElement HasMonthlyFalse
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[name='HasMonthly'][value='False']")); }
        }

        public IWebElement MonthlyDate
        {
            get { return Driver.Instance.FindElement(By.Id("CurrentMonthlyPayDate")); }
        }

        public IWebElement Step1Next
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[name='ButtonCommand'][value='next']")); }
        }

        public IWebElement Step1Exit
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[name='ButtonCommand'][value='exit']")); }
        }
        #endregion

        #region Commercial
        public IWebElement BusinessType
        {
            get { return Driver.Instance.FindElement(By.Id("BusinessTypeId")); }
        }

        public IWebElement Address1
        {
            get { return Driver.Instance.FindElement(By.Id("AddressFirstLine")); }
        }

        public IWebElement Address2
        {
            get { return Driver.Instance.FindElement(By.Id("AddressLines")); }
        }

        public IWebElement Postcode
        {
            get { return Driver.Instance.FindElement(By.Id("Postcode")); }
        }

        public IWebElement Telephone
        {
            get { return Driver.Instance.FindElement(By.Id("Telephone")); }
        }

        public IWebElement Next
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[type='submit']")); }
        }

        public IWebElement Exit
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[class='btn ml10']")); }
        }

        #endregion

        #region Government
        public IWebElement OfficeNumber
        {
            get { return Driver.Instance.FindElement(By.Id("PAYEOfficeNumber")); }
        }

        public IWebElement ReferenceNumber
        {
            get { return Driver.Instance.FindElement(By.Id("PAYEOfficeRef")); }
        }

        public IWebElement AccountsOfficeRef
        {
            get { return Driver.Instance.FindElement(By.Id("AccountsOfficeReference")); }
        }

        public IWebElement CorpTaxRef
        {
            get { return Driver.Instance.FindElement(By.Id("CorpTaxRef")); }
        }

        public IWebElement SelfAssessmentRef
        {
            get { return Driver.Instance.FindElement(By.Id("SelfAssessTaxRef")); }
        }

        public IWebElement EmployerAllowanceTrue
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[name='ClaimEmployementAllowance'][value='True']")); }
        }

        public IWebElement EmployerAllowanceFalse
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[name='ClaimEmployementAllowance'][value='False']")); }
        }

        public IWebElement SmallEmployerTrue
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[name='SERelible'][value='True']")); }
        }

        public IWebElement SmallEmployerFalse
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[name='SERelible'][value='False']")); }
        }

        public IWebElement RTITrue
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[name='RtiActive'][value='True']")); }
        }

        public IWebElement RTIFalse
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[name='RtiActive'][value='False']")); }
        }

        public IWebElement OnlineFilingKashFlow
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[name='FilingAgentId'][value='1']")); }
        }

        public IWebElement OnlineFilingOwnCreds
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[name='FilingAgentId'][value='0']")); }
        }
        #endregion

        #region Payment Options
        public IWebElement PaymentMenthod
        {
            get { return Driver.Instance.FindElement(By.Id("PaymentMethodId")); }
        }

        public IWebElement ElectronicPayment
        {
            get { return Driver.Instance.FindElement(By.Id("BankTransferMethodId")); }
        }

        public IWebElement BankAccountName
        {
            get { return Driver.Instance.FindElement(By.Id("AccountName")); }
        }

        public IWebElement SortCode
        {
            get { return Driver.Instance.FindElement(By.Id("SortCode")); }
        }

        public IWebElement AccountNumber
        {
            get { return Driver.Instance.FindElement(By.Id("AccountNumber")); }
        }

        public IWebElement BACSRef
        {
            get { return Driver.Instance.FindElement(By.Id("BACSReference")); }
        }

        #endregion

        public IWebElement ExitCompanySetup
        {
            get { return Driver.Instance.FindElement(By.PartialLinkText("Exit Company Setup")); }
        }

        #endregion

    }
}
