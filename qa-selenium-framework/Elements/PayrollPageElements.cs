﻿using OpenQA.Selenium;
using qa_selenium_framework.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qa_selenium_framework.Elements
{
    public class PayrollPageElements
    {
        #region Navigation
        public IWebElement PayrollMenu
        {
            get
            {
                return Driver.Instance.FindElement(By.LinkText("Payroll"));
            }
        }

        public IWebElement PayslipsAndPeriods
        {
            get
            {
                return Driver.Instance.FindElement(By.PartialLinkText("Payslips & Periods"));
            }
        }

        public IWebElement OnlineFiling
        {
            get
            {
                return Driver.Instance.FindElement(By.PartialLinkText("Online Filing"));
            }
        }

        public IWebElement WeeklyPayTab
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[href$='weekly']"));
            }
        }

        public IWebElement MonthlyPayTab
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[href$='monthly']"));
            }
        }
        #endregion


        public IWebElement LastPeriodExist
        {
            get
            {
                return Driver.Instance.FindElement(By.PartialLinkText("Go to last finalised"));
            }
        }
        public IWebElement FinaliseButton
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[data-bind='visible: IsEnabled'] [class='icon-finalise']"));
            }
        }

        public IList<IWebElement> AllPayslips
        {
            get { return Driver.Instance.FindElements(By.CssSelector(".row-clickable")); }
        }

        public IList<IWebElement> DeleteAllPayslips
        {
            get { return Driver.Instance.FindElements(By.CssSelector("a[title = 'Delete Payslip']")); }
        }

        public IWebElement ConfirmDeletePayslip
        {
            get { return Driver.Instance.FindElement(By.Id("DeleteIsConfirmed")); }
        }

        public IWebElement DeleteButton
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[type='submit']")); }
        }

        

        public IWebElement ConfirmFinaliseButton
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("btn-lock-paydate"));
            }
        }

        public IWebElement FinishFinalise
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector(".grey"));
            }
        }

        public IWebElement PreviousPeriod
        {
            get
            {
                return Driver.Instance.FindElement(By.PartialLinkText("«"));
            }
        }

        public IWebElement RestoreButton
        {
            get
            {
                return Driver.Instance.FindElement(By.PartialLinkText("Restore"));
            }
        }
        
        public IWebElement ConfirmRestoreCheckbox
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("RollbackIsConfirmed"));
            }
        }

        public IWebElement ConfirmRestoreButton
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[type='submit']"));
            }
        }

        public IWebElement PayDateTitle
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[class='title-cell psl-title-cell'"));
            }
        }

        public IWebElement CreateSaveButton
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[class='btn btn-primary btn-hasicon']"));
            }
        }

        public IWebElement DoneSavingButton
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[class='btn btn-primary btn-hasicon disabled']"));
            }
        }

        

        public IWebElement ApprovePayslip
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[class='psl-approval-ctrl'] [class='icon-uncheck']"));
            }
        }

        public IWebElement Approved
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[class='psl-approval-ctrl'] [class='icon-activecheck']"));
            }
        }

        public IWebElement CriticalError
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("paydate-issues-rticrit"));
            }
        }

        public IWebElement ReasonForLateSubmission
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[value='0-0-7']"));
            }
        }

        public IWebElement ReasonForLateOKButton
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[aria-labelledby='ui-dialog-title-late-reasons-dialog'] [type='button']"));
            }
        }

        #region Payslip
        public IWebElement NetPayLabel
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[data-bind='html: NetPay']"));
            }
        }

        public IWebElement TaxableGrossLabel
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[data-bind='html: GrossPay']"));
            }
        }

        public IWebElement TaxLabel
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[data-bind='html: Tax']"));
            }
        }
        
        public IWebElement NICsLabel
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[data-bind='html: NICs']"));
            }
        }

        public IWebElement NICsErLabel
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[data-bind='html: NICsEr']"));
            }
        }

        #endregion

        #region Payment Types

        public IWebElement AddNewPayment
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[id='allowance-column'] tr[class='new']"));
            }
        }

        public IWebElement Amount
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("tr:last-of-type [class='psia'] [class='txt']"));
            }
        }

        public IWebElement RepeatOn
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("tr:last-of-type [title='Repeats on following payslips. Click to end on this payslip.']"));
            }
        }

        public IWebElement RepeatOff
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("tr:last-of-type [title='Ends on this payslip. Click to repeat on following payslips.']"));
            }
        }

        public IWebElement Adjustment
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[style*='display: block'] a[data-node='2']"));
            }
        }

        public IWebElement Basic
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[style*='display: block'] a[data-node='3']"));
            }
        }

        public IWebElement Time
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[style*='display: block'] a[data-node='12']"));
            }
        }

        public IWebElement Benefits
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[style*='display: block'] a[data-node='4']"));
            }
        }

        public IWebElement Expenses
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[style*='display: block'] a[data-node='5']"));
            }
        }

        public IWebElement Funds
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[style*='display: block'] a[data-node='6']"));
            }
        }

        public IWebElement Schemes
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[style*='display: block'] a[data-node='10']"));
            }
        }

        public IWebElement LumpSum
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[style*='display: block'] a[data-node='7']"));
            }
        }
        #endregion

        #region Second Level Payment types

        public IWebElement Tax
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[style*='display: block'] a[data-node='236']"));
            }
        }

        public IWebElement NICs
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[style*='display: block'] a[data-node='237']"));
            }
        }

        public IWebElement Statutory
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[style*='display: block'] a[data-node='333']"));
            }
        }

        public IWebElement AlreadyPaid
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[style*='display: block'] a[data-node='347']"));
            }
        }

        public IWebElement OtherAdjustment
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[style*='display: block'] a[data-node='389']"));
            }
        }

        public IWebElement Salary
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[style*='display: block'] a[data-node='20']"));
            }
        }

        public IWebElement Wage
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[style*='display: block'] a[data-node='21']"));
            }
        }

        public IWebElement Advance
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[style*='display: block'] a[data-node='50']"));
            }
        }

        public IWebElement Commission
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[style*='display: block'] a[data-node='33']"));
            }
        }

        public IWebElement Tips
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[style*='display: block'] a[data-node='14']"));
            }
        }

        public IWebElement Fees
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[style*='display: block'] a[data-node='36']"));
            }
        }

        public IWebElement BackPay
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[style*='display: block'] a[data-node='22']"));
            }
        }

        public IWebElement OtherBasic
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[style*='display: block'] a[data-node='390']"));
            }
        }
        #endregion

        #region Third Level Payment Types

        public IWebElement RefundTax
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[style*='display: block'] a[data-node='238']"));
            }
        }

        public IWebElement OverrideTax
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[style*='display: block'] a[data-node='328']"));
            }
        }

        public IWebElement ErNICs
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[style*='display: block'] a[data-node='1005']"));
            }
        }

        public IWebElement RefundNICs
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[style*='display: block'] a[data-node='239']"));
            }
        }

        public IWebElement OverrideNICs
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[style*='display: block'] a[data-node='329']"));
            }
        }

        public IWebElement OverrideErNICs
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[style*='display: block'] a[data-node='1006']"));
            }
        }

        public IWebElement Monthly
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[style*='display: block'] a[data-node='31']"));
            }
        }

        public IWebElement ProRataDays
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[style*='display: block'] a[data-node='32']"));
            }
        }

        #endregion
    }
}
