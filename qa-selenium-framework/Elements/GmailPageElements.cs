﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qa_selenium_framework.Elements
{
    public class GmailPageElements
    {
        public IWebElement Email
        {
            get { return Driver.Instance.FindElement(By.Id("Email")); }
        }

        public IWebElement Next
        {
            get { return Driver.Instance.FindElement(By.Id("next")); }
        }

        public IWebElement Password
        {
            get { return Driver.Instance.FindElement(By.Id("Passwd")); }
        }

        public IWebElement SignIn
        {
            get { return Driver.Instance.FindElement(By.Id("signIn")); }
        }

        public IWebElement SearchText
        {
            get { return Driver.Instance.FindElement(By.Id("sbq")); }
        }

        public IWebElement SearchButton
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[class='search-form-submit'][value='Search Mail']")); }
        }

        public IWebElement TermsEmail
        {
            get { return Driver.Instance.FindElement(By.PartialLinkText("Terms And Conditions")); }
        }

        public IWebElement ActivateEmail
        {
            get { return Driver.Instance.FindElement(By.PartialLinkText("Activate your Kashflow")); }
        }

        public IWebElement EmailLink
        {
            get { return Driver.Instance.FindElement(By.PartialLinkText("go.kashflowpayroll.com")); }
        }

    }
}
