﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qa_selenium_framework
{
    public class LoginPageElements
    {
       
        public IWebElement username
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("UserName"));
            }
        }

        public IWebElement password
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("Password"));
            }
        }

        public IWebElement submit
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[type='submit']"));
            }
        }

        public IWebElement releaseNotes
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector(".ui-button-text"));
            }
        }

        public IWebElement demo
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector(".giant-choice .alt"));
            }
        }

        public IWebElement SignUpForFree
        {
            get
            {
                return Driver.Instance.FindElement(By.LinkText("Sign up for free."));
            }
        }

        public IWebElement FirstName
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("FirstName"));
            }
        }

        public IWebElement LastName
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("LastName"));
            }
        }

        public IWebElement EmailAddress
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("EmailAddress"));
            }
        }

        public IWebElement Phone
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("Phone"));
            }
        }

        public IWebElement TermsAccepted
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("TermsAccepted"));
            }
        }


    }
}
