﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qa_selenium_framework.Elements
{
    public class PensionPageElements
    {

        #region Navigation
        public IWebElement PensionMenu
        {
            get
            {
                return Driver.Instance.FindElement(By.LinkText("Pension and AE"));
            }
        }

        public IWebElement AllPensionSchemes
        {
            get
            {
                return Driver.Instance.FindElement(By.PartialLinkText("All Pension Schemes"));
            }
        }

        public IWebElement SetupDetails
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[href$='Configure']"));
            }
        }

        public IWebElement Communications
        {
            get
            {
                return Driver.Instance.FindElement(By.PartialLinkText("Communications"));
            }
        }

        public IWebElement AddNewPension
        {
            get
            {
                return Driver.Instance.FindElement(By.PartialLinkText("Add New Pension Scheme"));
            }
        }

        public IWebElement AESetup
        {
            get
            {
                return Driver.Instance.FindElement(By.PartialLinkText("AE Setup"));
            }
        }

        #region tabs

        public IWebElement SetupTab
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[class$='page-tab']:nth-child(1)"));
            }
        }

        public IWebElement AETab
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[class$='page-tab']:nth-child(2)"));
            }
        }

        public IWebElement OutputfileTab
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[class$='page-tab']:nth-child(3)"));
            }
        }

        #endregion

        #endregion

        #region AE Wizard
        public IWebElement ExitAEWizard
        {
            get
            {
                return Driver.Instance.FindElement(By.PartialLinkText("All Pension Schemes"));
            }
        }

        public IWebElement NextStep1
        {
            get
            {
                return Driver.Instance.FindElement(By.PartialLinkText("Next"));
            }
        }

        public IWebElement StagingDate
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("StagingDate"));
            }
        }

        public IWebElement Previous
        {
            get
            {
                return Driver.Instance.FindElement(By.PartialLinkText("« Previous"));
            }
        }

        public IWebElement Next
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[class='btn btn-primary ml10']"));
            }
        }

        public IWebElement PostponementTrue
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[id='Postponement'][value='True']"));
            }
        }

        public IWebElement PostponementFalse
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[id='Postponement'][value='False']"));
            }
        }

        public IWebElement Skip
        {
            get
            {
                return Driver.Instance.FindElement(By.PartialLinkText("Skip"));
            }
        }

        public IWebElement PostponementStarters
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("PostPonementPeriodStarters"));
            }
        }

        public IWebElement PostponementStartersValue
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("PostPonementPeriodStartersValue"));
            }
        }

        public IWebElement PostponementJobholders
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("PostPonementPeriodWorkers"));
            }
        }

        public IWebElement PostponementJobholdersValue
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("PostPonementPeriodWorkersValue"));
            }
        }

        public IWebElement OutputFilesTrue
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[name='useOutputFiles'][value='true'"));
            }
        }

        public IWebElement OutputFilesFalse
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[name='useOutputFiles'][value='false'"));
            }
        }

        public IWebElement OutputFileType
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[data-bind*='options: Items']"));
            }
        }

        public IWebElement OutputFileReference
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[name='FileReference']"));
            }
        }

        public IWebElement WithholdingTrue
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[name='UseWithholding'][value='true'"));
            }
        }

        public IWebElement WithholdingFalse
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[name='UseWithholding'][value='false'"));
            }
        }

        public IWebElement WithheldWeeks
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[name='WithholdWeeks']"));
            }
        }



        public IWebElement Group
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("Group"));
            }
        }


        public IWebElement AddGroup
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("add-0"));
            }
        }

        public IWebElement PaymentSource
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("Payment Source"));
            }
        }

        public IWebElement AddPaymentSource
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("add-1"));
            }
        }

        public IWebElement Category
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("Category"));
            }
        }
        
        public IWebElement AddCategory
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("add-0"));
            }
        }

        public IWebElement WorkerGroupID
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("Worker Group ID"));
            }
        }

        public IWebElement AddWorkerGroupID
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("add-0"));
            }
        }

        public IWebElement AddEEToEr
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("AddEeeToEer"));
            }
        }

        public IWebElement AddNew
        {
            get
            {
                return Driver.Instance.FindElement(By.PartialLinkText("Add New"));
            }
        }

        #region Pension Details

        public IWebElement PensionName
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("PensionName"));
            }
        }

        public IWebElement SCON
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("Scon"));
            }
        }

        public IWebElement UniqueID
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("PensionRef"));
            }
        }

        public IWebElement EmployeePortal
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("EmployeePortal"));
            }
        }

        public IWebElement Website
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("ProviderWebsite"));
            }
        }

        public IWebElement ProviderAddress1
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("Address1"));
            }
        }

        public IWebElement ProviderAddress2
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("Address2"));
            }
        }

        public IWebElement ProviderPostcode
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("Postcode"));
            }
        }

        public IWebElement WizardProviderPhoneNumber
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("ProviderPhoneNumber"));
            }
        }

        public IWebElement PensionProviderPhoneNumber
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("TelephoneNumber"));
            }
        }

        public IWebElement AdminName
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("ContactName"));
            }
        }

        public IWebElement AdminEmail
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("EmailAddress"));
            }
        }

        public IWebElement AdminPhoneNumber
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("TelephoneNumber"));
            }
        }

        public IWebElement AdminAddress1
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("AdminAddress1"));
            }
        }

        public IWebElement AdminAddress2
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("AdminAddress2"));
            }
        }

        public IWebElement AdminPostcode
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("AdminPostcode"));
            }
        }

        #endregion

        public IWebElement ReliefAtSource
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("_PensionRule_ReliefAtSource"));
            }
        }

        public IWebElement SalarySacrifice
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("_PensionRule_SalarySacrifice"));
            }
        }

        public IWebElement NetPayArrangement
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("_PensionRule_UnderNetPay"));
            }
        }

        public IWebElement EEPercentage
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("_EeContrMode_Percentage"));
            }
        }

        public IWebElement EEValue
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("_EeContrMode_Value"));
            }
        }

        public IWebElement EEContribution
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("EeContribution"));
            }
        }

        public IWebElement ERPercentage
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("_ErContrMode_Percentage"));
            }
        }

        public IWebElement ERValue
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("_ErContrMode_Value"));
            }
        }

        public IWebElement ERContribution
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("ErContribution"));
            }
        }

        public IWebElement IsQualifying
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("IsQualifyingScheme"));
            }
        }

        public IWebElement AEType
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("AutomaticEnrolmentType"));
            }
        }

        public IWebElement Default
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("IsDefaultAeScheme"));
            }
        }

        public IWebElement BasicRateTaxRelief
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("BasicRateTaxRelief"));
            }
        }

        public IWebElement GenerateOutputFiles
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("GenerateOutputFile"));
            }
        }

        public IWebElement CompleteSetup
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[data-bind*='click: UpdatePensionItems']"));
            }
        }

        public IWebElement ExitAESetup
        {
            get
            {
                return Driver.Instance.FindElement(By.PartialLinkText("Exit AE Setup »"));
            }
        }

        public IWebElement SubmitPension
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[type='submit']"));
            }
        }

        public IWebElement ExitPensionSetup
        {
            get
            {
                return Driver.Instance.FindElement(By.PartialLinkText("Exit Pension Setup »"));
            }
        }

        public IList<IWebElement> AllPensions
        {
            get { return Driver.Instance.FindElements(By.CssSelector(".btn-primary[title='Delete Pension']")); }
        }

        public IWebElement DeletePensionButton
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[title='Delete Pension']")); }
        }
        
        public IWebElement ConfirmDeleteTickbox
        {
            get { return Driver.Instance.FindElement(By.Id("DeleteIsConfirmed")); }
        }

        public IWebElement ConfirmDeleteButton
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[type='submit']")); }
        }


        #endregion


    }
}
