﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace qa_selenium_framework.Elements
{
    public class SalesAgentPageElements
    {
        #region Client Details
        public IWebElement ExistingCustomerTrue
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[name='IsExistingCustomer'][value='True']"));
            }
        }

        public IWebElement ExistingCustomerFalse
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[type='radio'][name='IsExistingCustomer'][value='False']"));
            }
        }
        
        public IWebElement FirstName
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("FirstName"));
            }
        }

        public IWebElement LastName
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("LastName"));
            }
        }

        public IWebElement CustomerEmail
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("CustomerEmail"));
            }
        }

        public IWebElement CustomerType
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("CustomerTypeId"));
            }
        }

        public IWebElement CompanyName
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("CompanyName"));
            }
        }

        public IWebElement Address1
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("Address1"));
            }
        }

        public IWebElement Address2
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("Address2"));
            }
        }

        public IWebElement Town
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("Town"));
            }
        }

        public IWebElement Postcode
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("Postcode"));
            }
        }

        public IWebElement Telephone
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("Telephone"));
            }
        }

        public IWebElement ClearFormButton
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("button:nth-child(1)"));
            }
        }

        public IWebElement NextButton
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("button:nth-child(3)"));
            }
        }
        #endregion

        #region Costs
        public IWebElement AutomaticEnrolmentTrue
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[name='UseAE'][value='true']"));
            }
        }

        public IWebElement AutomaticEnrolmentFalse
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[name='UseAE'][value='false']"));
            }
        }

        public IWebElement AEDate
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("AeStartDate"));
            }
        }

        public IWebElement NumberOfCompanies
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[name='NumberOfCompanies']"));
            }
        }

        public IWebElement NumberOfEmployees
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[name='NumberOfEmployees']"));
            }
        }

        public IWebElement NumberOfEss
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[name='NumberOfEss']"));
            }
        }
        
        public IWebElement NumberOfAe
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[name='NumberOfAe']"));
            }
        }

        public IWebElement CalculateButton
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("div.mt10.mb10.span12 > button"));
            }
        }
        #endregion

        #region CostsAsserts
        public IWebElement CostPerMonth
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("CostPerMonth"));
            }
        }

        public IWebElement CostPerAdditionalEmployee
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("CostPerAdditionalEmployee"));
            }
        }

        public IWebElement CostOfAnnualContract
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("TotalAnnualContractCost"));
            }
        }
        #endregion

        #region Banking Information
        public IWebElement ConfirmAccountHolder
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("IsAccountHolder"));
            }
        }

        public IWebElement ConfirmAuthorised
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("IsAuthorised"));
            }
        }

        public IWebElement SortCode1
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("SortCode1"));
            }
        }

        public IWebElement SortCode2
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("SortCode2"));
            }
        }

        public IWebElement SortCode3
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("SortCode3"));
            }
        }

        public IWebElement AccountName
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("AccountName"));
            }
        }

        public IWebElement AccountNumber
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("AccountNumber"));
            }
        }

        public IWebElement Verify
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("aGetBank"));
            }
        }
        
        public IWebElement SendMandate
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("PaperMandateRequired"));
            }
        }

        public IWebElement ValidBankIcon
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[name='Valid sort code?']"));
            }
        }


        #endregion

        #region Review Contract
        public IWebElement ReviewName
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[id='CustomerFullName']"));
            }
        }

        public IWebElement ReviewContract
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("CustomerTypeId"));
            }
        }

        public IWebElement ReviewCompanies
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("NumberOfCompanies"));
            }
        }

        public IWebElement ReviewEmployees
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("NumberOfEmployees"));
            }
        }

        public IWebElement ReviewAE
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("NumberOfAe"));
            }
        }

        public IWebElement ReviewPayrollCost
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("PayrollCostPerMonth"));
            }
        }

        public IWebElement ReviewAECost
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("AECostPerMonth"));
            }
        }

        public IWebElement ReviewMonthlyCost
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("CostPerMonth"));
            }
        }

        public IWebElement ReviewCostPerMonth
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("CostPerMonth"));
            }
        }

        public IWebElement ReviewCostPerEmp
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("CostPerAdditionalEmployee"));
            }
        }

        public IWebElement ReviewCostPerAE
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("CostPerAdditionalAE"));
            }
        }

        public IWebElement ReviewAnnual
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("TotalAnnualContractCost"));
            }
        }

        public IWebElement ReviewSubmit
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("SubmitBtn"));
            }
        }

        public IWebElement AgreeTerms
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("HasAgreedTerms"));
            }
        }

        public IWebElement OKButton
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[class*='ui-button ui']"));
            }
        }

        public IWebElement SubmitContract
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[class='btn btn-primary']"));
            }
        }

        public IWebElement UserMenu
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector(".visible-desktop"));
            }
        }

        public IWebElement LogOut
        {
            get
            {
                return Driver.Instance.FindElement(By.PartialLinkText("Logout"));
            }
        }

        public IWebElement ChoosePassword
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("NewPassword"));
            }
        }

        public IWebElement ConfirmPassword
        {
            get
            {
                return Driver.Instance.FindElement(By.Id("ConfirmPassword"));
            }
        }

        public IWebElement ActivateButton
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector("[class='btn btn-primary']"));
            }
        }

        public IWebElement DemoButton
        {
            get
            {
                return Driver.Instance.FindElement(By.CssSelector(".giant-choice .alt"));
            }
        }
     


        #endregion
    }
}
