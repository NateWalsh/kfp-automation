﻿using OpenQA.Selenium;
using qa_selenium_framework.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qa_selenium_framework.Elements
{
    public class CommsPageElements
    {
        #region Comms Tabs
        public IWebElement EnrolmentLettersTab
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[href^='/Pension'][href$='/Communications']")); }
        }

        public IWebElement TemplateTab
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[href^='/Pension'][href$='/Templates']")); }
        }
        #endregion

        public IWebElement DeliveryStatus
        {
            get { return Driver.Instance.FindElement(By.Id("SearchModel_PrintStatus")); }
        }

        public IWebElement CheckAll
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[name='checkAll']")); }
        }

        public IWebElement GenerateButton
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[type='submit']")); }
        }

        public IWebElement L1
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[data-click*='Type=2']")); }
        }

        public IWebElement L2and3
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[data-click*='Type=32']")); }
        }

        int LastInstanceOfLetter()
        {
            int letterCount = Letters.Count;
            return letterCount;
        }

        public IWebElement PublishedName
        {
            get { return Driver.Instance.FindElement(By.CssSelector(".row-clickable:nth-of-type(" + LastInstanceOfLetter() + ") td:nth-of-type(1)")); }
        }

        public IWebElement PublishedAEStatus
        {
            get { return Driver.Instance.FindElement(By.CssSelector(".row-clickable:nth-of-type(" + LastInstanceOfLetter() + ") td:nth-of-type(2)")); }
        }

        public IWebElement PublishedDoc
        {
            get { return Driver.Instance.FindElement(By.CssSelector(".row-clickable:nth-of-type(" + LastInstanceOfLetter() + ") td:nth-of-type(3)")); }
        }

        public IWebElement PublishedDate
        {
            get { return Driver.Instance.FindElement(By.CssSelector(".row-clickable:nth-of-type(" + LastInstanceOfLetter() + ") td:nth-of-type(4)")); }
        }

        public IList<IWebElement> Letters
        {
            get { return Driver.Instance.FindElements(By.CssSelector("[data-bind='foreach: commsItemsList'] tr")); }
        }

        public IList<IWebElement> LettersByName()
        {
            foreach (IWebElement e in Letters)
            {
                Console.WriteLine(e.Text);
            }

           
            return Driver.Instance.FindElements(By.CssSelector(".row-clickable"));
        }

        public IWebElement DownloadLetterButton
        {
            get { return Driver.Instance.FindElement(By.Id("btn-print-sel")); }
        }


    }
}
