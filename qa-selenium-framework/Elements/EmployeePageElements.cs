﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qa_selenium_framework.Elements
{
    public class EmployeePageElements
    {

        #region Navigation
        public IWebElement EmployeeMenu
        {
            get { return Driver.Instance.FindElement(By.CssSelector(".absicon-eees")); }
        }

        public IWebElement Overview
        {
            get { return Driver.Instance.FindElement(By.PartialLinkText("Overview")); }
        }

        public IWebElement Details
        {
            get { return Driver.Instance.FindElement(By.PartialLinkText("Details")); }
        }

        public IWebElement Pension
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[href^='/Employee'][href$='Pension']")); }
        }

        public IWebElement OpeningBalances
        {
            get { return Driver.Instance.FindElement(By.PartialLinkText("Opening Balances")); }
        }

        public IWebElement WorkingPattern
        {
            get { return Driver.Instance.FindElement(By.PartialLinkText("Working Pattern")); }
        }

        public IWebElement CalendarAndLeave
        {
            get { return Driver.Instance.FindElement(By.PartialLinkText("Calendar & Leave")); }
        }

        public IWebElement EarningsOrders
        {
            get { return Driver.Instance.FindElement(By.PartialLinkText("Earnings Orders")); }
        }

        public IWebElement AddNewEmployee
        {
            get { return Driver.Instance.FindElement(By.PartialLinkText("Add New Employee")); }
        }

        public IList<IWebElement> AllEmployees
        {
            get { return Driver.Instance.FindElements(By.CssSelector(".pull-right[title='Delete Employee']")); }
        }

        #endregion

        #region Tabs
        public IWebElement OverViewTab
        {
            get { return Driver.Instance.FindElement(By.CssSelector(".page-tab:nth-child(1)")); }
        }

        public IWebElement AllCurrentTab
        {
            get { return Driver.Instance.FindElement(By.CssSelector(".page-tab:nth-child(2)")); }
        }

        public IWebElement AllLeaversTab
        {
            get { return Driver.Instance.FindElement(By.CssSelector(".page-tab:nth-child(3)")); }
        }

        #endregion

        #region AllEmployeesScreen
        public IWebElement BigAddEmployeeButton
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[class='giant-choice']")); }
        }

        public IWebElement TrashEmployee
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[title='Delete Employee']")); }
        }

        public IList<IWebElement> TrashAllEmployee
        {
            get { return Driver.Instance.FindElements(By.CssSelector("a[title='Delete Employee']")); }
        }

        public IWebElement ConfirmDelete
        {
            get { return Driver.Instance.FindElement(By.Id("DeleteIsConfirmed")); }
        }

        public IWebElement DeleteButton
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[type='submit']")); }
        }

        public IWebElement DeleteWarning
        {
            get { return Driver.Instance.FindElement(By.CssSelector(".alert-error")); }
        }
        #endregion

        #region EmployeePension

        public IWebElement EePensionScheme
        {
            get { return Driver.Instance.FindElement(By.Id("PensionSchemeId")); }
        }

        public IWebElement EeContributions
        {
            get { return Driver.Instance.FindElement(By.Id("EmployeeContribution")); }
        }

        public IWebElement ErContributions
        {
            get { return Driver.Instance.FindElement(By.Id("EmployerContribution")); }
        }

        public IWebElement EeWorkerStatus
        {
            get { return Driver.Instance.FindElement(By.Id("WorkerStatus")); }
        }

        public IWebElement EeAeDate
        {
            get { return Driver.Instance.FindElement(By.Id("AeDate")); }
        }

        public IWebElement EeDeferralDate
        {
            get { return Driver.Instance.FindElement(By.Id("DeferralDate")); }
        }

        public IWebElement EEPensionJoinOptInDate
        {
            get { return Driver.Instance.FindElement(By.Id("JoinDate")); }
        }

        public IWebElement EeExitDate
        {
            get { return Driver.Instance.FindElement(By.Id("ExitDate")); }
        }

        public IWebElement EeOptOutDate
        {
            get { return Driver.Instance.FindElement(By.Id("OptOutDate")); }
        }

        public IWebElement EePensionInfoProvided
        {
            get { return Driver.Instance.FindElement(By.Id("PensionInfoProvided")); }
        }

        public IWebElement WorksOutsideTheUK
        {
            get { return Driver.Instance.FindElement(By.Id("WorksOutsideUK")); }
        }

        public IWebElement TransitionalPeriod
        {
            get { return Driver.Instance.FindElement(By.Id("TransitionalPeriod")); }
        }

        public IWebElement SubmitEEPension
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[type='submit']")); }
        }


        #endregion

        #region Add Employee Wizard

        #region The Basics
        public IWebElement FirstName
        {
            get { return Driver.Instance.FindElement(By.Id("FirstName")); }
        }

        public IWebElement LastName
        {
            get { return Driver.Instance.FindElement(By.Id("LastName")); }
        }

        public IWebElement JoinDate
        {
            get { return Driver.Instance.FindElement(By.Id("JoinDate")); }
        }

        public IWebElement NewToCompanyTrue
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[name='NewToCompany'][value='True']")); }
        }

        public IWebElement NewToCompanyFalse
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[name='NewToCompany'][value='False']")); }
        }

        public IWebElement StarterDeclarationA
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[name='StartingDeclaration'][value='A']")); }
        }

        public IWebElement StarterDeclarationB
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[name='StartingDeclaration'][value='B']")); }
        }

        public IWebElement StarterDeclarationC
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[name='StartingDeclaration'][value='C']")); }
        }

        public IWebElement StarterDeclarationUnknown
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[name='StartingDeclaration'][value='0']")); }
        }

        public IWebElement IsDirectorTrue
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[name='IsDirector'][value='True']")); }
        }

        public IWebElement IsDirectorFalse
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[name='IsDirector'][value='False']")); }
        }

        public IWebElement DirectorStartDate
        {
            get { return Driver.Instance.FindElement(By.Id("DirectorStartDate")); }
        }

        public IWebElement CummulativeNICSTrue
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[name='DirectorNICsCummulative'][value='True']")); }
        }

        public IWebElement CummulativeNICSFalse
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[name='DirectorNICsCummulative'][value='False']")); }
        }

        public IWebElement NextStep1
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[type='submit'][value='next']")); }
        }

        public IWebElement SaveExit
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[type='submit'][value='exit']")); }
        }

        #endregion

        #region Personal

        public IWebElement Title
        {
            get { return Driver.Instance.FindElement(By.Id("TitleId")); }
        }

        public IWebElement KnownAs
        {
            get { return Driver.Instance.FindElement(By.Id("KnownAs")); }
        }

        public IWebElement GenderMale
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[name='IsFemale'][value='False']")); }
        }

        public IWebElement GenderFemale
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[name='IsFemale'][value='True']")); }
        }
        
        public IWebElement DOB
        {
            get { return Driver.Instance.FindElement(By.Id("DoB")); }
        }

        public IWebElement MaritalStatus
        {
            get { return Driver.Instance.FindElement(By.Id("MaritalStatusId")); }
        }

        public IWebElement Next
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[type='submit']")); }
        }

        #endregion

        #region Contact

        public IWebElement Address1
        {
            get { return Driver.Instance.FindElement(By.Id("Address1")); }
        }

        public IWebElement Address2
        {
            get { return Driver.Instance.FindElement(By.Id("Address2")); }
        }

        public IWebElement Postcode
        {
            get { return Driver.Instance.FindElement(By.Id("Postcode")); }
        }
        
        public IWebElement Country
        {
            get { return Driver.Instance.FindElement(By.Id("CountryId")); }
        }

        public IWebElement WorkPhone
        {
            get { return Driver.Instance.FindElement(By.Id("WorkPhone")); }
        }

        public IWebElement HomePhone
        {
            get { return Driver.Instance.FindElement(By.Id("HomePhone")); }
        }

        public IWebElement MobilePhone
        {
            get { return Driver.Instance.FindElement(By.Id("MobilePhone")); }
        }

        #endregion

        #region Employment Details
        #endregion

        #region Government

        public IWebElement TaxCode
        {
            get { return Driver.Instance.FindElement(By.Id("TaxCodeRef")); }
        }

        public IWebElement TaxIsCumulative
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[name='TaxIsCumulative'][value='True']")); }
        }

        public IWebElement TaxIsNonCumulative
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[name='TaxIsCumulative'][value='False']")); }
        }

        public IWebElement StudentLoanTrueOld
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[name='HasStudentLoan'][value='True']")); }
        }

        public IWebElement StudentLoanFalseOld
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[name='HasStudentLoan'][value='False']")); }
        }

        public IWebElement StudentLoanSelector
        {
            get { return Driver.Instance.FindElement(By.Id("StudentLoantTypeId")); }
        }

        public IWebElement NiNo
        {
            get { return Driver.Instance.FindElement(By.Id("NiNo")); }
        }

        public IWebElement NiCat
        {
            get { return Driver.Instance.FindElement(By.Id("NiCategoryId")); }
        }
        #endregion

        #region Payment

        public IWebElement MonthlyPayCycle
        {
            get { return Driver.Instance.FindElement(By.Id("_Paycycle_Monthly")); }
        }

        public IWebElement WeeklyPayCycle
        {
            get { return Driver.Instance.FindElement(By.Id("_Paycycle_Weekly")); }
        }

        public IWebElement IntermittentlyTrue
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[name='IsPaidIntermittently'][value='True']")); }
        }

        public IWebElement IntermittentlyFalse
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[name='IsPaidIntermittently'][value='False']")); }
        }

        public IWebElement HoursPerWeek
        {
            get { return Driver.Instance.FindElement(By.Id("HoursPerWeek")); }
        }

        public IWebElement PayMethod
        {
            get { return Driver.Instance.FindElement(By.Id("PayMethodId")); }
        }

        public IWebElement PayslipDelivery
        {
            get { return Driver.Instance.FindElement(By.Id("DeliveryMethodId")); }
        }

        public IWebElement EmailAddress
        {
            get { return Driver.Instance.FindElement(By.Id("EmailAddress")); }
        }

        public IWebElement AccountName
        {
            get { return Driver.Instance.FindElement(By.Id("AccountName")); }
        }

        public IWebElement SortCode
        {
            get { return Driver.Instance.FindElement(By.Id("SortCode")); }
        }

        public IWebElement AccountNumber
        {
            get { return Driver.Instance.FindElement(By.Id("AccountNumber")); }
        }

        public IWebElement BACSRef
        {
            get { return Driver.Instance.FindElement(By.Id("BACSReference")); }
        }

        #endregion

        #endregion

        #region OBALS
        public IWebElement OBALSLink
        {
            get { return Driver.Instance.FindElement(By.PartialLinkText("Opening Balances")); }
        }

        public IWebElement PreviousGross
        {
            get { return Driver.Instance.FindElement(By.Id("GrossTaxableLastJob")); }
        }

        public IWebElement PreviousTax
        {
            get { return Driver.Instance.FindElement(By.Id("TaxPaidLastJob")); }
        }

        public IWebElement PreviousNICat
        {
            get { return Driver.Instance.FindElement(By.Id("NiCategoryId")); }
        }

        public IWebElement GrossForNICs
        {
            get { return Driver.Instance.FindElement(By.Id("NICableGrossEee")); }
        }

        public IWebElement PreviousEesNics
        {
            get { return Driver.Instance.FindElement(By.Id("NICPaidEee")); }
        }

        public IWebElement PreviousErsNics
        {
            get { return Driver.Instance.FindElement(By.Id("NICPaidEer")); }
        }

        public IWebElement LEL
        {
            get { return Driver.Instance.FindElement(By.Id("NICableGross1a")); }
        }

        public IWebElement LELtoPT
        {
            get { return Driver.Instance.FindElement(By.Id("NICableGross1b")); }
        }

        public IWebElement PTtoAUP
        {
            get { return Driver.Instance.FindElement(By.Id("NICableGross1c")); }
        }

        public IWebElement UAPtoUEL
        {
            get { return Driver.Instance.FindElement(By.Id("NICableGross1d")); }
        }

        public IWebElement PreviousStudentLoan
        {
            get { return Driver.Instance.FindElement(By.Id("NICableGross1d")); }
        }

        #endregion

    }
}
