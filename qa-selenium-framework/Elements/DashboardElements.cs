﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qa_selenium_framework.Elements
{
    public class DashboardElements
    {
        public IWebElement EmployeePicker
        {
            get { return Driver.Instance.FindElement(By.Id("employee-picker")); }
        }

        public IWebElement EmployeePickerText
        {
            get { return Driver.Instance.FindElement(By.Id("eep-auto")); }
        }

        public IWebElement CompanyPicker
        {
            get { return Driver.Instance.FindElement(By.Id("company-picker")); }
        }

        public IWebElement CompanyPickerText
        {
            get { return Driver.Instance.FindElement(By.Id("com-auto")); }
        }



    }
}
