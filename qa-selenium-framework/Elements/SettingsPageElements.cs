﻿using OpenQA.Selenium;
using qa_selenium_framework.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace qa_selenium_framework.Elements
{
    public class SettingsPageElements
    {
        #region Navigation Elements
        public IWebElement SettingsMenu
        {
            get { return Driver.Instance.FindElement(By.LinkText("Settings")); }
        }

        public IWebElement Subscriptions
        {
            get { return Driver.Instance.FindElement(By.PartialLinkText("Subscription & Billing")); }
        }

        public IWebElement Profile
        {
            get { return Driver.Instance.FindElement(By.PartialLinkText("Profile")); }
        }

        public IWebElement BillingTab
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[href='/Settings/47062/0/Subscription/Billing']")); }
        }

        public IWebElement ChangePasswordTab
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[href='/Settings/47062/0/Profile/Password']")); }
        }
        #endregion

        #region Subscriptions screen
        public IWebElement FreeAccount
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[value='4']")); }
        }

        public IWebElement BusinessAccount
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[value='256']")); }
        }

        public IWebElement GoButton
        {
            get { return Driver.Instance.FindElement(By.CssSelector("[type='submit']")); }
        }
        #endregion
    }
}
